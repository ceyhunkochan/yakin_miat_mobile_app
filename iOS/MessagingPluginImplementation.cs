﻿using System;
using UIKit;
using Xamarin.Forms;
using Foundation;
using MessageUI;
using System.Net;
using System.IO;
using System.Text;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;
using Yakinmiat.iOS;

[assembly: Xamarin.Forms.Dependency(typeof(MessagingPlugingImplementation))]
namespace Yakinmiat.iOS
{
	public class MessagingPlugingImplementation : IEmailTask, IWebTask, IPhoneCallTask,IIPAddressTask,IClose
	{
		public MessagingPlugingImplementation()
		{
		}

		public bool CanMakePhoneCall
		{
				
				 
			get
			{
				/*
				var uri = new Uri("http://www.epsilongrup.com");
				NSUrl nsurl = new NSUrl(uri.GetComponents(UriComponents.HttpRequestUrl, UriFormat.UriEscaped));
				return UIApplication.SharedApplication.CanOpenUrl(nsurl);
				*/
				throw new NotImplementedException();

			}

		}

		public bool CanSendEmail
		{
			get
			{
				return MFMailComposeViewController.CanSendMail;
			}
		}

		public bool CanSendEmailAttachments
		{
			get
			{
				throw new NotImplementedException();
			}
		}

		public bool CanSendEmailBodyAsHtml
		{  
			get
			{
				throw new NotImplementedException();
			}
		}



		public void MakePhoneCall(string number, string name = null)
		{


			UIAlertView alert = new UIAlertView("ARAMA", number+"\nBu numarayı aramak istiyor musunuz? ",null, "HAYIR", new string[] { "EVET" });
			alert.Show();
					
			alert.Clicked += (sender, e) =>
			{ 
			if (UIDevice.CurrentDevice.Model.ToLower().StartsWith("ipad") || UIDevice.CurrentDevice.Model.ToLower().StartsWith("ipod"))
				{ 
						new UIAlertView("ARAMA", "Bu cihaz arama yapmayı desteklemiyor!", null, "TAMAM", null).Show();
				}
				else
				{
					if (e.ButtonIndex != alert.CancelButtonIndex)
					{
						// Make Phone Call
						//var phoneDialer = CrossMessaging.Current.PhoneDialer;
						//phoneDialer.MakePhoneCall(number);
						//Device.OpenUri(new Uri("tel:"+number));
					}
						
				}

			};
		


			//Device.OpenUri(new Uri("tel:" + number));
		}






		public void OpenBrowser(string url)
		{

			/*
			UIAlertView alert = new UIAlertView("WEB SAYFASI", "Bu websitesini varsayılan tarayıca açmak istiyor musunuz? ", null, "HAYIR", new string[] { "EVET" });
			alert.Show();

			alert.Clicked += (sender, e) =>
			{
				if (e.ButtonIndex != alert.CancelButtonIndex)
				{
						if(url.StartsWith("http"))
							Device.OpenUri(new Uri(url));
						else
							Device.OpenUri(new Uri("http://" + url));
				}

				 

			};

			*/

			if (url.Contains("http"))
				Device.OpenUri(new Uri(url));
			else
				Device.OpenUri(new Uri("http://" + url));

		}


		public void SendEmail(string to, string subject = null, string message = null)
		{

			/*
			UIAlertView alert = new UIAlertView("E-MAIL", "Bu e-mail adresine gönderi istiyor musunuz? ", null, "HAYIR", new string[] { "EVET" });
			alert.Show();

			alert.Clicked += (sender, e) =>
			{
				if (e.ButtonIndex != alert.CancelButtonIndex)
				{
					if (CanSendEmail)
						Device.OpenUri(new Uri("mailto:" + to));
					else
						new UIAlertView("E-MAIL", "Bu cihaz e-mail göndermeyi desteklemiyor!", null, "TAMAM", null).Show();
				}



			};
			*/



			if (CanSendEmail)
				Device.OpenUri(new Uri("mailto:" + to));
			else
				new UIAlertView("E-MAIL", "Bu cihaz e-mail göndermeyi desteklemiyor!", null, "TAMAM", null).Show();

		}

		public bool DownloadFromString(string uri)
		{
			bool isDownloadCompleted = true;
			string localPath = String.Empty;
			try
			{


				var webClient = new WebClient();
				string[] arr = uri.Split('/');
				string fileName = arr[arr.Length - 1];
				Uri var_uri = new Uri(uri);


				webClient.DownloadDataAsync(var_uri);
				webClient.DownloadDataCompleted += (s, e) =>
				{
					var bytes = e.Result; // get the downloaded data
					string documentsPath = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
					string localFilename = fileName;
					localPath = Path.Combine(documentsPath, localFilename);
					File.WriteAllBytes(localPath, bytes); // writes to local storage
					if (e.Error != null)
						isDownloadCompleted = false;
				};

			}
			catch (Exception ex)
			{
				Debug.WriteLine("DownloadFromString Exception: " + ex.StackTrace);
				isDownloadCompleted = false;
				return false;
				 
			}
			return isDownloadCompleted;
		}


		public void OpenFile(string filePath) 
		{

			Device.OpenUri(new Uri(filePath));
		
		}

		public void Close()
		{
			Thread.CurrentThread.Abort();
		}

		public async Task<string> GetIPAddress()
		{
			string URLGetIP = "http://api.ipify.org";
			try
			{
				WebClient wc = new WebClient();
				string ip = await wc.DownloadStringTaskAsync(new Uri(URLGetIP));
				return ip; 
			}
			catch
			{
				return string.Empty;
			}

		}
	}
}
