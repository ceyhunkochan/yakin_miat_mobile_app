﻿using System;
using System.Collections.Generic;
using System.Linq;
using Foundation;
using UIKit;
using Newtonsoft.Json;
using Xamarin.Forms;
using ObjCRuntime;
using System.Net.NetworkInformation;
using Security;
using System.Threading.Tasks;
using AudioToolbox;
using Plugin.Media;

namespace Yakinmiat.iOS
{
	[Register("AppDelegate")]
	public partial class AppDelegate : global::Xamarin.Forms.Platform.iOS.FormsApplicationDelegate
	{


		void StoreKeysInKeychain(string key, string value)
		{
			var s = new SecRecord(SecKind.GenericPassword)
			{
				ValueData = NSData.FromString(value),
				Generic = NSData.FromString(key)
			};
			var err = SecKeyChain.Add(s);
		}


		string GetRecordsFromKeychain(string key)
		{
			SecStatusCode res;
			string id;
			var rec = new SecRecord(SecKind.GenericPassword)
			{
				Generic = NSData.FromString(key)
			};
			var match = SecKeyChain.QueryAsRecord(rec, out res);
			if (match != null)
			{
				NSData ns_object = match.ValueData;
				id = ns_object.ToString();
				return id;
			}


			return null;
		}


		public override bool FinishedLaunching(UIApplication app, NSDictionary options)
		{


			global::Xamarin.Forms.Forms.Init();
			ImageCircleRenderer.Init();
			global::ZXing.Net.Mobile.Forms.iOS.Platform.Init();

			CrossMedia.Current.Initialize();

			if (UIDevice.CurrentDevice.CheckSystemVersion(8, 0))
			{

				/*
					NSUuid identifier = UIDevice.CurrentDevice.IdentifierForVendor;

					var id = identifier.AsString();
					var id = UIDevice.CurrentDevice.IdentifierForVendor.AsString();
					


				var id = NSString.FromHandle(Messaging.IntPtr_objc_msgSend(UIDevice.CurrentDevice.Handle, Selector.GetHandle("uniqueIdentifier")));


				var uuid = Guid.NewGuid();
				string id = uuid.ToString();

				
				//Device Info Plugin
		 		CrossDeviceInfo.Current.GenerateAppId() 
	 		    CrossDeviceInfo.Current.GenerateAppId(true)
              new Label { Text = "Generated AppId: " + CrossDeviceInfo.Current.GenerateAppId(true, "hello") },
              new Label { Text = "Generated AppId: " + CrossDeviceInfo.Current.GenerateAppId(true, "hello", "world") },
               
              new Label { Text = "Model: " + CrossDeviceInfo.Current.Model },
              new Label { Text = "Platform: " + CrossDeviceInfo.Current.Platform },
              new Label { Text = "Version: " + CrossDeviceInfo.Current.Version },
					}
			
			  */

				//string id3 = Countly.OpenUDID.generateFreshOpenUDID();
				//string id2 = GetDeviceMacAddress();
				//string id = UIDevice.CurrentDevice.LocalizedModel;

				string unique_key;
				var id = UIDevice.CurrentDevice.IdentifierForVendor.AsString();
				if (GetRecordsFromKeychain("unique_id") != null)
				{
					unique_key = GetRecordsFromKeychain("unique_id");
					Yakinmiat.App.uniqueDeviceID = unique_key;

				}
				else
				{
					StoreKeysInKeychain("unique_id", id);
					Yakinmiat.App.uniqueDeviceID = id;
				}




				var pushSettings = UIUserNotificationSettings.GetSettingsForTypes(
								   UIUserNotificationType.Alert | UIUserNotificationType.Badge | UIUserNotificationType.Sound,
								   new NSSet());

				UIApplication.SharedApplication.RegisterUserNotificationSettings(pushSettings);
				UIApplication.SharedApplication.RegisterForRemoteNotifications();

				/*
					if (UIApplication.SharedApplication.IsRegisteredForRemoteNotifications)
					{
						new UIAlertView("REG SUCCESS ", "REGISTRATION DONE :)", null, "OK", null).Show();
					}
					else {
						new UIAlertView("REG FAILURE_", "REGISTRATION FAILED :/", null, "OK", null).Show();
					}
				*/
			}
			else 
			{
				UIRemoteNotificationType notificationTypes = UIRemoteNotificationType.Alert | UIRemoteNotificationType.Badge | UIRemoteNotificationType.Sound;
				UIApplication.SharedApplication.RegisterForRemoteNotificationTypes(notificationTypes);
			}

			app.ApplicationIconBadgeNumber = 0;





			//initialize registration to APNS
			/*
				if (UIDevice.CurrentDevice.CheckSystemVersion(8, 0))
				{
					var pushSettings = UIUserNotificationSettings.GetSettingsForTypes(
						   UIUserNotificationType.Alert | UIUserNotificationType.Badge | UIUserNotificationType.Sound,
						   new NSSet());

					UIApplication.SharedApplication.RegisterUserNotificationSettings(pushSettings);
					UIApplication.SharedApplication.RegisterForRemoteNotifications();
					if (UIApplication.SharedApplication.IsRegisteredForRemoteNotifications)
					{
						new UIAlertView("REG SUCCESS ", "REGISTRATION DONE :)", null, "OK", null).Show();
					}
					else { 
						new UIAlertView("REG FAILURE_", "REGISTRATION FAILED :/", null, "OK", null).Show();
					}
				}
				else {
					UIRemoteNotificationType notificationTypes = UIRemoteNotificationType.Alert | UIRemoteNotificationType.Badge | UIRemoteNotificationType.Sound;
					UIApplication.SharedApplication.RegisterForRemoteNotificationTypes(notificationTypes);
				}
				
			*/



			//initialize the FORM
			Forms.Init();
			LoadApplication(new App());

			return base.FinishedLaunching(app, options);

		}


		//XAMARIN IOS NOTIFICATI0N REG FUNCTION
		public override void RegisteredForRemoteNotifications(UIApplication application, NSData deviceToken)
		{
			// Get current device token
			var DeviceToken = deviceToken.Description;
			if (!string.IsNullOrWhiteSpace(DeviceToken))
			{
				DeviceToken = DeviceToken.Trim('<').Trim('>').Trim(' ');
				Yakinmiat.App.regToken = DeviceToken;

			}

			// Get previous device token
			var oldDeviceToken = NSUserDefaults.StandardUserDefaults.StringForKey("PushDeviceToken");

			// Has the token changed?
			if (string.IsNullOrEmpty(oldDeviceToken) || !oldDeviceToken.Equals(DeviceToken))
			{
				//TODO: Put your own logic here to notify your server that the device token has changed/been created!
			}

			string device = UIDevice.CurrentDevice.Model;
			//NSUserDefaults.StandardUserDefaults.SetString(,"userId" );
			// Save new device token 
			NSUserDefaults.StandardUserDefaults.SetString(DeviceToken, "PushDeviceToken");


		}



		public override void DidReceiveRemoteNotification(UIApplication application, NSDictionary userInfo, Action<UIBackgroundFetchResult> completionHandler)
		{



			if (application.ApplicationState == UIApplicationState.Active)
			{

				application.ApplicationIconBadgeNumber = 0;

				// RUNNING APP NOTIFICATION SOUND! 
				NSUrl url = NSUrl.FromFilename("chord.mp3");
				SystemSound systemSound = new SystemSound(url);
				systemSound.PlaySystemSound();

				UIAlertView alert = new UIAlertView("1 yeni bildiriminiz var !", "Okumak için TAMAM'a tıklayınız",
				null, "HAYIR", new string[] { "TAMAM" });
				alert.Show();

				alert.Clicked += (sender, e) =>
				{
					
					if (e.ButtonIndex != alert.CancelButtonIndex)
					{						 
						ProcessNotification(application, userInfo, false);
						//SystemSound.Vibrate.PlaySystemSound();
					}
					else
					{
						application.ApplicationIconBadgeNumber = 0;
					}

				};


				//new UIAlertView("PUSH WHEN APP IS OPEN", "", null, "OK", null).Show();
			}
			else
			{
				application.ApplicationIconBadgeNumber = 0;
				ProcessNotification(application, userInfo, false);
				//new UIAlertView("PUSH WHEN APP IS CLOSE", "", null, "OK", null).Show();

			}


		}





		public override void ReceivedRemoteNotification(UIApplication application, NSDictionary userInfo)
		{
			//WHEN APP IS COMPLETELY CLOSE
			application.ApplicationIconBadgeNumber = 0;
			ProcessNotification(application, userInfo, false);
			//new UIAlertView("ReceivedRemoteNotification", "", null, "OK", null).Show();

		}


		void ProcessNotification(UIApplication application, NSDictionary options, bool fromFinishedLaunching)
		{
			// Check to see if the dictionary has the aps key.  This is the notification payload you would have sent
			if (null != options && options.ContainsKey(new NSString("aps")))
			{


				//Get the aps,notice_id,notice_type  as 	dictionary
				NSDictionary aps = options.ObjectForKey(new NSString("aps")) as NSDictionary;
				NSObject dict_json = options.ValueForKey(new NSString("dict_json")) as NSObject;

				string json = dict_json.ToString();

				Dictionary<string, string> _dict_object = JsonConvert.DeserializeObject<Dictionary<string, string>>(json);
				string pageId_string = _dict_object["pageId"];
				int pageId = int.Parse(pageId_string);
				App.rootPageId = pageId;
					

				//string _type = _dict_object["notice_type"];
				//string _id = _dict_object["notice_id"];
				//string _event_row_id = _dict_object["event_row_id"];
				//int _badgeCount = Convert.ToInt32(_dict_object["badge_count"]);
				//int event_row_id = 0;
				//if (!string.IsNullOrWhiteSpace(_event_row_id))
				//	event_row_id = Int32.Parse(_event_row_id);

				//application.ApplicationIconBadgeNumber = _badgeCount;



				//Extract the alert text
				// NOTE: If you're using the simple alert by just specifying
				// "  aps:{alert:"alert msg here"}  ", this will work fine.
				// But if you're using a complex alert with Localization keys, etc.,
				// your "alert" object from the aps dictionary will be another NSDictionary.
				// Basically the JSON gets dumped right into a NSDictionary,
				// so keep that in mind.





				string alert = string.Empty;


				if (aps.ContainsKey(new NSString("alert")))
					alert = (aps[new NSString("alert")] as NSString).ToString();



				//If this came from the ReceivedRemoteNotification while the app was running,
				// we of course need to manually process things like the sound, badge, and alert.



				if (!fromFinishedLaunching)
				{

					//EkonomiKulubu.App.notice_id = _id;
					//EkonomiKulubu.App.notice_type = _type;
					//if (_type.Equals("2"))
						//EkonomiKulubu.App.event_row_id = event_row_id;

					//new UIAlertView("PUSH WHEN APP IS OPEN", alert, null, "OK", null).Show();
					//EkonomiKulubu.App.Current.MainPage = new RootPage();
					application.ApplicationIconBadgeNumber = 0;


					Xamarin.Forms.Device.BeginInvokeOnMainThread(() =>
					{
						Yakinmiat.App.Current.MainPage = new RootPage(); //this for iOS 
					});


				}
				else
				{




				}


			}
		}



		public override void FailedToRegisterForRemoteNotifications(UIApplication application, NSError error)
		{
			//error.LocalizedDescription
			//new UIAlertView("REG FAILURE_", "NOT REGISTERED", null, "OK", null).Show();
		}


	}// class AppDelegate
}//namespace EkonomiKulubu.IOS

