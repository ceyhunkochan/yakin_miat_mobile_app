﻿using System;
using Yakinmiat;
using Yakinmiat.iOS;
using Xamarin.Forms;
using System.IO;
using SQLite.Extensions;
using SQLite.Net.Interop;
using SQLitePCL;

[assembly: Dependency(typeof(SQLite_iOS))]

namespace Yakinmiat.iOS
{
	public class SQLite_iOS : ISQLite
	{
		private string _Directory;
		private SQLite.Net.Interop.ISQLitePlatform _Platform;

		public SQLite_iOS()
		{
		}

		public string Directory
		{
			get
			{
				if (string.IsNullOrEmpty(_Directory))
				{
					var directory  = System.Environment.GetFolderPath(Environment.SpecialFolder.Personal);
					_Directory = System.IO.Path.Combine(directory, "..", "Library");
				}
				return _Directory;
			}
		}

		public ISQLitePlatform Platform
		{
			get
			{
				if (_Platform == null)
				{
					_Platform = new SQLite.Net.Platform.XamarinIOS.SQLitePlatformIOS();
				}
				return _Platform;
			}
		}


	}
}
