﻿using System;
using Xamarin.Forms;
using Yakinmiat.Drugs;

namespace Yakinmiat
{
	public class MyWishListItem : ViewCell
	{


		public MyWishListItem()
		{


			var drugName = new Label()
			{
				Text = "",
				FontAttributes = FontAttributes.Bold,
				FontFamily = "HelveticaNeue-Medium",
				FontSize = 16,
				TextColor = Color.Black
			};
			drugName.FontSize = Device.OnPlatform(
														Device.GetNamedSize(NamedSize.Small, drugName),
														16,
														Device.GetNamedSize(NamedSize.Small, drugName)
												  );

			drugName.SetBinding(Label.TextProperty, "DrugName");





			var pieceLabel = new Label()
			{
				Text = "Adet : ",
				FontAttributes = FontAttributes.None,
				FontFamily = "HelveticaNeue-Medium",
				FontSize = 15,
				HorizontalOptions = LayoutOptions.Start,
				TextColor = Color.Black
			};

			var piece = new Label()
			{
				Text = "",
				FontAttributes = FontAttributes.None,
				FontFamily = "HelveticaNeue-Medium",
				HorizontalOptions = LayoutOptions.Start,
				FontSize = 15,
				TextColor = Color.Black
			};
			piece.FontSize = Device.OnPlatform(
													Device.GetNamedSize(NamedSize.Small, piece),
													15,
													Device.GetNamedSize(NamedSize.Small, piece)
												 );

			piece.SetBinding(Label.TextProperty, "Piece");

			var otherSupply = new Button()
			{
				Text = "Diğer",
				FontAttributes = FontAttributes.None,
				FontFamily = "HelveticaNeue-Medium",
				Image = "other.png",
				BackgroundColor = Color.FromHex("#32c5d2"),
				TextColor = Color.White,
				HorizontalOptions = LayoutOptions.FillAndExpand
			};


			 var barcode = new Label()
			 {
				 Text = "",
				 FontFamily = "HelveticaNeue-Medium",
				 FontSize = 15,
				 TextColor = Color.Black
			 };
			barcode.FontSize = Device.OnPlatform(
													Device.GetNamedSize(NamedSize.Small, barcode),
													15,
													Device.GetNamedSize(NamedSize.Small, barcode)
												 );

			barcode.SetBinding(Label.TextProperty, "Barcode");



			var barcodeStack = new StackLayout
			{
				Padding = new Thickness(10, 0, 0, 0),
				Spacing = 0,
				Orientation = StackOrientation.Horizontal,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				Children = { barcode }
			};

			var namePlace = new StackLayout
			{
				Padding = new Thickness(10, 0, 0, 0),
				Spacing = 0,
				Orientation = StackOrientation.Horizontal,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				Children = { drugName }
			};


			var pieceStack = new StackLayout
			{
				Padding = new Thickness(10, 0, 0, 0),
				Spacing = 0,
				Orientation = StackOrientation.Horizontal,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				Children = { pieceLabel, piece }
			};

			var buttonStack = new StackLayout
			{
				Padding = new Thickness(10, 0, 0, 0),
				Spacing = 0,
				Orientation = StackOrientation.Vertical,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				Children = { otherSupply }
			};
			var vetDetailsLayout = new StackLayout
			{
				Padding = new Thickness(10, 0, 0, 0),
				Spacing = 0,
				Orientation = StackOrientation.Vertical,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				Children = { namePlace, pieceStack, barcodeStack,buttonStack }
			};




			var cellLayout = new StackLayout
			{
				Spacing = 0,
				Padding = new Thickness(10, 10, 10, 10),
				Orientation = StackOrientation.Horizontal,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.FillAndExpand,
				Children = { vetDetailsLayout }
			};

			this.View = cellLayout;


		}




	}
}

