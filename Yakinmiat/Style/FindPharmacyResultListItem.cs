﻿using System;
using Xamarin.Forms;


namespace Yakinmiat
{
	public class FindPharmacyResultListItem : ViewCell
	{
		public FindPharmacyResultListItem()
		{



			var pharmacyName = new Label()
			{
				Text = "",
				FontAttributes = FontAttributes.Bold,
				FontFamily = "HelveticaNeue-Medium",
				FontSize = 17,
				TextColor = Color.Black
			};
			pharmacyName.FontSize = Device.OnPlatform(
													Device.GetNamedSize(NamedSize.Small, pharmacyName),
													17,
													Device.GetNamedSize(NamedSize.Small, pharmacyName)
												 );

			pharmacyName.SetBinding(Label.TextProperty, "PharmacyName");


			var dash = new Label()
			{
				Text = "  -  ",
				FontAttributes = FontAttributes.None,
				FontFamily = "HelveticaNeue-Medium",
				FontSize = 15,
				TextColor = Color.Black
			};

			var city = new Label()
			{
				Text = "",
				FontAttributes = FontAttributes.Bold,
				FontFamily = "HelveticaNeue-Medium",
				FontSize = 13,
				TextColor = Color.Black
			};
			city.FontSize = Device.OnPlatform(
													Device.GetNamedSize(NamedSize.Small, city),
													13,
													Device.GetNamedSize(NamedSize.Small, city)
												);

			city.SetBinding(Label.TextProperty, "Town");



			var state = new Label()
			{
				Text = "",
				FontAttributes = FontAttributes.Bold,
				FontFamily = "HelveticaNeue-Medium",
				FontSize = 13,
				TextColor = Color.Black
			};
			state.FontSize = Device.OnPlatform(
													Device.GetNamedSize(NamedSize.Small, state),
													13,
													Device.GetNamedSize(NamedSize.Small, state)
												);

			state.SetBinding(Label.TextProperty, "Country");

			var slash = new Label { Text = "/", FontSize = 18, FontAttributes = FontAttributes.Bold };




			var namePlaceStack = new StackLayout
			{
				Padding = new Thickness(10, 0, 0, 0),
				Spacing = 0,
				Orientation = StackOrientation.Horizontal,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				//Children = { pharmacyName, dash, city, slash, state }
				Children = { pharmacyName, dash, state }
			};




			var ownerLabel = new Label()
			{
				Text = "Sahibi : ",
				FontAttributes = FontAttributes.Bold,
				FontFamily = "HelveticaNeue-Medium",
				FontSize = 13,
				TextColor = Color.Black
			};

			var owner = new Label()
			{
				Margin = new Thickness(10, 0, 0, 0),
				Text = "",
				FontAttributes = FontAttributes.None,
				FontFamily = "HelveticaNeue-Medium",
				FontSize = 13,
				TextColor = Color.Black
			};
			owner.FontSize = Device.OnPlatform(
													Device.GetNamedSize(NamedSize.Small, owner),
													13,
													Device.GetNamedSize(NamedSize.Small, owner)
												);

			owner.SetBinding(Label.TextProperty, "PharmacistNameSurname");


			var ownerStack = new StackLayout
			{
				Padding = new Thickness(10, 0, 0, 0),
				Spacing = 0,
				Orientation = StackOrientation.Horizontal,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				Children = { ownerLabel, owner }
			};

			var vetDetailsLayout = new StackLayout
			{
				Padding = new Thickness(10, 0, 0, 0),
				Spacing = 0,
				Orientation = StackOrientation.Vertical,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				Children = { namePlaceStack, ownerStack }
			};

			var cellLayout = new StackLayout
			{
				Spacing = 0,
				Padding = new Thickness(10, 10, 10, 10),
				Orientation = StackOrientation.Horizontal,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.FillAndExpand,
				Children = { vetDetailsLayout }
			};

			this.View = cellLayout;
		}
	}
}

