﻿using System;
using Xamarin.Forms;


namespace Yakinmiat
{
	public class PharmacyListItem : ViewCell
	{
		public PharmacyListItem()
		{

			var pharmacyName = new Label()
			{
				Text = "",
				FontAttributes = FontAttributes.Bold,
				FontFamily = "HelveticaNeue-Medium",
				FontSize = 17,
				TextColor = Color.Black
			};
			pharmacyName.FontSize = Device.OnPlatform(
															Device.GetNamedSize(NamedSize.Small, pharmacyName),
															17,
															Device.GetNamedSize(NamedSize.Small, pharmacyName)
														);

			pharmacyName.SetBinding(Label.TextProperty, "Pharmacy");

			var dash = new Label()
			{
				Text = "  -  ",
				FontAttributes = FontAttributes.None,
				FontFamily = "HelveticaNeue-Medium",
				FontSize = 15,
				TextColor = Color.Black
			};


			var miat = new Label()
			{
				Text = " Miat : ",
				FontAttributes = FontAttributes.None,
				FontFamily = "HelveticaNeue-Medium",
				FontSize = 15,
				TextColor = Color.Black
			};

			var dueDate = new Label()
			{
				Text = "",
				FontAttributes = FontAttributes.None,
				FontFamily = "HelveticaNeue-Medium",
				FontSize = 15,
				TextColor = Color.Black
			};
			dueDate.FontSize = Device.OnPlatform(
													Device.GetNamedSize(NamedSize.Small, dueDate),
													15,
													Device.GetNamedSize(NamedSize.Small, dueDate)
												 );

			dueDate.SetBinding(Label.TextProperty, "Date");



			var dash2 = new Label()
			{
				Text = "  -  ",
				FontAttributes = FontAttributes.None,
				FontFamily = "HelveticaNeue-Medium",
				FontSize = 15,
				TextColor = Color.Black
			};


			var amount = new Label()
			{
				Text = "",
				FontAttributes = FontAttributes.Bold,
				FontFamily = "HelveticaNeue-Medium",
				FontSize = 15,
				TextColor = Color.Black
			};
			amount.FontSize = Device.OnPlatform(
													Device.GetNamedSize(NamedSize.Small, amount),
													15,
													Device.GetNamedSize(NamedSize.Small, amount)
												);

			amount.SetBinding(Label.TextProperty, "Piece");



			var city = new Label()
			{
				Text = "",
				FontAttributes = FontAttributes.Bold,
				FontFamily = "HelveticaNeue-Medium",
				FontSize = 13,
				TextColor = Color.Black
			};
			city.FontSize = Device.OnPlatform(
													Device.GetNamedSize(NamedSize.Small, city),
													13,
													Device.GetNamedSize(NamedSize.Small, city)
												);

			city.SetBinding(Label.TextProperty, "Country");




			var namePlace = new StackLayout
			{
				Padding = new Thickness(10, 0, 0, 0),
				Spacing = 0,
				Orientation = StackOrientation.Horizontal,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				Children = { pharmacyName, dash, city }
			};


			var duedateAmount = new StackLayout
			{
				Padding = new Thickness(10, 0, 0, 0),
				Spacing = 0,
				Orientation = StackOrientation.Horizontal,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				Children = { amount, dash2, miat, dueDate }
			};


			var vetDetailsLayout = new StackLayout
			{
				Padding = new Thickness(10, 0, 0, 0),
				Spacing = 0,
				Orientation = StackOrientation.Vertical,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				Children = { namePlace, duedateAmount }
			};

			var cellLayout = new StackLayout
			{
				Spacing = 0,
				Padding = new Thickness(10, 10, 10, 10),
				Orientation = StackOrientation.Horizontal,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.FillAndExpand,
				Children = { vetDetailsLayout }
			};

			this.View = cellLayout;


		}
	}
}
