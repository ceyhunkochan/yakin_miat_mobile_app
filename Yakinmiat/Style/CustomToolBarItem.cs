﻿using System;
using Xamarin.Forms;

namespace Yakinmiat
{
	public class CustomToolBarItem : ToolbarItem
	{
		public Entry entryItem;
		public CustomToolBarItem()
		{
			this.entryItem = new Entry();
			entryItem.Placeholder = "Ürün İsmi";
			entryItem.WidthRequest = 120;
			entryItem.IsEnabled = true;
		}
	}
}
