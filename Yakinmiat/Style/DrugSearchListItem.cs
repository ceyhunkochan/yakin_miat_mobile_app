﻿using System;
using Xamarin.Forms;


namespace Yakinmiat
{
	public class DrugSearchListItem : ViewCell
	{
		public DrugSearchListItem()
		{

			 
			 
			var drugNumber = new Label()
			{
				Text = "",
				FontFamily = "HelveticaNeue-Medium",
				FontSize = 15,
				TextColor = Color.Black
			};
			drugNumber.FontSize = Device.OnPlatform(
													Device.GetNamedSize(NamedSize.Small, drugNumber),
													15,
													Device.GetNamedSize(NamedSize.Small, drugNumber)
												 );

			drugNumber.SetBinding(Label.TextProperty, "Barcode");



			var drugName = new Label()
			{
				Text = "",
				FontFamily = "HelveticaNeue-Medium",
				FontSize = 15,
				TextColor = Color.Black
			};
			drugName.FontSize = Device.OnPlatform(
													Device.GetNamedSize(NamedSize.Small, drugName),
													15,
													Device.GetNamedSize(NamedSize.Small, drugName)
												 );

			drugName.SetBinding(Label.TextProperty, "Name");



			var vetDetailsLayout = new StackLayout
			{
				Padding = new Thickness(10, 0, 0, 0),
				Spacing = 0,
				Orientation = StackOrientation.Vertical,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				Children = { drugName,drugNumber  }
			};

			var cellLayout = new StackLayout
			{
				Spacing = 0,
				Padding = new Thickness(10, 10, 10, 10),
				Orientation = StackOrientation.Horizontal,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.FillAndExpand,
				Children = { vetDetailsLayout }
			};

			this.View = cellLayout;
		}
	}
}

