﻿using System;
using Xamarin.Forms;

namespace Yakinmiat
{
	public class ProfileCell : TextCell
	{
		public ProfileCell()
		{
			var data = new TextCell
			{
				Text = "TextCell Text",
				Detail = "TextCell Detail"
			};

			data.SetBinding(TextCell.TextProperty, "Title");
			data.SetBinding(TextCell.DetailProperty, "Subtitle");

			this.Text = data.Text;
			this.Detail = data.Detail;
		}
	}
}
