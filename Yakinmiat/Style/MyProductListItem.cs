﻿using System;
using Xamarin.Forms;
using Yakinmiat.Drugs;

namespace Yakinmiat
{
	public class MyProductListItem : ViewCell
	{


		public MyProductListItem()
		{ 


			var drugName = new Label()
			{ 
				Text = "",
				FontAttributes = FontAttributes.Bold,
				FontFamily = "HelveticaNeue-Medium",
				FontSize = 14,
				TextColor = Color.Black
			};
			drugName.FontSize = Device.OnPlatform(
														Device.GetNamedSize(NamedSize.Small, drugName),
														14,
														Device.GetNamedSize(NamedSize.Small, drugName)
													);

			drugName.SetBinding(Label.TextProperty, "DrugName");



			var barcodeLabel = new Label()
			{
				Text = "Fiyat : ",
				FontAttributes = FontAttributes.Bold,
				FontFamily = "HelveticaNeue-Medium",
				FontSize = 13,
				TextColor = Color.Black
			};




			var price = new Label()
			{
				Text = "",
				FontAttributes = FontAttributes.Bold,
				FontFamily = "HelveticaNeue-Medium",
				FontSize = 13,
				TextColor = Color.Black
			};
			price.FontSize = Device.OnPlatform(
													Device.GetNamedSize(NamedSize.Small, price),
													13,
													Device.GetNamedSize(NamedSize.Small, price)
												);

			price.SetBinding(Label.TextProperty, "Price");


			 









			var miat = new Label()
			{
				Text = " Miat : ",
				FontAttributes = FontAttributes.None,
				FontFamily = "HelveticaNeue-Medium",
				FontSize = 15,
				TextColor = Color.Black
			};

			var dueDate = new Label()
			{
				Text = "",
				FontAttributes = FontAttributes.None,
				FontFamily = "HelveticaNeue-Medium",
				FontSize = 15,
				TextColor = Color.Black
			};
			dueDate.FontSize = Device.OnPlatform(
													Device.GetNamedSize(NamedSize.Small, dueDate),
													15,
													Device.GetNamedSize(NamedSize.Small, dueDate)
												 );

			dueDate.SetBinding(Label.TextProperty, "Date");



			var dash2 = new Label()
			{
				Text = "-",
				FontAttributes = FontAttributes.None,
				FontFamily = "HelveticaNeue-Medium",
				FontSize = 15,
				TextColor = Color.Black
			};


			var amount = new Label()
			{
				Text = "",
				FontAttributes = FontAttributes.Bold,
				FontFamily = "HelveticaNeue-Medium",
				FontSize = 15,
				TextColor = Color.Black
			};
			amount.FontSize = Device.OnPlatform(
													Device.GetNamedSize(NamedSize.Small, amount),
													15,
													Device.GetNamedSize(NamedSize.Small, amount)
												);

			amount.SetBinding(Label.TextProperty, "Piece");



			 var amountLabel = new Label()
			 {
				 Text = " Adet ",
				 FontAttributes = FontAttributes.None,
				 FontFamily = "HelveticaNeue-Medium",
				 FontSize = 15,
				 TextColor = Color.Black
			 };

		 

			var namePlace = new StackLayout
			{
				Padding = new Thickness(10, 0, 0, 0),
				Spacing = 0,
				Orientation = StackOrientation.Horizontal,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				Children = { drugName }
			};


			var barcodeStack = new StackLayout
			{
				Padding = new Thickness(10, 0, 0, 0),
				Spacing = 0,
				Orientation = StackOrientation.Horizontal,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				Children = { barcodeLabel, price }
			};

			var duedateAmount = new StackLayout
			{
				Padding = new Thickness(10, 0, 0, 0),
				Spacing = 0,
				Orientation = StackOrientation.Horizontal,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				Children = { amount,amountLabel, dash2, miat, dueDate }
			};


			var vetDetailsLayout = new StackLayout
			{
				Padding = new Thickness(10, 0, 0, 0),
				Spacing = 0,
				Orientation = StackOrientation.Vertical,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				Children = { namePlace, duedateAmount,barcodeStack }
			};

			var deleteButton = new Button
			{
				Text = "Delete"
			};
			deleteButton.SetBinding(Button.CommandParameterProperty, new Binding("."));
			deleteButton.SetBinding(Button.CommandProperty,
			 new Binding("DeleteCommand"));

			var cellLayout = new StackLayout
			{
				Spacing = 0,
				Padding = new Thickness(10, 10, 10, 10),
				Orientation = StackOrientation.Horizontal,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.FillAndExpand,
				Children = { vetDetailsLayout }
			};

			this.View = cellLayout;


		}




	}
}

