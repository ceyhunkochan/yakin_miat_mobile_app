﻿using System;
using Xamarin.Forms;

namespace Yakinmiat
{
	public class ChatListItem : ViewCell
	{
		public ChatListItem()
		{
			var pharmacyName = new Label()
			{
				FontAttributes = FontAttributes.Bold,
				FontFamily = "HelveticaNeue-Medium",
				TextColor = Color.Black,
			};
			pharmacyName.FontSize = Device.OnPlatform(
															Device.GetNamedSize(NamedSize.Small, pharmacyName),
															17,
															Device.GetNamedSize(NamedSize.Small, pharmacyName)
														);

			pharmacyName.SetBinding(Label.TextProperty, "PharmacyName");


			var message = new Label()
			{
				FontAttributes = FontAttributes.None,
				FontFamily = "HelveticaNeue-Medium",
				FontSize = 15,
				TextColor = Color.Black
			};
			message.FontSize = Device.OnPlatform(
															Device.GetNamedSize(NamedSize.Small, message),
															17,
															Device.GetNamedSize(NamedSize.Small, message)
														);
			message.SetBinding(Label.TextProperty, "Message");


			var pharmacyNamePlace = new StackLayout
			{
				Padding = new Thickness(10, 0, 0, 0),
				Spacing = 0,
				Orientation = StackOrientation.Horizontal,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				Children = { pharmacyName }
			};
			pharmacyNamePlace.SetBinding(StackLayout.HorizontalOptionsProperty, "Alignment");


			var messagePlace = new StackLayout
			{
				Padding = new Thickness(10, 0, 0, 0),
				Spacing = 0,
				Orientation = StackOrientation.Horizontal,
				Children = { message }
			};
			messagePlace.SetBinding(StackLayout.HorizontalOptionsProperty, "Alignment");


			var vetDetailsLayout = new StackLayout
			{
				Padding = new Thickness(10, 0, 0, 0),
				Spacing = 0,
				Orientation = StackOrientation.Vertical,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				Children = { pharmacyNamePlace, messagePlace }
			};

			var cellLayout = new StackLayout
			{
				Spacing = 0,
				Padding = new Thickness(10, 10, 10, 10),
				Orientation = StackOrientation.Horizontal,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.FillAndExpand,
				Children = { vetDetailsLayout }
			};

			this.View = cellLayout;
		}
	}
}
