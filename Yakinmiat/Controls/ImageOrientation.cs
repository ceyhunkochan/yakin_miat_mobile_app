﻿using System;
namespace Yakinmiat
{
	public enum ImageOrientation
	{
		/// <summary>
		/// The image to left
		/// </summary>
		ImageToLeft = 0,
		/// <summagry>
		/// The image on top
		/// </summary>
		ImageOnTop = 1,
		/// <summary>
		/// The image to right
		/// </summary>
		ImageToRight = 2,
		/// <summary>
		/// The image on bottom
		/// </summary>
		ImageOnBottom = 3,
		/// <summary>
		/// The image centered
		/// </summary>
		ImageCentered = 4
	}
}
