﻿using System;
using System.Collections.Generic;
using System.Linq;
using Xamarin.Forms;

namespace Yakinmiat
{
	public partial class ChatPage : ContentPage
	{
		public List<ServerChatModel> wishData;
		bool isListSet = false;
		public ChatPage(string userId, string pharmacistName, string pharmacyName)
		{
			InitializeComponent();
			Title = pharmacistName;
			var context = new ChatViewModel(this, userId, pharmacyName);
			BindingContext = context;

			chatList.ItemTemplate = new DataTemplate(typeof(ChatListItem));
			chatList.SetBinding(ListView.ItemsSourceProperty, "Messages");
			context.GetUserConversationCommand.Execute(null);
			chatList.ItemSelected += (sender, e) =>
			{
				chatList.SelectedItem = null;
			};

			msg.SetBinding(Entry.TextProperty, ChatViewModel.MessagePropertyName);
			msgSend.Clicked += (object sender, EventArgs e) =>
			{
				context.SendMessageCommand.Execute(null);

			};


			chatList.ItemAppearing += (object sender, ItemVisibilityEventArgs e) =>
		   	{
				   //var last = chatList.ItemsSource.Cast<object>().LastOrDefault();
				   //chatList.ScrollTo(last, ScrollToPosition.MakeVisible, true);
			   };

		}
	}
}
