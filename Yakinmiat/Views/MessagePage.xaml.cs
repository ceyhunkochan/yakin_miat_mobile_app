﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace Yakinmiat
{
	public partial class MessagePage : ContentPage
	{
		ChatViewModel _ChatViewModel;
		public MessagePage()
		{
			InitializeComponent();
			BindingContext = _ChatViewModel = new ChatViewModel(this, null, null);
			_ChatViewModel.GetConversationCommand.Execute(null);
			searchList.SetBinding(ListView.ItemsSourceProperty, "Conversations");

			Title = "Kişiler";
			searchList.ItemSelected += async (object sender, SelectedItemChangedEventArgs e) =>
			{
				if (searchList.SelectedItem == null)
				{
					return;
				}
				var conversation = searchList.SelectedItem as ActiveChatUserModel;
				searchList.SelectedItem = null;
				await Navigation.PushAsync(new ChatPage(conversation.Id, conversation.Pharmacist, conversation.PharmacyName));
			};
		}
	}
}
