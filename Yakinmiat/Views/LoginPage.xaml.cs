﻿using System;
using System.Collections.Generic;
using Plugin.Connectivity;
using Xamarin.Forms;
using Yakinmiat.UserModels;

namespace Yakinmiat
{
	public partial class LoginPage : ContentPage
	{
		LoginViewModel loginViewModel;
		public LoginPage()
		{
			InitializeComponent();

			if (CrossConnectivity.Current.IsConnected)
			{
				var tapGestureRecognizer = new TapGestureRecognizer();
				tapGestureRecognizer.Tapped += (s, e) =>
				{
					Navigation.PushAsync(new ForgetPasswordPage());
				};
				forgetPassword.GestureRecognizers.Add(tapGestureRecognizer);

				BindingContext = loginViewModel = new LoginViewModel(this);


				activityIndicator.SetBinding(IsVisibleProperty, "IsBusy"); 				activityIndicator.SetBinding(ActivityIndicator.IsRunningProperty, "IsBusy");

				usernameEntry.SetBinding(Entry.TextProperty, LoginViewModel.UsernamePropertyName);

				passwordEntry.SetBinding(Entry.TextProperty, LoginViewModel.PasswordPropertyName);

				loginBtn.SetBinding(Button.CommandProperty, LoginViewModel.LoginCommandPropertyName);

			}
			else
			{
				DisplayAlert("Uyarı", "Lütfen internet bağlantınızı kontrol ediniz.", "Tamam");
			}


		}
	}
}
