﻿using System;
using System.Linq;
using System.Collections.Generic;

using Xamarin.Forms;
using Yakinmiat.Drugs;
using ZXing.Net.Mobile.Forms;
using Plugin.Media;
using Plugin.Connectivity;

namespace Yakinmiat
{
	public partial class SearchProductForRequestPage : ContentPage
	{
		ZXingScannerPage scanPage;
		List<DrugModel> searchPageItems;
		UrunBulViewModel _UrunBulViewModel;
		public string qrcodeResult;
		public string _source = null;

		public SearchProductForRequestPage()
		{
			InitializeComponent();

			searchList.ItemTemplate = new DataTemplate(typeof(DrugSearchListItem));
			searchList.ItemsSource = null;
			searchList.HasUnevenRows = true;
			Title = "Ürün Listesi";
			Icon = "urunler.PNG";



			if (CrossConnectivity.Current.IsConnected)
			{
				BindingContext = _UrunBulViewModel = new UrunBulViewModel(this, null, null, null, null);
				//cameraIcon.GestureRecognizers.Add(new TapGestureRecognizer
				//{
				//	TappedCallback = async (v, o) =>
				//	{

				//		if (!CrossMedia.Current.IsCameraAvailable)
				//		{
				//			await DisplayAlert("No Camera", "No Camera Available", "OK");
				//		}
				//		scanPage = new ZXingScannerPage();
				//		scanPage.Title = "Tarama";
				//		scanPage.OnScanResult += (result) =>
				//		{
				//			scanPage.IsScanning = false;

				//			Device.BeginInvokeOnMainThread(() =>
				//		   {
				//			   Navigation.PopAsync();
				//			   //DisplayAlert("Scanned Barcode", result.Text, "OK");
				//			   qrcodeResult = result.Text;

				//		   });
				//		};

				//		await Navigation.PushAsync(scanPage);


				//		//var customScanPage = new CustomScannerCS();
				//		//await Navigation.PushAsync(customScanPage);
				//	},
				//	NumberOfTapsRequired = 1
				//});


				searchList.ItemSelected += async (sender, e) =>
					{
						if (searchList.SelectedItem == null)
						{
							return;
						}

						var drug = searchList.SelectedItem as DrugModel;
						searchList.SelectedItem = null;
						await Navigation.PushAsync(new AddRequest(drug.Id, drug.Name, drug.Barcode, null));
					};

			}
			else
			{
				DisplayAlert("Uyarı", "Lütfen internet bağlantınızı kontrol ediniz.", "Tamam");
			}

		}

		async void onScanProduct(object sender, EventArgs e)
		{
			if (!CrossMedia.Current.IsCameraAvailable)
			{
				await DisplayAlert("No Camera", "No Camera Available", "OK");
			}
			scanPage = new ZXingScannerPage();
			scanPage.Title = "Tarama";
			scanPage.OnScanResult += (result) =>
			{
				scanPage.IsScanning = false;

				Device.BeginInvokeOnMainThread(() =>
			   {
				   Navigation.PopAsync();
				   qrcodeResult = result.Text;

			   });
			};

			await Navigation.PushAsync(scanPage);
		}

		async void onSearch(object sender, TextChangedEventArgs e)
		{
			if (!String.IsNullOrEmpty(e.NewTextValue) && e.NewTextValue.Length > 3)
			{
				var drugList = await DrugController.GetDrugs(e.NewTextValue);
				if (drugList.Count > 0)
				{
					searchList.ItemsSource = drugList;
				}
			}else searchList.ItemsSource = null;

		}

		protected async override void OnAppearing()
		{
			base.OnAppearing();
			if (!string.IsNullOrEmpty(qrcodeResult))
			{
				QRCodeModel qrcode = new QRCodeModel();

				if (qrcodeResult.Length > 40)
				{
					qrcode = await DrugController.GetBarcodeDrugs(qrcodeResult.Substring(1, qrcodeResult.Length - 1));
				}
				else {
					qrcode = await DrugController.GetBarcodeDrugs(qrcodeResult);

				}
				if (qrcode.DrugId != null)
				{
					App.ProductSearchPage = null;
					//await DisplayAlert("Barkod Tarama", qrcode.DrugName, "OK");
					await Navigation.PushAsync(new AddRequest(qrcode.DrugId, qrcode.DrugName, qrcode.Barcode, null));
				}
				else
				{

					await DisplayAlert("Barkod Tarama", "Ürün bulunamadı", "OK");
				}
			}
			qrcodeResult = null;
		}



	}
}
