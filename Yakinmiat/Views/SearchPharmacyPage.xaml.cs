﻿using System;
using System.Linq;
using System.Collections.Generic;

using Xamarin.Forms;
using Yakinmiat.Drugs;
using ZXing.Net.Mobile.Forms;
using Plugin.Media;
using Yakinmiyat.Profile;

namespace Yakinmiat
{
	public partial class SearchPharmacyPage : ContentPage
	{

		ProfileModel profile, profile1, profile2;
		List<ProfileModel> profileList;
		public SearchPharmacyPage()
		{
			InitializeComponent();
			searchList.ItemTemplate = new DataTemplate(typeof(PharmacySearchListItem));
			searchList.ItemsSource = null;
			searchList.RowHeight = 60;

			profile = new ProfileModel();
			profile.NameSurname = "YUNUS GÜRGÖZ";
			profile.PharmacyName = "Yunus Eczanesi";
			profile.PharmacyCode = "8680001381784";
			profile.Phone1 = "(+90) 532 29 17";
			profile.Phone2 = "(+90) 532 29 17";
			profile.Fax = "(+90) 312 229 17 49";
			profile.CellPhone = "(+90) 532 29 17";
			profile.Address = "BTA Takovska 6 Belgrade 11000";
			profile.Town = "Yenişehir";
			profile.Country = "Mersin";

			profile1 = new ProfileModel();
			profile1.NameSurname = "Emre Karagöz";
			profile1.PharmacyName = "Eskisehir Eczanesi";
			profile1.PharmacyCode = "8680001381784";
			profile1.Phone1 = "(+90) 532 29 17";
			profile1.Phone2 = "(+90) 532 29 17";
			profile1.Fax = "(+90) 312 229 17 49";
			profile1.CellPhone = "(+90) 532 29 17";
			profile1.Address = "BTA Takovska 6 Belgrade 11000";
			profile1.Town = "Ulus";
			profile1.Country = "Ankara";



			profile2 = new ProfileModel();
			profile2.NameSurname = "Burak Aydın";
			profile2.PharmacyName = "Burak Eczanesi";
			profile2.PharmacyCode = "8680001381784";
			profile2.Phone1 = "(+90) 532 29 17";
			profile2.Phone2 = "(+90) 532 29 17";
			profile2.Fax = "(+90) 312 229 17 49";
			profile2.CellPhone = "(+90) 532 29 17";
			profile2.Address = "BTA Takovska 6 Belgrade 11000";
			profile2.Town = "Kazan";
			profile2.Country = "Ankara";


			profileList = new List<ProfileModel>();
			profileList.Add(profile);
			profileList.Add(profile1);
			profileList.Add(profile2);

			searchList.ItemSelected += async (sender, e) =>
			 {
				 if (searchList.SelectedItem == null)
				 {
					 return;
				 }

				 var selected = searchList.SelectedItem as ProfileModel;
				 searchList.SelectedItem = null;
				 await Navigation.PushAsync(new PharmacyDetailPage(selected.PharmacyId));
			 };

		}


		public ListView DefaultListView { get { return this.searchList; } set { this.searchList = value; } }

		void OnSearchItemClicked(object sender, EventArgs e)
		{
			if (searchBar.Text.Contains("eczane"))
				searchList.ItemsSource = profileList;
		}

		protected override void OnAppearing()
		{
			base.OnAppearing();

		}

	}
}
