﻿using System;
using System.Collections.Generic;
using Plugin.Connectivity;
using Xamarin.Forms;
using Yakinmiyat.Profile;
using Yakinmiyat.Users;

namespace Yakinmiat
{
	public partial class PharmacyDetailPage : ContentPage
	{
		GetProfileViewModel _GetProfileViewModel;
		List<ProfileInfoGroupedModel> ProfileInfo;
		string UserBaseId;
		string _PharmacyId;
		bool PharmacyFavorite;
		public PharmacyDetailPage(string PharmacyId)
		{
			InitializeComponent();
			this._PharmacyId = PharmacyId;

			if (CrossConnectivity.Current.IsConnected)
			{
				BindingContext = _GetProfileViewModel = new GetProfileViewModel(this, PharmacyId);
				_GetProfileViewModel.GetProfileCommand.Execute(null);

				searchList.ItemsSource = null;
				searchList.HasUnevenRows = true;

				btn_fav.Clicked += Btn_Fav_Clicked;
			}
			else
			{
				DisplayAlert("Uyarı", "Lütfen internet bağlantınızı kontrol ediniz.", "Tamam");
			}

		}

		protected async override void OnAppearing()
		{
			base.OnAppearing();
			using (var database = new DBService())
			{
				DataBaseModel _database = database.Data();
				if (_database != null)
				{
					var UserData = Newtonsoft.Json.JsonConvert.DeserializeObject<UserInfoModel>(_database.UserInfo);
					if (UserData != null)
					{
						UserBaseId = UserData.UserId;
					}
				}
			}

			ProfileInfo = new List<ProfileInfoGroupedModel>();
			var returnModel = await ProfileController.GetProfile("", "", userid: UserBaseId, profileId: _PharmacyId);

			this.PharmacyFavorite = returnModel.isFavorite;

			if (returnModel.isFavorite)
			{
				btn_fav.BackgroundColor = Color.FromHex("#e7505a");

				btn_fav.Text = "Favori Çıkar";
			}
			else
			{
				btn_fav.BackgroundColor = Color.FromHex("#32c5d2");
				btn_fav.Text = "Favori Ekle";
			}

			this.ProfileInfo.Add(new ProfileInfoGroupedModel { Subtitle = returnModel.PharmacyCode, Title = "Eczane GLN Numarası" });

			if (string.IsNullOrWhiteSpace(returnModel.CellPhone))
			{
				this.ProfileInfo.Add(new ProfileInfoGroupedModel { Subtitle = "Girilmedi.", Title = "Cep Telefonu" });
			}
			else
			{
				if (returnModel.CellPhone[0] != '0')
				{
					this.ProfileInfo.Add(new ProfileInfoGroupedModel { Subtitle = "+90" + returnModel.CellPhone.Replace("(", "").Replace(")", "").Replace(" ", ""), Title = "Cep Telefonu" });
				}
				else
				{
					this.ProfileInfo.Add(new ProfileInfoGroupedModel { Subtitle = "+9" + returnModel.CellPhone.Replace("(", "").Replace(")", "").Replace(" ", ""), Title = "Cep Telefonu" });
				}
			}

			if (string.IsNullOrWhiteSpace(returnModel.Fax))
			{
				this.ProfileInfo.Add(new ProfileInfoGroupedModel { Subtitle = "Girilmedi.", Title = "Fax Numarası" });
			}
			else
			{
				if (returnModel.Fax[0] != '0')
				{
					this.ProfileInfo.Add(new ProfileInfoGroupedModel { Subtitle = "+90" + returnModel.Fax.Replace("(", "").Replace(")", "").Replace(" ", ""), Title = "Fax Numarası" });
				}
				else
				{
					this.ProfileInfo.Add(new ProfileInfoGroupedModel { Subtitle = "+9" + returnModel.Fax.Replace("(", "").Replace(")", "").Replace(" ", ""), Title = "Fax Numarası" });
				}
			}



			if (string.IsNullOrWhiteSpace(returnModel.Phone1))
			{
				this.ProfileInfo.Add(new ProfileInfoGroupedModel { Subtitle = "Girilmedi.", Title = "Diğer Telefon - 1" });
			}
			else
			{

				if (returnModel.Phone1[0] != '0')
				{
					this.ProfileInfo.Add(new ProfileInfoGroupedModel { Subtitle = "+90" + returnModel.Phone1.Replace("(", "").Replace(")", "").Replace(" ", ""), Title = "Diğer Telefon - 1" });
				}
				else
				{
					this.ProfileInfo.Add(new ProfileInfoGroupedModel { Subtitle = "+9" + returnModel.Phone1.Replace("(", "").Replace(")", "").Replace(" ", ""), Title = "Diğer Telefon - 1" });
				}
			}


			if (string.IsNullOrWhiteSpace(returnModel.Phone2))
			{
				this.ProfileInfo.Add(new ProfileInfoGroupedModel { Subtitle = "Girilmedi.", Title = "Diğer Telefon - 2" });
			}
			else
			{
				if (returnModel.Phone2[0] != '0')
				{
					this.ProfileInfo.Add(new ProfileInfoGroupedModel { Subtitle = "+90" + returnModel.Phone2.Replace("(", "").Replace(")", "").Replace(" ", ""), Title = "Diğer Telefon - 2" });
				}
				else
				{
					this.ProfileInfo.Add(new ProfileInfoGroupedModel { Subtitle = "+9" + returnModel.Phone2.Replace("(", "").Replace(")", "").Replace(" ", ""), Title = "Diğer Telefon - 2" });
				}
			}

			this.ProfileInfo.Add(new ProfileInfoGroupedModel { Subtitle = returnModel.Address, Title = "Adres" });
			this.ProfileInfo.Add(new ProfileInfoGroupedModel { Subtitle = returnModel.Country, Title = "İl - İlçe" });
			searchList.ItemsSource = this.ProfileInfo;

		}

		async void Btn_Fav_Clicked(object sender, EventArgs e)
		{
			activityIndicator.IsRunning = true;
			activityIndicator.IsVisible = true;
			if (this.PharmacyFavorite)
			{
				btn_fav.BackgroundColor = Color.FromHex("#32c5d2");
				btn_fav.Text = "Favori Ekle";
				var favResult = await ProfileController.SetFavorite("", "", UserBaseId, _PharmacyId, true);
				var returnModel = await ProfileController.GetProfile("", "", userid: UserBaseId, profileId: _PharmacyId);
				this.PharmacyFavorite = returnModel.isFavorite;

			}
			else
			{
				btn_fav.BackgroundColor = Color.FromHex("#e7505a");//e7505a

				btn_fav.Text = "Favori Çıkar";
				var favResult = await ProfileController.SetFavorite("", "", UserBaseId, _PharmacyId, false);
				var returnModel = await ProfileController.GetProfile("", "", userid: UserBaseId, profileId: _PharmacyId);
				this.PharmacyFavorite = returnModel.isFavorite;

			}
			activityIndicator.IsRunning = false;
			activityIndicator.IsVisible = false;
		}
	}
}
