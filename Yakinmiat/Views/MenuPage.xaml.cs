﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;
using Yakinmiyat.Models;
using Yakinmiat.Drugs;

namespace Yakinmiat
{
	public partial class MenuPage : ContentPage
	{

		public ListView ListView { get { return listView; } }

		public MenuPage()
		{
			InitializeComponent();


			var masterPageItems = new List<MenuPageItem>();

			masterPageItems.Add(new MenuPageItem
			{
				Title = "    Mesaj Kutusu",
				IconSource = "ic_home_6.png",
				Redirect = new MessagePage(),
			});

			masterPageItems.Add(new MenuPageItem
			{
				Title = "    Ürün Bul",
				IconSource = "ic_home_1.png",
				Redirect = new SearchProductPage()//FindProductPage
			});
			masterPageItems.Add(new MenuPageItem
			{
				Title = "    Ürün Ekle",
				IconSource = "ic_home_2.png",
				//Redirect = new AddProduct(new DrugModel() { Name = "Default NAME", Barcode = "Default BARCODE" })
				Redirect = new AddProductPage()
			});
			masterPageItems.Add(new MenuPageItem
			{
				Title = "    İstek Listem",
				IconSource = "ic_home_3.png",
				Redirect = new RequestListPage()
			});
			masterPageItems.Add(new MenuPageItem
			{
				Title = "    Takip Listem",
				IconSource = "ic_home_3.png",
				Redirect = new FollowListPage()
			});
			masterPageItems.Add(new MenuPageItem
			{
				Title = "    Eczane Bul",
				IconSource = "ic_home_4.png",
				Redirect = new FindPharmacyPage()
			});

			masterPageItems.Add(new MenuPageItem
			{
				Title = "    İletişim",
				IconSource = "ic_home_5.png",
				Redirect = new ContactPage(),
			});

			listView.ItemsSource = masterPageItems;

			/*
			    listView.ItemSelected += MenuItemSelected;
				listView.ItemSelected += (sender, e) =>
				{
					 
				};
			*/
		}




		/*

			async void MenuItemSelected(object sender, EventArgs e)
			{
				  var selectedObject =   as MenuPageItem;
				                            
			}

		*/

	}
}
