﻿using System;
using System.Collections.Generic;
using System.Linq;
using Plugin.Connectivity;
using Xamarin.Forms;
using Yakinmiyat.Profile;
using Yakinmiyat.SearchDrug;

namespace Yakinmiat
{

	public partial class PharmacyListPage : ContentPage
	{
		
		FindProductViewModel _FindProductViewModel;
		int? _DrugId;
		string _DrugName;

		public PharmacyListPage(int? DrugId, string DrugName)
		{

			InitializeComponent();
			this._DrugId = DrugId;
			this._DrugName = DrugName;

			if (CrossConnectivity.Current.IsConnected)
			{
				BindingContext = _FindProductViewModel = new FindProductViewModel(this, DrugId, null, null);
				_FindProductViewModel.GetSearchDrugListCommand.Execute(null);

				//nameLabel.SetBinding(Label.TextProperty, "DrugName");
				//activityIndicator.SetBinding(ActivityIndicator.IsVisibleProperty, "IsBusy");
				//activityIndicator.SetBinding(ActivityIndicator.GetSearchDrugListCommandIsRunningProperty, "IsBusy");
					 

				searchList.HasUnevenRows = true;
				searchList.SetBinding(ListView.ItemsSourceProperty, "SearchDrugList");



				searchList.ItemSelected += async (sender, e) =>
				{
					 if (searchList.SelectedItem == null)
					 {
						 return;
					 }

					 var selected = searchList.SelectedItem as SearchDrugModel;
					 searchList.SelectedItem = null;
					 await Navigation.PushAsync(new PharmacyDetailPage(selected.PharmacyId));
				};

			}
			else
			{
				DisplayAlert("Uyarı", "Lütfen internet bağlantınızı kontrol ediniz.", "Tamam");
			}

		}


		protected override void OnAppearing()
		{
			base.OnAppearing();
			this.nameLabel.Text = _DrugName;
		}



		public async void AddFollowList(object sender, EventArgs e)
		{
			var item = (Xamarin.Forms.Button)sender;
			List<SearchDrugModel> drug_list = (List<SearchDrugModel>) searchList.ItemsSource;
			string drug_date = drug_list[0].Date;
			BindingContext = _FindProductViewModel = new FindProductViewModel(this, this._DrugId, item.CommandParameter as int?, drug_date);
			_FindProductViewModel.AddFollowCommand.Execute(null);
			//_FindProductViewModel.GetSearchDrugListCommand.Execute(null);
		}

	}
}
