﻿using System;
using System.Collections.Generic;
using Plugin.Connectivity;
using Xamarin.Forms;

namespace Yakinmiat
{
	public partial class ForgetPasswordPage : ContentPage
	{
		public ForgetPasswordPage()
		{
			InitializeComponent();

			if (!CrossConnectivity.Current.IsConnected)
			{
				DisplayAlert("Uyarı", "Lütfen internet bağlantınızı kontrol ediniz.", "Tamam");
			}

		}
	}
}
