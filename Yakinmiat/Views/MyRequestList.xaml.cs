﻿using System;
using System.Collections.Generic;
using System.Linq;
using Xamarin.Forms;
using Yakinmiyat.SearchDrug;
using Yakinmiyat.SupplyList;
using Yakinmiyat.WishList;

namespace Yakinmiat
{
	public partial class MyRequestList : ContentPage
	{
		MyRequestViewModel _MyRequestViewModel;
		public List<WishListModel> wishData;
		bool isListSet = false;
		public MyRequestList()
		{
			InitializeComponent();
			Title = "İstek Listem";
			Icon = "liste.PNG";

			BindingContext = _MyRequestViewModel = new MyRequestViewModel(this, null, null,null, null,null);
			_MyRequestViewModel.WishListCommand.Execute(null);

			//searchList.ItemTemplate = new DataTemplate(typeof(MyWishListItem));
			searchList.ItemsSource = null;
			searchList.HasUnevenRows = true;
			searchList.SetBinding(ListView.ItemsSourceProperty, "WishList");

			searchBar.SetBinding(SearchBar.TextProperty, MyRequestViewModel.SearchValPropertyName);

			searchList.ItemSelected += async (sender, e) =>
			{
				if (searchList.SelectedItem == null)
				{
					return;
				}

				var wish = searchList.SelectedItem as WishListModel;
				searchList.SelectedItem = null;
				AddRequest updateDrugPage = new AddRequest(wish.DrugId, wish.DrugName, wish.Barcode, wish.Id);
				updateDrugPage.Title = "Adet Güncelle";
				await Navigation.PushAsync(updateDrugPage);

			};

			if (Device.OS == TargetPlatform.Android)
			{
				this.Appearing += Handle_Appearing;
			}


			//this.btnOtherSuppliers.Clicked += async (object sender, EventArgs e) => 
			//{

			//	var item = (Xamarin.Forms.Button)sender;

			//	await DisplayAlert("Uyarı", "Button Clicked", "Tamam");
			//};
		}

		public async void OtherSuppliers(object sender, EventArgs e)
		{
			var item = (Xamarin.Forms.Button)sender;

			var other = item.CommandParameter as List<SearchDrugModel>;
			await Navigation.PushAsync(new OtherSuppliersPage(other));
			//await DisplayAlert("Uyarı", "Button Clicked", "Tamam");
		}

		async void onSearch(object sender, TextChangedEventArgs e)
		{
			var dataIsFull = searchList.ItemsSource as List<WishListModel>;
			if (isListSet == false && dataIsFull.Count()>0)
			{
				wishData = new List<WishListModel>();
				wishData = searchList.ItemsSource as List<WishListModel>;
				isListSet = true;
			}

			if (isListSet)
			{
				if (!String.IsNullOrEmpty(e.NewTextValue) && e.NewTextValue.Length > 3)
				{
					var drugList = wishData.Where(x => x.DrugName.ToLower().Contains(e.NewTextValue.ToLower())|| x.Barcode.Contains(e.NewTextValue.ToLower())).ToList();

					searchList.ItemsSource = drugList;
				}
				else searchList.ItemsSource = wishData;
			}
		}

		void Handle_Appearing(object sender, EventArgs e)
		{
			BindingContext = _MyRequestViewModel = new MyRequestViewModel(this, null, null, null, null,null);
			_MyRequestViewModel.WishListCommand.Execute(null);
		}

		protected override void OnAppearing()
		{
			BindingContext = _MyRequestViewModel = new MyRequestViewModel(this, null, null, null, null,null);
			_MyRequestViewModel.WishListCommand.Execute(null);
		}


	}
}
