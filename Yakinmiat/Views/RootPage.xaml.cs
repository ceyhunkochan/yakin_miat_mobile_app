﻿using System;
using System.Collections.Generic;
using Plugin.Connectivity;
using Xamarin.Forms;

namespace Yakinmiat
{
	
	public partial class RootPage : MasterDetailPage
	{

		private int pageId = 0;

		public RootPage() // 0  -> MainPage
		{
			InitializeComponent();

			menuPage.ListView.ItemSelected += OnItemSelected;

			this.pageId = App.rootPageId;

		 
			if (pageId != 0) 
			{ 
				RedirectPushMessage(pageId);
			}

			if (!CrossConnectivity.Current.IsConnected)
			{
				DisplayAlert("Uyarı", "Lütfen internet bağlantınızı kontrol ediniz.", "Tamam");
			}
				

		}

		protected override void OnAppearing()
		{
			base.OnAppearing();
		}

		void OnItemSelected(object sender, SelectedItemChangedEventArgs e)
		{
			var item = e.SelectedItem as MenuPageItem;
			if (item != null)
			{
				NavigationPage n = new NavigationPage(item.Redirect);
				n.BarBackgroundColor = Color.FromHex("#289F66");
				n.BarTextColor = Color.White;
				Detail = n;
				menuPage.ListView.SelectedItem = null;
				IsPresented = false;
			}
		}

		public enum Redirect 
		{ 
			MainPage,
			MessagePage,
			SearchProductPage,
			AddProductPage,
			RequestListPage,
			FindPharmacyPage,
			ContactPage
		}

		public void RedirectPushMessage(int pageId) 
		{ 
			switch (pageId)
			{
				case (int)Redirect.MainPage:
					
					//App.pageId = 0;
					NavigationPage n0 = new NavigationPage(new MainPage());
					n0.BarBackgroundColor = Color.FromHex("#289F66");
					n0.BarTextColor = Color.White;
					Detail = n0;
					menuPage.ListView.SelectedItem = null;
					IsPresented = false;
					break;



				case (int)Redirect.MessagePage:
					
					//App.pageId = 0;
					NavigationPage n1 = new NavigationPage(new MessagePage());
					n1.BarBackgroundColor = Color.FromHex("#289F66");
					n1.BarTextColor = Color.White;
					Detail = n1;
					menuPage.ListView.SelectedItem = null;
					IsPresented = false;
					break;	



				case (int)Redirect.SearchProductPage:
					
					//App.pageId = 0;
					NavigationPage n2 = new NavigationPage(new SearchProductPage());
					n2.BarBackgroundColor = Color.FromHex("#289F66");
					n2.BarTextColor = Color.White;
					Detail = n2;
					menuPage.ListView.SelectedItem = null;
					IsPresented = false;
					break;


  				case (int)Redirect.AddProductPage:
					
					//App.pageId = 0;
					NavigationPage n3 = new NavigationPage(new AddProductPage());
					n3.BarBackgroundColor = Color.FromHex("#289F66");
					n3.BarTextColor = Color.White;
					Detail = n3;
					menuPage.ListView.SelectedItem = null;
					IsPresented = false;
					break;




				case (int)Redirect.RequestListPage:
					
					//App.pageId = 0;
					NavigationPage n4 = new NavigationPage(new RequestListPage());
					n4.BarBackgroundColor = Color.FromHex("#289F66");
					n4.BarTextColor = Color.White;
					Detail = n4;
					menuPage.ListView.SelectedItem = null;
					IsPresented = false;
					break;


				
				case (int)Redirect.FindPharmacyPage:
					
					//App.pageId = 0;
					NavigationPage n5 = new NavigationPage(new FindPharmacyPage());
					n5.BarBackgroundColor = Color.FromHex("#289F66");
					n5.BarTextColor = Color.White;
					Detail = n5;
					menuPage.ListView.SelectedItem = null;
					IsPresented = false;
					break;
				

				
				case (int)Redirect.ContactPage:
					
					//App.pageId = 0;
					NavigationPage n6 = new NavigationPage(new ContactPage());
					n6.BarBackgroundColor = Color.FromHex("#289F66");
					n6.BarTextColor = Color.White;
					Detail = n6;
					menuPage.ListView.SelectedItem = null;
					IsPresented = false;
					break;
					
			}
		}
		 
	}
}
