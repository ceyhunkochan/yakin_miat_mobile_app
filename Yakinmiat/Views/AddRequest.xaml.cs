﻿using System;
using System.Collections.Generic;
using Plugin.Connectivity;
using Xamarin.Forms;

namespace Yakinmiat
{
	public partial class AddRequest : ContentPage
	{
		int? _Drugid;
		int? _isEditMode;
		MyRequestViewModel _MyRequestViewModel;
		public AddRequest(int? DrugId, string DrugName, string Barcode, int? isEditMode)
		{
			InitializeComponent();

			if (CrossConnectivity.Current.IsConnected)
			{
				productName.Text = DrugName;
				productBarcode.Text = Barcode;

				_isEditMode = isEditMode;
				_Drugid = DrugId;

				BindingContext = _MyRequestViewModel = new MyRequestViewModel(this, DrugId, isEditMode, null, null, null);

				if (isEditMode.HasValue)
				{
					var delete = new ToolbarItem("Sil", "delete.png", () =>
					{
					}, ToolbarItemOrder.Primary);
					ToolbarItems.Add(delete);

					delete.Clicked += async (object sender, EventArgs e) =>
					{
						await Navigation.PushAsync(new DeleteDrugPage(DrugId, DrugName, Barcode, isEditMode, 2));

					};
				}


				if (isEditMode.HasValue)
				{
					_MyRequestViewModel.WishItemCommand.Execute(null);
				}

				btn_kaydet.Clicked += Btn_Kaydet_Clicked;
			}
			else
			{
				DisplayAlert("Uyarı", "Lütfen internet bağlantınızı kontrol ediniz.", "Tamam");
			}
		}


		async void Btn_Kaydet_Clicked(object sender, EventArgs e)
		{

	
			if (CrossConnectivity.Current.IsConnected)
			{
				int? productPiece = String.IsNullOrEmpty(txtPiece.Text) ? 0 : Convert.ToInt32(txtPiece.Text);
				if (!productPiece.HasValue)
				{
					await DisplayAlert("Uyarı", "Miktar alanı boş bırakılamaz.", "Tamam");
				}
				else if (productPiece <= 0)
				{
					await DisplayAlert("Uyarı", "Miktar 0 veya 0 dan küçük olamaz.", "Tamam");
				}

				else
				{
					BindingContext = _MyRequestViewModel = new MyRequestViewModel(this, _Drugid, _isEditMode, productPiece, null, null);
					_MyRequestViewModel.EditWishCommand.Execute(null);
					_MyRequestViewModel.WishListCommand.Execute(null);
					_MyRequestViewModel.WishItemCommand.Execute(null);
				}
			}
			else
			{
				DisplayAlert("Uyarı", "Lütfen internet bağlantınızı kontrol ediniz.", "Tamam");
			}

		}
	}
}
