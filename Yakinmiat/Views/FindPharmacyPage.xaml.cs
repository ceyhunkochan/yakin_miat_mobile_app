﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;
using Yakinmiyat.Profile;
using Plugin.Connectivity;
using Yakinmiyat.SearchPharmacy;
using Yakinmiyat.Users; 

namespace Yakinmiat
{
	public partial class FindPharmacyPage : ContentPage
	{

		 


	    ConstantModel _ConstantModel;
		List<SearchPharmacyResultModel> resultList;
		public DBService database;
		public DataBaseModel data;
		public UserInfoModel userModel;
		FindPharmacyViewModel _FindPharmacyViewModel;



		public FindPharmacyPage()
		{
			InitializeComponent();
			activityIndicator.IsVisible = false;
			activityIndicator.IsRunning = false;
		}

		 

		protected async override void OnAppearing()
		{
			base.OnAppearing();

			using (database = new DBService())
			{
				data = database.Data();
				if (data != null)
				{ 
					userModel  = Newtonsoft.Json.JsonConvert.DeserializeObject<UserInfoModel>(data.UserInfo);
				}
			}


			if (CrossConnectivity.Current.IsConnected)
			{
				try
				{
					


					//FETCH _CONSTANTMODEL 

					if (_ConstantModel == null)
					{
						_ConstantModel = await ConstantController.GetConstant();
					}
			

					cityPicker.Items.Add("İl Seçiniz.");

					foreach (var item in _ConstantModel.City)
					{
						cityPicker.Items.Add(item.name);
					}

					cityPicker.SelectedIndex = 0;



					franchisePicker.Items.Add("Bayilik Seçiniz.");

					foreach (var item in _ConstantModel.Brand)
					{
						franchisePicker.Items.Add(item.name);
					}

					franchisePicker.SelectedIndex = 0;

					 

				}
				catch (Exception ex)
				{
					BugService.SendBugMessage(userModel.UserId, data.DeviceUniqueId, ex.Message, ex.StackTrace);
					await DisplayAlert("Uyarı", "Sayfa düzgün görüntülenemedi, lütfen daha sonra tekrar deneyin.", "Tamam");
				}

			}
			else
			{
				await DisplayAlert("Uyarı", "Lütfen internet bağlantınızı kontrol ediniz.", "Tamam");
			}

		}//void OnAppearing



		public async void SearchClicked(object sender, EventArgs e)
		{
			
			//DisplayAlert("Search","Search button clicked !","Tamam");
			if (CrossConnectivity.Current.IsConnected)
			{ 
					
				string pharmacyName = pharmacyNameEntry.Text;
				string glnNumber = glnNumberEntry.Text;
				int? cityId = null;
				int? brandId = null;

				if (cityPicker.SelectedIndex != 0) 
				{ 
					cityId = int.Parse(_ConstantModel.City[cityPicker.SelectedIndex - 1].id);
				}

				if (franchisePicker.SelectedIndex != 0)
					brandId = int.Parse(_ConstantModel.Brand[franchisePicker.SelectedIndex - 1].id);


				BindingContext = _FindPharmacyViewModel = new FindPharmacyViewModel(this, userModel.UserId, pharmacyName, glnNumber, cityId, brandId);

				activityIndicator.SetBinding(IsVisibleProperty, "IsBusy");
				activityIndicator.SetBinding(ActivityIndicator.IsRunningProperty, "IsBusy");

				_FindPharmacyViewModel.GetPharmacyListCommand.Execute(null);


			}
			else
			{
				await DisplayAlert("Uyarı", "Lütfen internet bağlantınızı kontrol ediniz.", "Tamam");
			}

		}

	}
}
