﻿using System;
using System.Collections.Generic;
using Plugin.Connectivity;
using Xamarin.Forms;
using Yakinmiat.Drugs;

namespace Yakinmiat
{
	public partial class RequestListPage : TabbedPage
	{
		public RequestListPage(bool isLeftMenu = true)
		{
			InitializeComponent();

			NavigationPage.SetHasNavigationBar(this, true);
			NavigationPage.SetHasBackButton(this, isLeftMenu);

			if (!CrossConnectivity.Current.IsConnected)
			{
				DisplayAlert("Uyarı", "Lütfen internet bağlantınızı kontrol ediniz.", "Tamam");
			}

		}
	}
}
