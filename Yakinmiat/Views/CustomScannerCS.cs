﻿using System;

using Xamarin.Forms;
using ZXing.Net.Mobile.Forms;

namespace Yakinmiat
{
	public class CustomScannerCS : ContentPage
	{
		ZXingScannerView zxing;
		ZXingDefaultOverlay overlay;
		public CustomScannerCS() : base()
		{
			zxing = new ZXingScannerView
			{
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.FillAndExpand,
				AutomationId = "zxingScannerView",
			};

			zxing.OnScanResult += (result) =>
			   Device.BeginInvokeOnMainThread(async () =>
			   {

				   // Stop analysis until we navigate away so we don't keep reading barcodes
				   zxing.IsAnalyzing = false;

				   // Show an alert
				   await DisplayAlert("Scanned Barcode", result.Text, "OK");

				   // Navigate away
				   await Navigation.PopAsync();
			   });

			overlay = new ZXingDefaultOverlay
			{
				TopText = "Hold your phone up to the barcode",
				BottomText = "Scanning will happen automatically",
				ShowFlashButton = zxing.HasTorch,
				AutomationId = "zxingDefaultOverlay",
			};
			overlay.FlashButtonClicked += (sender, e) =>
			{
				zxing.IsTorchOn = !zxing.IsTorchOn;
			};
			var grid = new Grid
			{
				VerticalOptions = LayoutOptions.FillAndExpand,
				HorizontalOptions = LayoutOptions.FillAndExpand,
			};
			grid.Children.Add(zxing);
			grid.Children.Add(overlay);

			// The root page of your application
			Content = grid;
		}

		protected override async void OnAppearing()
		{
			base.OnAppearing();

			try
			{
				zxing.IsScanning = true;
			}
			catch (Exception ex)
			{
				await DisplayAlert("Scanned Barcode", ex.StackTrace, "OK");
			}

		}

		protected override void OnDisappearing()
		{
			zxing.IsScanning = false;

			base.OnDisappearing();
		}
	}
}

