﻿using System;
using System.Collections.Generic;
using Plugin.Connectivity;
using Xamarin.Forms;
using Yakinmiat.Drugs;

namespace Yakinmiat
{
	public partial class DeleteDrugPage : ContentPage
	{
		public int? _id
		{
			get;
			set;
		}
		public int _source
		{
			get;
			set;
		}


		DeleteProductViewModel _DeleteProductViewModel;
		public DeleteDrugPage(int? DrugId, string DrugName, string Barcode, int? id, int source)
		{
			InitializeComponent();

			if (CrossConnectivity.Current.IsConnected)
			{
				this._id = id;
				this._source = source;

				productName.Text = DrugName;
				productBarcode.Text = Barcode;

				DeleteOption.SelectedIndexChanged += (object sender, EventArgs e) =>
				{
					txtGLN.Text = "";
					if (source == 1)
					{
						if (DeleteOption.SelectedIndex > 0)
						{
							var optionSelected = App._ConstantModel.SupplyDeleteOptions[DeleteOption.SelectedIndex - 1].id;
							if (optionSelected == "6")
							{
								glnStack.IsVisible = true;
							}
							else
							{
								glnStack.IsVisible = false;
							}
						}
					}
					else
					{
						if (DeleteOption.SelectedIndex > 0)
						{
							var optionSelected = App._ConstantModel.WishDeleteOptions[DeleteOption.SelectedIndex - 1].id;
							if (optionSelected == "2")
							{
								glnStack.IsVisible = true;
							}
							else
							{
								glnStack.IsVisible = false;
							}
						}
					}
				};


				btn_Sil.Clicked += (object sender, EventArgs e) =>
				{
					if (DeleteOption.SelectedIndex < 0)
					{
						DisplayAlert("Uyarı", "Silme sebebi seçmediniz.", "Tamam");
					}
					else if (String.IsNullOrEmpty(txtPiece.Text))
					{
						DisplayAlert("Uyarı", "Silmek istediğiniz miktarı girmediniz.", "Tamam");
					}
					else
					{
						if (source == 1)
						{
							var optionSelected = App._ConstantModel.SupplyDeleteOptions[DeleteOption.SelectedIndex - 1].id;
							if (optionSelected == "6" && String.IsNullOrEmpty(txtGLN.Text))
							{
								DisplayAlert("Uyarı", "GLN Numarası girilmedi.", "Tamam");
							}
							else
							{
								BindingContext = _DeleteProductViewModel = new DeleteProductViewModel(this, _id, Convert.ToInt32(optionSelected), Convert.ToInt32(txtPiece.Text), txtGLN.Text, 1);
								_DeleteProductViewModel.DeleteCommand.Execute(null);
							}
						}
						else
						{
							var optionSelected = App._ConstantModel.WishDeleteOptions[DeleteOption.SelectedIndex - 1].id;
							if (optionSelected == "2" && String.IsNullOrEmpty(txtGLN.Text))
							{
								DisplayAlert("Uyarı", "GLN Numarası girilmedi.", "Tamam");
							}
							else
							{
								BindingContext = _DeleteProductViewModel = new DeleteProductViewModel(this, _id, Convert.ToInt32(optionSelected), Convert.ToInt32(txtPiece.Text), txtGLN.Text, 2);
								_DeleteProductViewModel.DeleteCommand.Execute(null);
							}
						}
					}

				};
			}
			else
			{
				DisplayAlert("Uyarı", "Lütfen internet bağlantınızı kontrol ediniz.", "Tamam");
			}
		}



		protected override void OnAppearing()
		{
			base.OnAppearing();

			if (this._source == 1)
			{
				DeleteOption.Items.Add("Silme sebebinizi seçiniz");
				foreach (var item in App._ConstantModel.SupplyDeleteOptions)
				{
					DeleteOption.Items.Add(item.name);
				}
				DeleteOption.SelectedIndex = 0;
			}
			else
			{
				DeleteOption.Items.Add("Silme sebebinizi seçiniz");
				foreach (var item in App._ConstantModel.WishDeleteOptions)
				{
					DeleteOption.Items.Add(item.name);
				}
				DeleteOption.SelectedIndex = 0;
			}
		}
	}
}
