﻿using System;
using System.Collections.Generic;
using Plugin.Connectivity;
using Xamarin.Forms;
using Yakinmiat.Drugs;

namespace Yakinmiat
{
	public partial class AddProduct : ContentPage
	{


		MyProductViewModel _MyProductViewModel;
		DrugModel _DrugModel;
		int? _Drugid;
		int? _isEditMode;
		public AddProduct(int? DrugId, string DrugName, string Barcode, int? isEditMode)
		{
			InitializeComponent();

			if (CrossConnectivity.Current.IsConnected)
			{
				_isEditMode = isEditMode;
				_Drugid = DrugId;

				activityIndicator.SetBinding(IsVisibleProperty, "IsBusy");
				activityIndicator.SetBinding(ActivityIndicator.IsRunningProperty, "IsBusy");

				if (isEditMode.HasValue)
				{
					var delete = new ToolbarItem("Sil", "delete.png", () =>
					{
					}, ToolbarItemOrder.Primary);
					ToolbarItems.Add(delete);

					delete.Clicked += async (object sender, EventArgs e) =>
					{
						await Navigation.PushAsync(new DeleteDrugPage(DrugId, DrugName, Barcode, isEditMode, 1));

					};
				}

				datepickerMiat.Date = DateTime.Today.AddMonths(1);
				productName.Text = DrugName;
				productBarcode.Text = Barcode;

				BindingContext = _MyProductViewModel = new MyProductViewModel(this, null, DrugId, isEditMode, null, null, null);

				if (isEditMode.HasValue)
				{
					_MyProductViewModel.SupplyItemCommand.Execute(null);
				}

				btn_kaydet.Clicked += Btn_Kaydet_Clicked;
			}
			else
			{
				DisplayAlert("Uyarı", "Lütfen internet bağlantınızı kontrol ediniz.", "Tamam");
			}

		}



		async void Btn_Kaydet_Clicked(object sender, EventArgs e)
		{
			if (CrossConnectivity.Current.IsConnected)
			{
				int? productPiece = String.IsNullOrEmpty(txtPiece.Text) ? 0 : Convert.ToInt32(txtPiece.Text);

				if (!string.IsNullOrWhiteSpace(txtPrice.Text))
					txtPrice.Text = txtPrice.Text.Replace(",", ".");

				decimal? productPrice = String.IsNullOrEmpty(txtPrice.Text) ? 0 : Convert.ToDecimal(txtPrice.Text);

				DateTime productTerm = Convert.ToDateTime(datepickerMiat.Date);



				if (string.IsNullOrWhiteSpace(txtPiece.Text))
				{
					await DisplayAlert("Uyarı", "Miktar alanı boş bırakılamaz.", "Tamam");
				}
				else if (productPiece < 1)
				{
					await DisplayAlert("Uyarı", "Miktar 0 veya 0'dan küçük olamaz.", "Tamam");
				}
				else if (string.IsNullOrWhiteSpace(txtPrice.Text))
				{
					await DisplayAlert("Uyarı", "Tutar alanı boş bırakılamaz.", "Tamam");
				}
				else if (productPrice < 1)
				{
					await DisplayAlert("Uyarı", "Tutar 0 dan 0'dan küçük olamaz.", "Tamam");
				}
				else if (productTerm < DateTime.Today.AddMonths(+1) && !this.Title.Trim().Equals(("Ürün Güncelle").Trim()))
				{
					await DisplayAlert("Uyarı", "Miat 1 aydan küçük olamaz.", "Tamam");
				}
				else
				{
					BindingContext = _MyProductViewModel = new MyProductViewModel(this, null, _Drugid, _isEditMode, productPiece, productPrice, productTerm.ToString("dd.MM.yyyy"));
					_MyProductViewModel.EditSupplyCommand.Execute(null);
					_MyProductViewModel.SupplyListCommand.Execute(null);
					_MyProductViewModel.SupplyItemCommand.Execute(null);
				}

			}
			else
			{
				await DisplayAlert("Uyarı", "Lütfen internet bağlantınızı kontrol ediniz.", "Tamam");
			}



		}
	}
}
