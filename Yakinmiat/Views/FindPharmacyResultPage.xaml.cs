﻿using System;
using System.Collections.Generic;
 
using Xamarin.Forms;
using Yakinmiyat.SearchPharmacy;

namespace Yakinmiat
{
	public partial class FindPharmacyResultPage : ContentPage
	{

		public SearchPharmacyResultModel selectedListItem;
		List<SearchPharmacyResultModel> resultList;
		public FindPharmacyResultPage(List<SearchPharmacyResultModel> resultList)
		{
			InitializeComponent();
			this.resultList = resultList;
		}

		protected override void OnAppearing()
		{
			base.OnAppearing();

			if (this.resultList != null && this.resultList.Count != 0)
			{
				pharmacyResultList.ItemsSource = this.resultList;
				pharmacyResultList.ItemTemplate = new DataTemplate(typeof(FindPharmacyResultListItem));
			}
			else
			{
				warning.IsVisible = true;
				pharmacyResultList.IsVisible = false;
			}

		}

		public void ResultListItemSelected(object sender, EventArgs e) 
		{
			selectedListItem = pharmacyResultList.SelectedItem as SearchPharmacyResultModel;
			Navigation.PushAsync(new PharmacyDetailPage(selectedListItem.PharmacyId));
		}
	}
}
