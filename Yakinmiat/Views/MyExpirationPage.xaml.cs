﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using Xamarin.Forms;
using Yakinmiat.Drugs;
using Yakinmiyat.SupplyList;

namespace Yakinmiat
{
	public partial class MyExpirationPage : ContentPage
	{
		MyProductViewModel _MyProductViewModel;
		public List<SupplyListModel> supplyData;
		bool isListSet = false;
		public MyExpirationPage()
		{
			InitializeComponent();

			Title = "Ürün Listem";
			Icon = "liste.PNG";



			searchList.ItemTemplate = new DataTemplate(typeof(MyProductListItem));
			searchList.ItemsSource = null;
			searchList.HasUnevenRows = true;
			searchList.SetBinding(ListView.ItemsSourceProperty, "SupplyList");
			searchList.ItemSelected += async (sender, e) =>
			{
				if (searchList.SelectedItem == null)
				{
					return;
				}

				var drug = searchList.SelectedItem as SupplyListModel;
				searchList.SelectedItem = null;
				await Navigation.PushAsync(new AddProduct(drug.DrugId, drug.DrugName, drug.Barcode, drug.Id) { Title="Ürün Güncelle" });
			};

			if (Device.OS== TargetPlatform.Android)
			{
				this.Appearing += Handle_Appearing;
			}



		}

		async void onSearch(object sender, TextChangedEventArgs e)
		{

			var dataIsFull = searchList.ItemsSource as List<SupplyListModel>;
			if (isListSet == false && dataIsFull.Count() > 0)
			{
				supplyData = new List<SupplyListModel>();
				supplyData = searchList.ItemsSource as List<SupplyListModel>;
				isListSet = true;
			}

			if (isListSet)
			{
				if (!String.IsNullOrEmpty(e.NewTextValue) && e.NewTextValue.Length > 3)
				{
					var drugList = supplyData.Where(x => x.DrugName.ToLower().Contains(e.NewTextValue.ToLower()) || x.Barcode.Contains(e.NewTextValue.ToLower())).ToList();

					searchList.ItemsSource = drugList;
				}
				else searchList.ItemsSource = supplyData;
			}

		}

		protected override void OnAppearing()
		{
			base.OnAppearing();
			BindingContext = _MyProductViewModel = new MyProductViewModel(this, null, null, null, null, null, null);
			_MyProductViewModel.SupplyListCommand.Execute(null);
		}


		void Handle_Appearing(object sender, EventArgs e)
		{
			BindingContext = _MyProductViewModel = new MyProductViewModel(this, null, null, null, null, null, null);
			_MyProductViewModel.SupplyListCommand.Execute(null);
		}
	}
}
