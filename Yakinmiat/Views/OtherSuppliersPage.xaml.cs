﻿using System;
using System.Collections.Generic;
using System.Linq;
using Plugin.Connectivity;
using Xamarin.Forms;
using Yakinmiyat.SearchDrug;

namespace Yakinmiat
{
	public partial class OtherSuppliersPage : ContentPage
	{
		MyRequestViewModel _MyRequestViewModel;
		List<SearchDrugModel> _OtherSuppliers;
		public OtherSuppliersPage(List<SearchDrugModel> OtherSuppliers)
		{
			InitializeComponent();
			searchList.ItemsSource = OtherSuppliers;
			_OtherSuppliers = OtherSuppliers;

			if (!CrossConnectivity.Current.IsConnected)
			{
				DisplayAlert("Uyarı", "Lütfen internet bağlantınızı kontrol ediniz.", "Tamam");
			}

		}

		public void AddFollowList(object sender, EventArgs e)
		{
			var item = (Xamarin.Forms.Button)sender;
			var SelectedItem = _OtherSuppliers.Where(x => x.Id == item.CommandParameter as int?).FirstOrDefault();

			BindingContext = _MyRequestViewModel = new MyRequestViewModel(this, SelectedItem.DrugId, null, null, item.CommandParameter as int?, SelectedItem.Date);
			_MyRequestViewModel.AddFollowCommand.Execute(null);
		}

	}
}
