﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;
using Yakinmiyat.Models;
using Yakinmiyat.Users;

namespace Yakinmiat
{
	public partial class ContactPage : ContentPage
	{

		public DataBaseModel d;
		public UserInfoModel userIModel;

		public ContactPage()
		{
			InitializeComponent();

			using (var sqlLiteDB = new DBService())
			{
				d = sqlLiteDB.Data();
				if (d != null)
				{
					userIModel = Newtonsoft.Json.JsonConvert.DeserializeObject<UserInfoModel>(d.UserInfo);
				}

			}

		}


		public void FireNotification(object sender, EventArgs e) 
		{
			DependencyService.Get<IAndroidNotify>().AndroidNotify();
			//BugService.SendBugMessage(userIModel.UserId, d.DeviceUniqueId, "ex.Message", "ex.StackTrace");

		}
	}

}
