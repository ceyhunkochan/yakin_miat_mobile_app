﻿using System;
using System.Collections.Generic;
using Plugin.Connectivity;
using Xamarin.Forms;

namespace Yakinmiat
{
	public partial class FollowListPage : ContentPage
	{
		MyRequestViewModel _MyRequestViewModel;
		public FollowListPage()
		{
			InitializeComponent();

			if (CrossConnectivity.Current.IsConnected)
			{
				searchList.SetBinding(ListView.ItemsSourceProperty, "FollowCart");
				//searchList.IsEnabled = false;

				searchList.ItemSelected += (sender, e) =>
				 {
					 if (searchList.SelectedItem == null)
					 {
						 return;
					 }
					 else
					 {
						 searchList.SelectedItem = null;
						 return;
					 }
				};
			}
			else
			{
				DisplayAlert("Uyarı", "Lütfen internet bağlantınızı kontrol ediniz.", "Tamam");
			}

		}

		public async void Delete(object sender, EventArgs e)
		{
			var item = (Xamarin.Forms.Button)sender;
			BindingContext = _MyRequestViewModel = new MyRequestViewModel(this, null, null, null, item.CommandParameter as int?, null);
			_MyRequestViewModel.DeleteFollowCommand.Execute(null);
		}

		protected override void OnAppearing()
		{
			base.OnAppearing();

			BindingContext = _MyRequestViewModel = new MyRequestViewModel(this, null, null, null, null, null);
			_MyRequestViewModel.FollowListCommand.Execute(null);

		}
	}
}
