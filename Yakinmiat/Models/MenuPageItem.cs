﻿using System;
using Xamarin.Forms;
namespace Yakinmiat
{
	public class MenuPageItem
	{
		public string Title { get; set; }

		public string IconSource { get; set; }

		//public Type TargetType { get; set; }
		public Page Redirect { get; set;}

		public string PageName { get; set;}
	}
}
