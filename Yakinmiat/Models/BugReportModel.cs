﻿using System;
using System.Collections.Generic;

namespace Yakinmiyat.Models
{
     

	public class BugReportModel : IApiUser
	{
		public BugReportModel()
		{
			Error = new List<ErrorViewModel>();
			apiPass = "epsApiPass*!";
			apiUser = "epsApiUser!";
		}

		public string Log { get; set; }
		public string ErrorMessage { get; set; }
		public string UserID { get; set; }
		public string DeviceID { get; set; }


		public bool isErrorHappend { get; set; }
		public List<ErrorViewModel> Error { get; set; }
		 
		public string apiUser { get; set; }
		public string apiPass { get; set; }
		 

	}

}