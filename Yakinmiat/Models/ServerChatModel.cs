﻿using System;
using Xamarin.Forms;

namespace Yakinmiat
{
	public class ServerChatModel
	{
		public string UserId
		{
			get;
			set;
		}
		public string InOrOutPost
		{
			get;
			set;
		}
		public string PharmacyName
		{
			get;
			set;
		}
		public string Message
		{
			get;
			set;
		}
		public string Image
		{
			get;
			set;
		}
		public bool isSeen
		{
			get;
			set;
		}
		public string SendDate
		{
			get;
			set;
		}
		public string ReceiverHubId
		{
			get;
			set;
		}
		public LayoutOptions Alignment
		{
			get;
			set;
		}
	}
}
