﻿using System;
using System.Collections.Generic;
using System.Linq;
using Yakinmiyat.Models;

namespace Yakinmiyat.SearchDrug
{
    public class SearchDrugModel:IApiUser
    {
        public SearchDrugModel()
        {
            Error = new List<ErrorViewModel>();
        }
        public string SearchDrug { get; set; }
        public string UserId { get; set; }
        public int Id { get; set; }
        public string DrugName { get; set; }
        public int? DrugId { get; set; }
        public int Piece { get; set; }
        public string Date { get; set; }
        public string Pharmacy { get; set; }
        public string PharmacyId { get; set; }
        public string Country { get; set; }
        public string Town { get; set; }
        public bool isErrorHappend { get; set; }
        public List<ErrorViewModel> Error { get; set; }
        #region Api User
        public string apiUser { get; set; }
        public string apiPass { get; set; }
        #endregion
    }
}