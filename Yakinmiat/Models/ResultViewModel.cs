﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Yakinmiyat.Models
{
    public class ResultViewModel : IApiUser
    {
        public ResultViewModel()
        {
            Error = new List<ErrorViewModel>();
        }
        public string Message { get; set; }
        public bool isErrorHappend { get; set; }
        public List<ErrorViewModel> Error { get; set; }
        #region Api User
        public string apiUser { get; set; }
        public string apiPass { get; set; }
        #endregion
    }
}