﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Yakinmiyat.Models
{
    public interface IApiUser
    {
        string apiUser { get; set; }
        string apiPass { get; set; }
    }
}
