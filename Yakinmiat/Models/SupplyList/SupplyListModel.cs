﻿using System;
using System.Collections.Generic;
using System.Linq;
using Yakinmiyat.Models;

namespace Yakinmiyat.SupplyList
{
    public class SupplyListModel : IApiUser
    {
        public SupplyListModel()
        {
            Error = new List<ErrorViewModel>();
        }
    
        public int Id { get; set; }
        public string DrugName { get; set; }
        public int Piece { get; set; }
        public string Date { get; set; }
        public string UserId { get; set; }
        public int? DrugId { get; set; }
		public string Price
		{
			get;
			set;
		}
		public string Barcode
		{
			get;
			set;
		}
        public bool isErrorHappend { get; set; }
        public List<ErrorViewModel> Error { get; set; }
        #region Api User
        public string apiUser { get; set; }
        public string apiPass { get; set; }
        #endregion
    }
}