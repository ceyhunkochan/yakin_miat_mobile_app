﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Yakinmiyat.Models
{
    public class ErrorViewModel
    {
        public string ErrorName { get; set; }
        public string Error { get; set; }
    }
}