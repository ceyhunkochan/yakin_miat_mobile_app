﻿using System;
using System.Collections.Generic;
using System.Linq;
using Yakinmiyat.Models;
using Yakinmiyat.SupplyList;
using Yakinmiyat.WishList;

namespace Yakinmiyat.Profile
{
    public class ProfileModel : IApiUser
    {
        public ProfileModel()
        {
            Error = new List<ErrorViewModel>();
            SupplyList = new List<SupplyListModel>();
            WishList = new List<WishListModel>();
        }
        public string PharmacyId { get; set; }
        public string NameSurname { get; set; }
        public string PharmacyName { get; set; }
        public string PharmacyCode { get; set; }
        public string Phone1 { get; set; }
        public string Phone2 { get; set; }
        public string Fax { get; set; }
        public string CellPhone { get; set; }
        public string Address { get; set; }
        public string Country { get; set; }
        public string Town { get; set; }
        public bool isFavorite { get; set; }
        public string ProfilePicture { get; set; }
        public List<SupplyListModel> SupplyList { get; set; }
        public List<WishListModel> WishList { get; set; }
        public bool isErrorHappend { get; set; }
        public List<ErrorViewModel> Error { get; set; }
        #region Api User
        public string apiUser { get; set; }
        public string apiPass { get; set; }
        #endregion
    }
}