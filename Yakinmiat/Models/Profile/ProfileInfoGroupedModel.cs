﻿using System;
namespace Yakinmiat
{
	public class ProfileInfoGroupedModel
	{
		public ProfileInfoGroupedModel()
		{
			Title = "";
			Subtitle = "";
		}
		public string Title
		{
			get;
			set;
		}
		public string Subtitle
		{
			get;
			set;
		}
	}
}
