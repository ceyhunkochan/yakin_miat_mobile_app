﻿using System;
using System.Collections.Generic;
using System.Linq;
using Yakinmiyat.Models;

namespace Yakinmiyat.Favorite
{
    public class FavoriteModel : IApiUser
    {
        public FavoriteModel()
        {
            Error = new List<ErrorViewModel>();
        }

        public int? Id { get; set; }
        public string Pharmacy { get; set; }
        public string PharmacyId { get; set; }
        public string NameSurname { get; set; }
        public string CellPhone { get; set; }
        public string Phone1 { get; set; }
        public string Country { get; set; }
        public bool isErrorHappend { get; set; }
        public List<ErrorViewModel> Error { get; set; }
        #region Api User
        public string apiUser { get; set; }
        public string apiPass { get; set; }
        #endregion
    }
}