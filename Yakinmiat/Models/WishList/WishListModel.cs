﻿using System;
using System.Collections.Generic;
using System.Linq;
using Yakinmiyat.Models;
using Yakinmiyat.SearchDrug;

namespace Yakinmiyat.WishList
{
    public class WishListModel : IApiUser
    {
        public WishListModel()
        {
            Error = new List<ErrorViewModel>();
			OtherSuppliers = new List<SearchDrugModel>();
        }
        public int Id { get; set; }
        public int? DrugId { get; set; }
        public string DrugName { get; set; }
        public int Piece { get; set; }
        public string Date { get; set; }
        public string Pharmacy { get; set; }
        public bool Favorite { get; set; }
        public string PharmacyId { get; set; }
		public string Barcode
		{
			get;
			set;
		}
		public List<SearchDrugModel> OtherSuppliers
		{
			get;
			set;
		}
		public bool isOtherHave
		{
			get;
			set;
		}
        public bool isErrorHappend { get; set; }
        public List<ErrorViewModel> Error { get; set; }
        #region Api User
        public string apiUser { get; set; }
        public string apiPass { get; set; }
        #endregion
    }
}