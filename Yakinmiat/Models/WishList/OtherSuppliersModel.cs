﻿using System;
using System.Collections.Generic;
using System.Linq;
using Yakinmiyat.Models;

namespace Yakinmiyat.WishList
{
    public class OtherSuppliersModel : IApiUser
    {

		//istek listem - tüm eczaneler
		//PharmacyId ve DrugId gönderilecek


        public OtherSuppliersModel()
        {
            Error = new List<ErrorViewModel>();
        }
        public string PharmacyId { get; set; }
        public int? DrugId { get; set; }
        public bool isErrorHappend { get; set; }
        public List<ErrorViewModel> Error { get; set; }
        #region Api User
        public string apiUser { get; set; }
        public string apiPass { get; set; }
        #endregion
    }
}