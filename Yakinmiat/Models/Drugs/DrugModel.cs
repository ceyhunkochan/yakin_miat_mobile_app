﻿using System;
using System.Collections.Generic;
using System.Linq;
using Xamarin.Forms;

namespace Yakinmiat.Drugs
{
    public class DrugModel 
    {
        public int? Id { get; set; }
        public string Name { get; set; }
        public string Barcode { get; set; }
        public int? Count { get; set; }
        public string Date { get; set; }
    }
}