﻿using System;
namespace Yakinmiat
{
	public class QRCodeModel
	{
		public string Barcode
		{
			get;
			set;
		}
		public string DrugName
		{
			get;
			set;
		}
		public string Date
		{
			get;
			set;
		}
		public int? DrugId
		{
			get;
			set;
		}
	}
}
