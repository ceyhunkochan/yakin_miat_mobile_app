﻿using System;
using System.Collections.Generic;
using System.Linq;
using Yakinmiyat.Models;

namespace Yakinmiyat.SearchPharmacy
{
    public class SearchPharmacyResultModel : IApiUser
    {
        public SearchPharmacyResultModel()
        {

			apiPass = "epsApiPass*!";
			apiUser = "epsApiUser!";
            Error = new List<ErrorViewModel>();
        }
        public string PharmacyId { get; set; }
        public string PharmacyName { get; set; }
        public string PharmacistNameSurname { get; set; }
        public string Country { get; set; }
        public string Town { get; set; }
        public bool isPharmacyFavorite { get; set; }
        public bool isErrorHappend { get; set; }
        public List<ErrorViewModel> Error { get; set; }
        #region Api User
        public string apiUser { get; set; }
        public string apiPass { get; set; }
        #endregion
    }
}