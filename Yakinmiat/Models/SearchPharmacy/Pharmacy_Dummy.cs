﻿using System;
namespace Yakinmiat
{
	public class Pharmacy_Dummy
	{
		public string PharmacyId { get; set; }
		public string pharmacy { get; set; }
		public string dueDate { get; set; }
		public string amount { get; set; }
		public string city { get; set; }
		public string state { get; set; }
	}
}
