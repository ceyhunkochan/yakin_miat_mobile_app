﻿using System;
namespace Yakinmiat
{
	public class FollowCartModel
	{
		public int? Id
		{
			get;
			set;
		}
		public string Pharmacy
		{
			get;
			set;
		}
		public int? DrugId
		{
			get;
			set;
		}
		public string Drug
		{
			get;
			set;
		}
		public string Term
		{
			get;
			set;
		}
		public int? Piece
		{
			get;
			set;
		}
		public string PharmacyId
		{
			get;
			set;
		}
		public string Price
		{
			get;
			set;
		}
	}
}
