﻿using System;
using System.Collections.Generic;
using Yakinmiyat.Models;

namespace Yakinmiat
{
	public class ConstantModel
	{
		public ConstantModel()
		{
			City = new List<SelectViewModel>();
			Brand = new List<SelectViewModel>();
			SupplyDeleteOptions = new List<SelectViewModel>();
			WishDeleteOptions = new List<SelectViewModel>();
		}

		public List<SelectViewModel> City
		{
			get;
			set;
		}
		public List<SelectViewModel> Brand
		{
			get;
			set;
		}
		public List<SelectViewModel> SupplyDeleteOptions
		{
			get;
			set;
		}
		public List<SelectViewModel> WishDeleteOptions
		{
			get;
			set;
		}
	}
}
