﻿using System;
using System.Collections.Generic;
using System.Linq;
using Yakinmiyat.Models;

namespace Yakinmiyat.Users
{
    public class DeviceRegisterModel : IApiUser
    {
        public DeviceRegisterModel()
        {
            Error = new List<ErrorViewModel>();
        }
        public string UserId { get; set; }
        public string Device { get; set; }
        public string Token { get; set; }
        public string DeviceUniqueId { get; set; }
        public bool isErrorHappend { get; set; }
        public List<ErrorViewModel> Error { get; set; }

        #region Api User
        public string apiUser { get; set; }
        public string apiPass { get; set; }
        #endregion
    }
}