﻿using System;
using System.Collections.Generic;
using System.Linq;
using Yakinmiyat.Models;

namespace Yakinmiyat.Users
{
    public class UserInfoModel : IApiUser
    {
        public UserInfoModel()
        {
            Error = new List<ErrorViewModel>();
        }
        public string UserId { get; set; }
        public string PharmacyName { get; set; }
        public string PharmacistName { get; set; }
        public string PharmacistSurname { get; set; }
        public string GLNNo { get; set; }
        public string Email { get; set; }
        public string Phone1 { get; set; }
        public string Phone2 { get; set; }
        public string Fax { get; set; }
        public string CellPhone { get; set; }
        public string Address { get; set; }
        public int? CityId { get; set; }
        public int? TownId { get; set; }
        public string MembershipStartDate { get; set; }
        public string MembershipEndDate { get; set; }
        public string RemainingTime { get; set; }
        public string PhotoPath { get; set; }
        public bool isErrorHappend { get; set; }
        public List<ErrorViewModel> Error { get; set; }
        #region Api User
        public string apiUser { get; set; }
        public string apiPass { get; set; }
        #endregion
    }

}