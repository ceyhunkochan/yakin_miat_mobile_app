﻿using System;
using System.Collections.Generic;
using System.Linq;
using Yakinmiyat.Models;

namespace Yakinmiyat.Users
{
    public class LoginModel : IApiUser
    {
        public LoginModel()
        {
            Error = new List<ErrorViewModel>();
			apiPass = "epsApiPass*!";
			apiUser = "epsApiUser!";
        }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string Device { get; set; }
        public string IP { get; set; }
        public bool isErrorHappend { get; set; }
        public List<ErrorViewModel> Error { get; set; }
        #region Api User
        public string apiUser { get; set; }
        public string apiPass { get; set; }
        #endregion
    }
}