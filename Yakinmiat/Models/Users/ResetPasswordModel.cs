﻿using System;
using System.Collections.Generic;
using System.Linq;
using Yakinmiyat.Models;

namespace Yakinmiyat.Users
{
    public class ResetPasswordModel :IApiUser
    {
        public ResetPasswordModel()
        {
            Error = new List<ErrorViewModel>();
        }
        public string KullaniciAdi { get; set; }
        public string EmailAdresi { get; set; }
        public bool isErrorHappend { get; set; }
        public List<ErrorViewModel> Error { get; set; }
        #region Api User
        public string apiUser { get; set; }
        public string apiPass { get; set; }
        #endregion
    }
}