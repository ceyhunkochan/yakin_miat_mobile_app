﻿using System;
using SQLite.Net;
using SQLite.Net.Attributes;

namespace Yakinmiat
{

	public class DataBaseModel
	{
		[PrimaryKey, AutoIncrement]
		public int ID { get; set; }
		public string UserName { get; set; }
		public string Password { get; set; }
		public string UserInfo { get; set; }
		public string RegistrationId { get; set; }
		public string DeviceUniqueId { get; set; }
		public string AppliVersion { get; set; }
		public override string ToString()
		{
			return string.Format("[DataBaseModel: ID={0}, UserName={1}, Password={2}, UserInfo={3}, RegistrationId={4}, DeviceUniqueId={5}, AppliVersion={6}]", ID, UserName, Password, UserInfo, RegistrationId, DeviceUniqueId, AppliVersion);
		}
	}
}

