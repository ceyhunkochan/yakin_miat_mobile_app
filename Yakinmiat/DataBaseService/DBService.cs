﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using SQLite.Net;
using Xamarin.Forms;
using Yakinmiat;

namespace Yakinmiat
{
	public class DBService : IDisposable
	{
		private SQLiteConnection connection;
		public DBService()
		{
			var config = DependencyService.Get<ISQLite>();
			connection = new SQLiteConnection(config.Platform, Path.Combine(config.Directory, "Yakinmiat.db3"));

			connection.CreateTable<DataBaseModel>();
		}



		public DataBaseModel Data()
		{
			return connection.Table<DataBaseModel>().FirstOrDefault();
		}

		public void Insert(DataBaseModel model)
		{
			connection.Insert(model);
		}

		public void Update(DataBaseModel model)
		{
			connection.Update(model);
		}

		public void Delete()
		{
			connection.DeleteAll<DataBaseModel>();
		}

		public void Dispose()
		{
			connection.Dispose();
		}
	}
}

