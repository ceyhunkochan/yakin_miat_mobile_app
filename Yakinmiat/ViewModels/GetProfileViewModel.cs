﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xamarin.Forms;
using Yakinmiyat.Profile;
using Yakinmiyat.Users;

namespace Yakinmiat
{
	public class GetProfileViewModel : BaseViewModel
	{
		private Command getProfileCommand;
		private Command phoneCallCell;
		private Command phoneCallPhone1;
		private Command phoneCallPhone2;
		private Command setFavCommand;
		private Command sendMessageCommand;
		public string _profileId;
		List<ProfileInfoGroupedModel> profileInfo;
		public const string ProfileInfoPropertyName = "ProfileInfo";
		public List<ProfileInfoGroupedModel> ProfileInfo
		{
			get { return profileInfo; }
			set { SetProperty(ref profileInfo, value, ProfileInfoPropertyName); }
		}




		public DataBaseModel d;
		public UserInfoModel userIModel;


		public GetProfileViewModel(Page page, string profileId) : base(page)
		{
			this._profileId = profileId;
			this.ProfileInfo = new List<ProfileInfoGroupedModel>();

			using (var sqlLiteDB = new DBService())
			{
				d = sqlLiteDB.Data();
				if (d != null)
				{
					userIModel = Newtonsoft.Json.JsonConvert.DeserializeObject<UserInfoModel>(d.UserInfo);
				}
			}

		}




		string profilePicture = string.Empty;
		public const string ProfilePicturePropertyName = "ProfilePicture";
		public string ProfilePicture
		{
			get { return profilePicture; }
			set { SetProperty(ref profilePicture, value, ProfilePicturePropertyName); }
		}

		string pharmacyName = string.Empty;
		public const string PharmacyNamePropertyName = "PharmacyName";
		public string PharmacyName
		{
			get { return pharmacyName; }
			set { SetProperty(ref pharmacyName, value, PharmacyNamePropertyName); }
		}

		string pharmacistName = string.Empty;
		public const string PharmacistNamePropertyName = "PharmacistName";
		public string PharmacistName
		{
			get { return pharmacistName; }
			set { SetProperty(ref pharmacistName, value, PharmacistNamePropertyName); }
		}


		Color isPharmacyFav;
		public const string IsPharmacyFavPropertyName = "IsPharmacyFav";
		public Color IsPharmacyFav
		{
			get { return isPharmacyFav; }
			set { SetProperty(ref isPharmacyFav, value, IsPharmacyFavPropertyName); }
		}

		string isPharmacyFavText;
		public const string IsPharmacyFavTextPropertyName = "IsPharmacyFavText";
		public string IsPharmacyFavText
		{
			get { return isPharmacyFavText; }
			set { SetProperty(ref isPharmacyFavText, value, IsPharmacyFavTextPropertyName); }
		}

		bool pharmacyFavorite = false;
		public const string PharmacyFavoritePropertyName = "PharmacyFavorite";
		public bool PharmacyFavorite
		{
			get { return pharmacyFavorite; }
			set { SetProperty(ref pharmacyFavorite, value, PharmacyFavoritePropertyName); }
		}

		public Command GetProfileCommand
		{
			get
			{
				return getProfileCommand ??
					(getProfileCommand = new Command(async () => await ExecuteGetProfileCommand(), () => { return !IsBusy; }));
			}
		}

		public Command PhoneCallCell
		{
			get
			{
				return phoneCallCell ??
					(phoneCallCell = new Command(async () => await ExecutePhoneCallCell(), () => { return !IsBusy; }));
			}
		}

		public Command PhoneCallPhone1
		{
			get
			{
				return phoneCallPhone1 ??
					(phoneCallPhone1 = new Command(async () => await ExecutePhoneCallPhone1(), () => { return !IsBusy; }));
			}
		}

		public Command PhoneCallPhone2
		{
			get
			{
				return phoneCallPhone2 ??
					(phoneCallPhone2 = new Command(async () => await ExecutePhoneCallPhone2(), () => { return !IsBusy; }));
			}
		}

		public Command SetFavCommand
		{
			get
			{
				return setFavCommand ??
					(setFavCommand = new Command(async () => await ExecuteSetFavCommand(), () => { return !IsBusy; }));
			}
		}

		public Command SendMessageCommand
		{
			get
			{
				return sendMessageCommand ??
					(sendMessageCommand = new Command(async () => await ExecuteSendMessageCommand(), () => { return !IsBusy; }));
			}
		}

		private async Task ExecutePhoneCallCell()
		{

			phoneCallCell.ChangeCanExecute();

			//if (!String.IsNullOrEmpty(this.PharmacyCellPhone))
			//{
			//	DependencyService.Get<IPhoneCallTask>().MakePhoneCall(this.PharmacyCellPhone);
			//}
		}

		private async Task ExecutePhoneCallPhone1()
		{

			phoneCallPhone1.ChangeCanExecute();

			//if (!String.IsNullOrEmpty(this.PharmacyPhone1))
			//{
			//	DependencyService.Get<IPhoneCallTask>().MakePhoneCall(this.PharmacyPhone1);
			//}
		}

		private async Task ExecutePhoneCallPhone2()
		{

			phoneCallPhone2.ChangeCanExecute();

			//if (!String.IsNullOrEmpty(this.PharmacyPhone2))
			//{
			//	DependencyService.Get<IPhoneCallTask>().MakePhoneCall(this.PharmacyPhone2);
			//}
		}

		private async Task ExecuteSetFavCommand()
		{
			if (IsBusy)
				return;

			IsBusy = true;

			setFavCommand.ChangeCanExecute();



			try
			{
				if (this.PharmacyFavorite)
				{
					this.IsPharmacyFav = Color.FromHex("#e7505a");
					this.IsPharmacyFavText = "Favori Çıkar";
					//var favResult = await ProfileController.SetFavorite("", "", UserBaseId, _profileId, true);
					//var returnModel = await ProfileController.GetProfile("", "", userid: UserBaseId, profileId: _profileId);
					this.PharmacyFavorite = true;

				}
				else
				{
					this.IsPharmacyFav = Color.FromHex("#32c5d2");
					this.IsPharmacyFavText = "Favori Ekle";
					//var favResult = await ProfileController.SetFavorite("", "", UserBaseId, _profileId, false);
					//var returnModel = await ProfileController.GetProfile("", "", userid: UserBaseId, profileId: _profileId);
					this.PharmacyFavorite = false;

				}
				IsBusy = false;
			}
			catch (Exception ex)
			{
				BugService.SendBugMessage(userIModel.UserId, d.DeviceUniqueId, ex.Message, ex.StackTrace);
			}
			finally
			{
				IsBusy = false;
			}
		}

		private async Task ExecuteSendMessageCommand()
		{
			sendMessageCommand.ChangeCanExecute();

			await page.DisplayAlert("Uyarı", "Mesaj Button Click", "Tamam");
		}

		private async Task ExecuteGetProfileCommand()
		{
			if (IsBusy)
				return;

			IsBusy = true;

			getProfileCommand.ChangeCanExecute();

			try
			{

				var returnModel = await ProfileController.GetProfile("", "", userid: UserBaseId, profileId: _profileId);
				page.Title = "Eczane Bilgileri";
				this.ProfilePicture = returnModel.ProfilePicture;
				this.PharmacyName = returnModel.PharmacyName + " Eczanesi";
				this.PharmacistName = returnModel.NameSurname;
				this.PharmacyFavorite = returnModel.isFavorite;

				if (returnModel.isFavorite)
				{
					this.IsPharmacyFav = Color.FromHex("#e7505a");

					this.IsPharmacyFavText = "Favori Çıkar";
				}
				else
				{
					this.IsPharmacyFav = Color.FromHex("#32c5d2");
					this.IsPharmacyFavText = "Favori Ekle";
				}


				this.ProfileInfo.Add(new ProfileInfoGroupedModel { Subtitle = returnModel.PharmacyCode, Title = "Eczane GLN Numarası" });

				if (returnModel.CellPhone[0] != '0')
				{
					this.ProfileInfo.Add(new ProfileInfoGroupedModel { Subtitle = "+90" + returnModel.CellPhone.Replace("(", "").Replace(")", "").Replace(" ", ""), Title = "Cep Telefonu" });
				}
				else
				{
					this.ProfileInfo.Add(new ProfileInfoGroupedModel { Subtitle = "+9" + returnModel.CellPhone.Replace("(", "").Replace(")", "").Replace(" ", ""), Title = "Cep Telefonu" });
				}
				if (returnModel.Fax[0] != '0')
				{
					this.ProfileInfo.Add(new ProfileInfoGroupedModel { Subtitle = "+90" + returnModel.Fax.Replace("(", "").Replace(")", "").Replace(" ", ""), Title = "Fax Numarası" });
				}
				else
				{
					this.ProfileInfo.Add(new ProfileInfoGroupedModel { Subtitle = "+9" + returnModel.Fax.Replace("(", "").Replace(")", "").Replace(" ", ""), Title = "Fax Numarası" });
				}
				if (returnModel.Phone1[0] != '0')
				{
					this.ProfileInfo.Add(new ProfileInfoGroupedModel { Subtitle = "+90" + returnModel.Phone1.Replace("(", "").Replace(")", "").Replace(" ", ""), Title = "Diğer Telefon - 1" });
				}
				else
				{
					this.ProfileInfo.Add(new ProfileInfoGroupedModel { Subtitle = "+9" + returnModel.Phone1.Replace("(", "").Replace(")", "").Replace(" ", ""), Title = "Diğer Telefon - 1" });
				}
				if (returnModel.Phone2[0] != '0')
				{
					this.ProfileInfo.Add(new ProfileInfoGroupedModel { Subtitle = "+90" + returnModel.Phone2.Replace("(", "").Replace(")", "").Replace(" ", ""), Title = "Diğer Telefon - 2" });
				}
				else
				{
					this.ProfileInfo.Add(new ProfileInfoGroupedModel { Subtitle = "+9" + returnModel.Phone2.Replace("(", "").Replace(")", "").Replace(" ", ""), Title = "Diğer Telefon - 2" });
				}
				this.ProfileInfo.Add(new ProfileInfoGroupedModel { Subtitle = returnModel.Address, Title = "Adres" });
				this.ProfileInfo.Add(new ProfileInfoGroupedModel { Subtitle = returnModel.Country, Title = "İl - İlçe" });
				IsBusy = false;
			}
			catch (Exception ex)
			{
				BugService.SendBugMessage(userIModel.UserId, d.DeviceUniqueId, ex.Message, ex.StackTrace);
			}
			finally
			{
				IsBusy = false;
			}

		}
	}
}
