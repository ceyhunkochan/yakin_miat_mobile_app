﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xamarin.Forms;
using Yakinmiyat.SearchDrug;
using Yakinmiyat.SearchPharmacy;

namespace Yakinmiat
{
	public class FindPharmacyResultViewModel : BaseViewModel
	{
		private Command getSearchDrugListCommand;
		public int? _DrugId;
		public List<SearchPharmacyResultModel> searchModel;
		public FindPharmacyResultViewModel(Page page, List<SearchPharmacyResultModel> searchModel) : base(page)
		{
			this.searchModel = searchModel;
		}

		List<SearchDrugModel> searchPharmacyList;
		public const string SearchPharmacyPropertyName = "SearchPharmacyList";
		public List<SearchDrugModel> SearchDrugList
		{
			get { return searchPharmacyList; }
			set { SetProperty(ref searchPharmacyList, value, SearchPharmacyPropertyName); }
		}

		public Command GetSearchDrugListCommand
		{
			get
			{
				return getSearchDrugListCommand ??
					(getSearchDrugListCommand = new Command(async () => await ExecuteGetSearchDrugListCommand(), () => { return !IsBusy; }));
			}
		}
		private async Task ExecuteGetSearchDrugListCommand()
		{
			if (IsBusy)
				return;

			IsBusy = true;

			getSearchDrugListCommand.ChangeCanExecute();

			try
			{


				SearchDrugModel postModel = new SearchDrugModel();
				postModel.SearchDrug = _DrugId.ToString();
				postModel.UserId = UserBaseId;

				var returnModel = await SearchDrugController.SearchDrug(postModel);
				this.SearchDrugList = new List<SearchDrugModel>();

				if (returnModel.Count <= 0)
				{
					await page.DisplayAlert("Uyarı", "Ürün hiçbir eczane de bulunamadı.", "Tamam");
					await page.Navigation.PopAsync();
				}
				else {
					this.SearchDrugList.AddRange(returnModel);
				}
				IsBusy = false;

			}
			catch (Exception ex)
			{
				IsBusy = false;
				await page.DisplayAlert("Uyarı", "Ürün arama sırasında bir sorun oluştu.", "Tamam");
			}

		}
	}
}
