﻿using System;
using System.Collections.ObjectModel;
using Xamarin.Forms;

namespace Yakinmiat
{
	public class ChatViewModel : BaseViewModel
	{
		public string _userid;
		public static string _pharmacyname;
		private ObservableCollection<ServerChatModel> _messages;

		public ObservableCollection<ServerChatModel> Messages
		{
			get { return _messages; }
			set
			{
				_messages = value;
				OnPropertyChanged("Messages");
			}
		}

		private ObservableCollection<ActiveChatUserModel> _conversation;

		public ObservableCollection<ActiveChatUserModel> Conversations
		{
			get { return _conversation; }
			set
			{
				_conversation = value;
				OnPropertyChanged("Conversations");
			}
		}

		string message = null;
		public const string MessagePropertyName = "Message";
		public string Message
		{
			get { return message; }
			set { SetProperty(ref message, value, MessagePropertyName); }
		}


		public ChatViewModel(Page page, string userid, string pharmacyname) : base(page)
		{
			this._userid = userid;
			_pharmacyname = pharmacyname != null ? pharmacyname.Replace(" Eczanesi", "") : null;
			_messages = new ObservableCollection<ServerChatModel>();
			_conversation = new ObservableCollection<ActiveChatUserModel>();
			App._chatServices.OnMessageReceived += _chatServices_OnMessageReceived;
		}


		Command getConversationCommand;
		public Command GetConversationCommand
		{
			get
			{
				return getConversationCommand ??
				(getConversationCommand = new Command(ExecuteGetConversationCommand));
			}
		}

		Command getUserConversationCommand;
		public Command GetUserConversationCommand
		{
			get
			{
				return getUserConversationCommand ??
				(getUserConversationCommand = new Command(ExecuteGetUserConversationCommand));
			}
		}

		public Command sendMessageCommand;
		public Command SendMessageCommand
		{
			get
			{
				return sendMessageCommand ??
				(sendMessageCommand = new Command(ExecuteSendMessageCommand));
			}
		}

		async void ExecuteSendMessageCommand()
		{
			IsBusy = true;

			ChatController.SendMessage(UserBaseId, message, _userid);
			var chatItem = new ServerChatModel();

			chatItem.Alignment = LayoutOptions.Start;
			chatItem.PharmacyName = UserBaseInfo.PharmacyName;
			chatItem.Message = this.Message;
			chatItem.InOrOutPost = "in";


			_messages.Add(chatItem);
			IsBusy = false;
		}

		async void ExecuteGetUserConversationCommand()
		{
			IsBusy = true;

			var userConversation = await ChatController.GetUserConversation(UserBaseId, _userid);
			foreach (var item in userConversation)
			{
				var chatItem = new ServerChatModel();
				if (item.InOrOutPost == "post in")
				{
					chatItem.Alignment = LayoutOptions.Start;
				}
				else chatItem.Alignment = LayoutOptions.End;

				chatItem.PharmacyName = item.PharmacyName;
				chatItem.Message = item.Message;
				chatItem.InOrOutPost = item.InOrOutPost;
				chatItem.Image = item.Image;

				_messages.Add(chatItem);
			}

			IsBusy = false;
		}

		async void ExecuteGetConversationCommand()
		{
			IsBusy = true;

			var conversation = await ChatController.GetConversation(UserBaseId);
			foreach (var item in conversation)
			{
				item.Image = "http://yakinmiat.com" + item.Image;
				item.PharmacyName = item.PharmacyName + " Eczanesi";
				_conversation.Add(item);
			}

			IsBusy = false;
		}

		void _chatServices_OnMessageReceived(object sender, ChatMessage e)
		{
			if (_pharmacyname == e._name && _pharmacyname != null)
			{
				var chatItem = new ServerChatModel();
				if (e._dir == "out")
				{
					chatItem.Alignment = LayoutOptions.End;
				}
				else chatItem.Alignment = LayoutOptions.Start;

				chatItem.PharmacyName = e._name;
				chatItem.Message = e._message;
				chatItem.InOrOutPost = e._dir;
				chatItem.Image = e._avatar;

				_messages.Add(chatItem);
			}

		}
	}
}
