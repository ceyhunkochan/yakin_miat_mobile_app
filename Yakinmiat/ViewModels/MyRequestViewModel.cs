﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xamarin.Forms;
using Yakinmiyat.Users;
using Yakinmiyat.WishList;

namespace Yakinmiat
{
	public class MyRequestViewModel : BaseViewModel
	{
		int? drugId;
		int? _id;
		int? _piece;
		int? _followId;
		string _followDate;
		protected bool addDrug = true;


		public static List<WishListModel> searchList;
		List<WishListModel> wishList;
		public const string WishListPropertyName = "WishList";
		public List<WishListModel> WishList
		{
			get { return wishList; }
			set { SetProperty(ref wishList, value, WishListPropertyName); }
		}

		List<FollowCartModel> followCart;
		public const string FollowCartPropertyName = "FollowCart";
		public List<FollowCartModel> FollowCart
		{
			get { return followCart; }
			set { SetProperty(ref followCart, value, FollowCartPropertyName); }
		}

		int? piece = null;
		public const string PiecePropertyName = "Piece";
		public int? Piece
		{
			get { return piece; }
			set { SetProperty(ref piece, value, PiecePropertyName); }
		}


		bool warning = false;
		public const string WarningPropertyName = "Warning";
		public bool Warning
		{
			get { return warning; }
			set { SetProperty(ref warning, value, WarningPropertyName); }
		}


		bool is_warning = true;
		public const string IsWarningPropertyName = "IsWarning";
		public bool IsWarning
		{
			get { return is_warning; }
			set { SetProperty(ref is_warning, value, IsWarningPropertyName); }
		}

		string searchVal = "";
		public const string SearchValPropertyName = "SearchVal";
		public string SearchVal
		{
			get { return searchVal; }
			set { SetProperty(ref searchVal, value, SearchValPropertyName); }
		}


		public DataBaseModel d;
		public UserInfoModel userIModel;

		public MyRequestViewModel(Page page, int? DrugId, int? id, int? piece, int? followId, string followDate) : base(page)
		{
			this.drugId = DrugId;
			this._id = id;
			this._piece = piece;
			this._followId = followId;
			this._followDate = followDate;
			this.WishList = new List<WishListModel>();
		
		
			using (var sqlLiteDB = new DBService())
			{
				d = sqlLiteDB.Data();
				if (d != null)
				{
					userIModel = Newtonsoft.Json.JsonConvert.DeserializeObject<UserInfoModel>(d.UserInfo);
				}
			}


		}

		public const string WishListCommandPropertyName = "WishListCommand";
		Command wishListCommand;
		public Command WishListCommand
		{
			get
			{
				return wishListCommand ??
					(wishListCommand = new Command(async () => await WishListCommandExecute()));
			}
		}

		public const string WishItemCommandPropertyName = "WishItemCommand";
		Command wishItemCommand;
		public Command WishItemCommand
		{
			get
			{
				return wishItemCommand ??
					(wishItemCommand = new Command(async () => await WishItemCommandExecute()));
			}
		}

		public const string WishListTextChangeCommandPropertyName = "WishListTextChangeCommand";
		Command wishListTextChangeCommand;
		public Command WishListTextChangeCommand
		{
			get
			{
				return wishListTextChangeCommand ??
					(wishListTextChangeCommand = new Command(async () => await WishListTextChange()));
			}
		}


		public const string EditWishCommandPropertyName = "EditWishCommand";
		Command editWishCommand;
		public Command EditWishCommand
		{
			get
			{
				return editWishCommand ??
					(editWishCommand = new Command(async () => await ExecuteEditWishCommandCommand()));
			}
		}

		public const string FollowListCommandPropertyName = "FollowListCommand";
		Command followListCommand;
		public Command FollowListCommand
		{
			get
			{
				return followListCommand ??
					(followListCommand = new Command(async () => await ExecuteFollowListCommandCommand()));
			}
		}

		public const string AddFollowCommandPropertyName = "AddFollowCommand";
		Command addFollowCommand;
		public Command AddFollowCommand
		{
			get
			{
				return addFollowCommand ??
					(addFollowCommand = new Command(async () => await ExecuteAddFollowCommandCommand()));
			}
		}

		public const string DeleteFollowCommandPropertyName = "DeleteFollowCommand";
		Command deleteFollowCommand;
		public Command DeleteFollowCommand
		{
			get
			{
				return deleteFollowCommand ??
					(deleteFollowCommand = new Command( async () => await ExecuteDeleteFollowCommandCommand()));
			}
		}

		private async Task ExecuteDeleteFollowCommandCommand()
		{

			IsBusy = true;

			deleteFollowCommand.ChangeCanExecute();
			try
			{

				bool answer = await page.DisplayAlert("Uyarı", "Ürün takip listenizden silinmek üzere, onaylıyor musunuz ?", "Evet", "Hayır");

				if (answer)
				{
					var resultData = await WishController.DeleteFollow(this._followId);
					if (resultData == "OK")
					{
						this.FollowCart = await WishController.GetFollow(UserBaseId);

						if (this.FollowCart == null)
						{
							this.Warning = true;
							this.IsWarning = false;
						}
						else
						{
							if (this.FollowCart.Count == 0)
							{
								this.Warning = true;
								this.IsWarning = false;
							}
							else
							{
								this.Warning = false;
								this.IsWarning = true;
							}
						}


					}


					IsBusy = false;
				}
				else
				{
					this.FollowCart = await WishController.GetFollow(UserBaseId);
					this.Warning = false;
					this.IsWarning = true;
					IsBusy = false;

				}

			}
			catch (Exception ex)
			{
				BugService.SendBugMessage(userIModel.UserId, d.DeviceUniqueId, ex.Message, ex.StackTrace);
			}
			finally
			{
				IsBusy = false;
			}

		}

		private async Task ExecuteAddFollowCommandCommand()
		{

			IsBusy = true;
			addFollowCommand.ChangeCanExecute();

			try
			{

				var wish_list = await WishController.GetFollow(UserBaseId);

				foreach (var item in wish_list)
				{
					if (item.DrugId == this.drugId) 
					{
						DateTime miat1 = DateTime.ParseExact(item.Term, "dd.mm.yyyy", null);
						DateTime miat2 = DateTime.ParseExact(this._followDate, "dd.mm.yyyy", null);

						if(DateTime.Compare(miat1,miat2) == 0 )
						{
							addDrug = false;
						}
					}
				}


				if (addDrug)
				{
					var resultData = await WishController.AddFollow(this._followId, UserBaseId);
					if (resultData == "OK")
					{
						await page.DisplayAlert("Uyarı", "Takip listenize ürün eklendi.", "Tamam");
					}
				}
				else
				{
					await page.DisplayAlert("Uyarı", "Bu ürün takip listenizde mevcut.", "Tamam");
				}


				 
			}
			catch (Exception ex)
			{
				BugService.SendBugMessage(userIModel.UserId, d.DeviceUniqueId, ex.Message, ex.StackTrace);
			}
			finally
			{
				IsBusy = false;
			}

		}

		private async Task ExecuteFollowListCommandCommand()
		{


			IsBusy = true;
			followListCommand.ChangeCanExecute();

			try
			{
				this.FollowCart = await WishController.GetFollow(UserBaseId);

				if (this.FollowCart == null)
				{
					this.Warning = true;
					this.IsWarning = false;
				}
				else
				{
					if (this.FollowCart.Count == 0)
					{ 
						this.Warning = true;
						this.IsWarning = false;
					}
					else
					{
						this.Warning = false;
						this.IsWarning = true;
					}
				}


				IsBusy = false;
			}
			catch (Exception ex)
			{
				BugService.SendBugMessage(userIModel.UserId, d.DeviceUniqueId, ex.Message, ex.StackTrace);
			}
			finally
			{
				IsBusy = false;
			}
		}

		private async Task ExecuteEditWishCommandCommand()
		{

			IsBusy = true;
			editWishCommand.ChangeCanExecute();

			try
			{
				if (_id.HasValue)
				{
					var returnData = await WishController.UpdateWishList(_id, _piece, UserBaseId);
					if (returnData == "OK")
					{
						await page.DisplayAlert("Uyarı", "Ürün güncelleme işlemi başarılı.", "Tamam");
						await page.Navigation.PopAsync();
					}
					else await page.DisplayAlert("Uyarı", "Ürün güncelleme işlemi sırasında sorun oluştu.", "Tamam");
				}
				else
				{
					var returnData = await WishController.SetWishList("", "", this.drugId, _piece, UserBaseId);
					if (returnData.isErrorHappend == false)
					{
						await page.DisplayAlert("Uyarı", "Ürün ekleme işlemi başarılı.", "Tamam");
						await page.Navigation.PopAsync();
					}
					else await page.DisplayAlert("Uyarı", "Ürün ekleme işlemi sırasında sorun oluştu. " + returnData.Error[0].Error + "", "Tamam");
				}
				 
			}
			catch (Exception ex)
			{
				BugService.SendBugMessage(userIModel.UserId, d.DeviceUniqueId, ex.Message, ex.StackTrace);
			}
			finally
			{
				IsBusy = false;
			}
		}

		private async Task WishListTextChange()
		{

			IsBusy = true;
			wishListTextChangeCommand.ChangeCanExecute();

			try
			{
				this.WishList = searchList.Where(x => x.DrugName.Contains(this.SearchVal)).ToList();
			}
			catch (Exception ex)
			{
				BugService.SendBugMessage(userIModel.UserId, d.DeviceUniqueId, ex.Message, ex.StackTrace);
			}
			finally
			{
				IsBusy = false;
			}

		}

		private async Task WishListCommandExecute()
		{

			IsBusy = true;
			wishListCommand.ChangeCanExecute();

			try
			{
				this.WishList = await WishController.GetWishList("", "", UserBaseId);
				foreach (var item in this.WishList)
				{
					if (item.OtherSuppliers.Count > 0)
					{
						item.isOtherHave = true;
					}
					else item.isOtherHave = false;
				}
				searchList = new List<WishListModel>();
				searchList.AddRange(this.WishList);
				 
			}
			catch (Exception ex)
			{
				BugService.SendBugMessage(userIModel.UserId, d.DeviceUniqueId, ex.Message, ex.StackTrace);
			}
			finally
			{
				IsBusy = false;
			}


		}

		private async Task WishItemCommandExecute()
		{

			IsBusy = true;
			wishItemCommand.ChangeCanExecute();

			try
			{


				this.WishList = await WishController.GetWishList("", "", UserBaseId);

				if (this.WishList.Count > 0)
				{
					var item = this.WishList.Where(x => x.Id == this._id).FirstOrDefault();
					if (item != null)
					{
						this.Piece = item.Piece;

					}
				}
				 
			}
			catch (Exception ex)
			{
				BugService.SendBugMessage(userIModel.UserId, d.DeviceUniqueId, ex.Message, ex.StackTrace);
			}
			finally
			{
				IsBusy = false;
			}


		}

	}
}
