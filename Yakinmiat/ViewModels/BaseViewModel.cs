﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using Xamarin.Forms;
using Yakinmiyat.Users;

namespace Yakinmiat
{
	public class BaseViewModel : INotifyPropertyChanged
	{
		protected Page page;
		public BaseViewModel(Page page)
		{
			this.page = page;

			using (var database = new DBService())
			{
				DataBaseModel _database = database.Data();
				if (_database != null)
				{
					this.userBaseInfo = new UserInfoModel();
					this.UserBaseInfo = Newtonsoft.Json.JsonConvert.DeserializeObject<UserInfoModel>(_database.UserInfo);
					if (this.UserBaseInfo != null)
					{
						this.UserBaseId = this.UserBaseInfo.UserId;

					}
				}
			}
		}

		private string userBaseId = string.Empty;
		public const string UserBaseIdPropertyName = "UserBaseId";

		/// <summary>
		/// Gets or sets the "User Base Id" property
		/// </summary>
		/// <value>The title.</value>
		public string UserBaseId
		{
			get { return userBaseId; }
			set { SetProperty(ref userBaseId, value, UserBaseIdPropertyName); }
		}

		private UserInfoModel userBaseInfo;
		public const string UserBaseInfoPropertyName = "UserBaseInfo";

		/// <summary>
		/// Gets or sets the "User Base Id" property
		/// </summary>
		/// <value>The title.</value>
		public UserInfoModel UserBaseInfo
		{
			get { return userBaseInfo; }
			set { SetProperty(ref userBaseInfo, value, UserBaseInfoPropertyName); }
		}

		private List<string> userRoles;
		public const string UserRolesPropertyName = "UserRoles";

		/// <summary>
		/// Gets or sets the "User Role List" property
		/// </summary>
		/// <value>The title.</value>
		public List<string> UserRoles
		{
			get { return userRoles; }
			set { SetProperty(ref userRoles, value, UserRolesPropertyName); }
		}

		private string title = string.Empty;
		public const string TitlePropertyName = "Title";

		/// <summary>
		/// Gets or sets the "Title" property
		/// </summary>
		/// <value>The title.</value>
		public string Title
		{
			get { return title; }
			set { SetProperty(ref title, value, TitlePropertyName); }
		}

		private string subTitle = string.Empty;
		/// <summary>
		/// Gets or sets the "Subtitle" property
		/// </summary>
		public const string SubtitlePropertyName = "Subtitle";
		public string Subtitle
		{
			get { return subTitle; }
			set { SetProperty(ref subTitle, value, SubtitlePropertyName); }
		}

		private string icon = null;
		/// <summary>
		/// Gets or sets the "Icon" of the viewmodel
		/// </summary>
		public const string IconPropertyName = "Icon";
		public string Icon
		{
			get { return icon; }
			set { SetProperty(ref icon, value, IconPropertyName); }
		}

		private bool isBusy;
		/// <summary>
		/// Gets or sets if the view is busy.
		/// </summary>
		public const string IsBusyPropertyName = "IsBusy";
		public bool IsBusy
		{
			get { return isBusy; }
			set { SetProperty(ref isBusy, value, IsBusyPropertyName); }
		}

		private bool canLoadMore = true;
		/// <summary>
		/// Gets or sets if we can load more.
		/// </summary>
		public const string CanLoadMorePropertyName = "CanLoadMore";
		public bool CanLoadMore
		{
			get { return canLoadMore; }
			set { SetProperty(ref canLoadMore, value, CanLoadMorePropertyName); }
		}
		protected void SetObservableProperty<T>(
			ref T field,
			T value,
			[CallerMemberName] string propertyName = "")
		{
			if (EqualityComparer<T>.Default.Equals(field, value)) return;
			field = value;
			OnPropertyChanged(propertyName);
		}

		protected void SetProperty<T>(
			ref T backingStore, T value,
			string propertyName = "",
			Action onChanged = null,
			Action<T> onChanging = null)
		{


			if (EqualityComparer<T>.Default.Equals(backingStore, value))
				return;
			if (onChanging != null)
				onChanging(value);

			OnPropertyChanging(propertyName);
			backingStore = value;

			if (onChanged != null)
				onChanged();

			OnPropertyChanged(propertyName);
		}
		#region INotifyPropertyChanging implementation
		public event PropertyChangingEventHandler PropertyChanging;
		#endregion

		public void OnPropertyChanging(string propertyName)
		{
			if (PropertyChanging == null)
				return;

			PropertyChanging(this, new PropertyChangingEventArgs(propertyName));
		}

		#region INotifyPropertyChanged implementation
		public event PropertyChangedEventHandler PropertyChanged;
		#endregion

		public void OnPropertyChanged(string propertyName)
		{
			if (PropertyChanged == null)
				return;

			PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
		}
	}
}
