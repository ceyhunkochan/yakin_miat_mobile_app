﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xamarin.Forms;
using Yakinmiat.Drugs;
using Yakinmiyat.Users;

namespace Yakinmiat
{
	public class UrunBulViewModel : BaseViewModel
	{
		string searchVal;

		int? _piece;
		string _term;
		int? _drugid;

		List<DrugModel> drugList;
		public const string DrugListPropertyName = "DrugList";
		public List<DrugModel> DrugList
		{
			get { return drugList; }
			set { SetProperty(ref drugList, value, DrugListPropertyName); }
		}



		public DataBaseModel d;
		public UserInfoModel userIModel;

		public UrunBulViewModel(Page page, string SearchVal, int? drugid, int? piece, string term) : base(page)
		{
			this.searchVal = SearchVal;
			this._drugid = drugid;
			this._piece = piece;
			this._term = term;
			drugList = new List<DrugModel>();


			using (var sqlLiteDB = new DBService())
			{
				d = sqlLiteDB.Data();
				if (d != null)
				{
					userIModel = Newtonsoft.Json.JsonConvert.DeserializeObject<UserInfoModel>(d.UserInfo);
				}
			}

		}

		public const string AddProductCommandPropertyName = "AddProductCommand";
		Command addProductCommand;
		public Command AddProductCommand
		{
			get
			{
				return addProductCommand ??
					(addProductCommand = new Command(async () => await ExecuteAddProductCommand()));
			}
		}

		private async Task ExecuteAddProductCommand()
		{

			IsBusy = true;

			addProductCommand.ChangeCanExecute();

			try
			{

				var returnData = await SupplyController.SetSupplyList("", "", _drugid, _piece, UserBaseId, _term, 0);
				if (returnData.isErrorHappend == false)
				{
					await page.DisplayAlert("Uyarı", "Ürün ekleme işlemi başarılı.", "Tamam");
				}
				else
				{ 
					await page.DisplayAlert("Uyarı", "Ürün ekleme işlemi sırasında sorun oluştu. " + returnData.Error[0].Error + "", "Tamam");
				}

			}
			catch (Exception ex)
			{
				BugService.SendBugMessage(userIModel.UserId, d.DeviceUniqueId, ex.Message, ex.StackTrace);
			}
			finally
			{ 
				IsBusy = false;
			}

		}


	}
}
