﻿using System;
using System.Threading.Tasks;
using Xamarin.Forms;
using Yakinmiyat.Users;

namespace Yakinmiat
{
	public class DeleteProductViewModel : BaseViewModel
	{
		public int? _id
		{
			get;
			set;
		}
		public int? _options
		{
			get;
			set;
		}
		public int? _piece
		{
			get;
			set;
		}
		public string _gln
		{
			get;
			set;
		}
		public int _source
		{
			get;
			set;
		}

		public DataBaseModel d;
		public UserInfoModel userIModel;

		public DeleteProductViewModel(Page page, int? id, int? options, int? piece, string gln, int source) : base(page) //source eğer 1=ürün sil 2=istek sil
		{
			this._id = id;
			this._options = options;
			this._piece = piece;
			this._gln = gln;
			this._source = source;

			using (var sqlLiteDB = new DBService())
			{
				d = sqlLiteDB.Data();
				if (d != null)
				{
					userIModel = Newtonsoft.Json.JsonConvert.DeserializeObject<UserInfoModel>(d.UserInfo);
				}

			}

		}


		public const string DeleteCommandPropertyName = "DeleteCommand";
		Command deleteCommand;
		public Command DeleteCommand
		{
			get
			{
				return deleteCommand ??
					(deleteCommand = new Command(async () => await ExecuteDeleteCommand()));
			}
		}



		private async Task ExecuteDeleteCommand()
		{

			IsBusy = true;
			deleteCommand.ChangeCanExecute();

			try
			{
				if (_source == 1)
				{
					var returnVal = await SupplyController.DeleteSupply("", "", UserBaseId, _id, _options.ToString(), _gln, _piece);
					if (returnVal.isErrorHappend == false)
					{
						await page.DisplayAlert("Uyarı", "İşleminiz başarıyla gerçekleştirildi.", "Tamam");
						await page.Navigation.PopToRootAsync();
					}
					else
					{
						if (returnVal.Error.Count>0)
						{
							await page.DisplayAlert("Uyarı", returnVal.Error[0].Error, "Tamam");
						}
						else await page.DisplayAlert("Uyarı", "İşleminiz sırasında bir sorun ile karşılaşıldı lütfen daha sonra tekrar deneyiniz.", "Tamam");
					}
				}
				else
				{
					var returnVal = await WishController.DeleteWish("", "", UserBaseId, _id, _options.ToString(), _gln, _piece);
					if (returnVal.isErrorHappend == false)
					{
						await page.DisplayAlert("Uyarı", "İşleminiz başarıyla gerçekleştirildi.", "Tamam");
						await page.Navigation.PopToRootAsync();
					}
					else
					{
						if (returnVal.Error.Count > 0)
						{
							await page.DisplayAlert("Uyarı", returnVal.Error[0].Error, "Tamam");
						}
						else await page.DisplayAlert("Uyarı", "İşleminiz sırasında bir sorun ile karşılaşıldı lütfen daha sonra tekrar deneyiniz.", "Tamam");
					}
				}
				IsBusy = false;
			}
			catch (Exception ex)
			{
				BugService.SendBugMessage(userIModel.UserId, d.DeviceUniqueId, ex.Message, ex.StackTrace);
			}
			finally
			{
				IsBusy = false;
			}
		}
	}
}
