﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xamarin.Forms;
using Yakinmiyat.SearchDrug;
using Yakinmiyat.SearchPharmacy;
using Yakinmiyat.Users;

namespace Yakinmiat
{
	public class FindPharmacyViewModel : BaseViewModel
	{
		private Command getPharmacyListCommand;
		public int? _DrugId;
		List<SearchPharmacyResultModel> resultList;

		string user_id, pharmacy_name, gln_number;
		int? city_id, brand_id;



		public DataBaseModel d;
		public UserInfoModel userIModel;

		public FindPharmacyViewModel(Page page, string user_id, string pharmacy_name, string gln_number, int? city_id, int? brand_id) : base(page)
		{
			this.user_id = user_id;
			this.pharmacy_name = pharmacy_name;
			this.gln_number = gln_number;
			this.city_id = city_id;
			this.brand_id = brand_id;

			using (var sqlLiteDB = new DBService())
			{
				d = sqlLiteDB.Data();
				if (d != null)
				{
					userIModel = Newtonsoft.Json.JsonConvert.DeserializeObject<UserInfoModel>(d.UserInfo);
				}

			}

		}


		List<SearchPharmacyResultModel> searchPharmacyList;
		public const string SearchPharmacyPropertyName = "SearchPharmacyList";
		public List<SearchPharmacyResultModel> SearchDrugList
		{
			get { return searchPharmacyList; }
			set { SetProperty(ref searchPharmacyList, value, SearchPharmacyPropertyName); }
		}

		public Command GetPharmacyListCommand
		{
			get
			{
				return getPharmacyListCommand ??
					(getPharmacyListCommand = new Command(async () => await ExecuteGetPharmacyListCommand(), () => { return !IsBusy; }));
			}
		}

		private async Task ExecuteGetPharmacyListCommand()
		{
			if (IsBusy)
				return;

			IsBusy = true;

			getPharmacyListCommand.ChangeCanExecute();

			try
			{

				//public static async Task<List<SearchPharmacyResultModel>> SearchPharmacy(string user, string pass, string userid, string name, string code, int? city)
				resultList = new List<SearchPharmacyResultModel>();
				resultList = await SearchPharmacyController.SearchPharmacy(Constants.apiUser,
																	 Constants.apiPass,
		                                                             this.user_id ,
		                                                             this.pharmacy_name,
		                                                             this.gln_number,
		                                                             this.city_id
		                                                             );//this.brand_id
				                                                           	 
				IsBusy = false; 

				await page.Navigation.PushAsync(new FindPharmacyResultPage(resultList));

			}
			catch (Exception ex)
			{
				BugService.SendBugMessage(userIModel.UserId, d.DeviceUniqueId, ex.Message, ex.StackTrace);
				await page.DisplayAlert("Uyarı", "Eczane listeleme sayfasına yönlendirilemedi.", "Tamam");
			}
			finally
			{
				IsBusy = false;
			}

		}
	}
}
