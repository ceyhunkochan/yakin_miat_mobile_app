﻿using System;
using System.Diagnostics;
using System.Threading.Tasks;
using Version.Plugin;
using Xamarin.Forms;
using Yakinmiyat.Users;

namespace Yakinmiat
{
	public class LoginViewModel : BaseViewModel
	{

		string username = string.Empty;
		public const string UsernamePropertyName = "UserName";
		public string UserName
		{
			get { return username; }
			set { SetProperty(ref username, value, UsernamePropertyName); }
		}

		string password = string.Empty;
		public const string PasswordPropertyName = "Password";
		public string Password
		{
			get { return password; }
			set { SetProperty(ref password, value, PasswordPropertyName); }
		}

		public DataBaseModel d;
		public UserInfoModel userIModel;

		public LoginViewModel(Page page) : base(page)
		{
			using (var sqlLiteDB = new DBService())
			{
				d = sqlLiteDB.Data();
				if (d != null)
				{
					userIModel = Newtonsoft.Json.JsonConvert.DeserializeObject<UserInfoModel>(d.UserInfo);
				}
			}
		}

		public const string LoginCommandPropertyName = "LoginCommand";
		Command loginCommand;
		public Command LoginCommand
		{
			get
			{
				return loginCommand ??
					(loginCommand = new Command(async () => await ExecuteLoginCommand()));
			}
		}

		private async Task ExecuteLoginCommand()
		{

			bool isLoginSuccess = false;
			if (IsBusy)
				return;

			IsBusy = true;
			loginCommand.ChangeCanExecute();

			try
			{
				var _loginModel = new LoginModel();
				_loginModel.Password = this.Password;
				_loginModel.UserName = this.UserName;

				var loginResult = await UserController.Login(_loginModel);
				if (!loginResult.isErrorHappend)
				{
					isLoginSuccess = true;

					DBService database = new DBService();

					var loginModelJson = Newtonsoft.Json.JsonConvert.SerializeObject(loginResult);
					DataBaseModel dataModel = new DataBaseModel();
					dataModel.UserInfo = loginModelJson;
					dataModel.Password = this.Password;
					dataModel.UserName = this.username;
					dataModel.RegistrationId = App.regToken;
					dataModel.DeviceUniqueId = App.uniqueDeviceID;
					dataModel.AppliVersion = CrossVersion.Current.Version;

					database.Insert(dataModel);

					DeviceRegisterModel regModel = new DeviceRegisterModel();

					//regmodel variables
					regModel.Token = App.regToken;
					regModel.Device = Device.OS.ToString();
					regModel.UserId = loginResult.UserId;
					regModel.DeviceUniqueId = App.uniqueDeviceID;

					var registerResult = await UserController.RegisterUserDevice(loginResult.UserId,
																				 Device.OS.ToString(),
					                                                             App.regToken,
																				 App.uniqueDeviceID,
																				 null); //hubId
					//check if registration on server side is OK
					if (registerResult.isErrorHappend == false)
					{
						Debug.WriteLine("Success Register User Device");
					}
					else
					{
						Debug.WriteLine("Fail Register User Device");
					}

				}
				else 
				{ 
					isLoginSuccess = false; 
				}



			}
			catch (Exception ex)
			{
				BugService.SendBugMessage(userIModel.UserId, d.DeviceUniqueId, ex.Message, ex.StackTrace);
			}
			finally
			{
				IsBusy = false;
			}

			if (isLoginSuccess)
			{

				if (Device.OS == TargetPlatform.Android)
					Application.Current.MainPage = new RootPage();
				else
					await page.Navigation.PushModalAsync(new RootPage());

			}
			else
			{

				await page.DisplayAlert("Kullanıcı Girişi", "Bilgilerinizi kontrol edip tekrar deneyiniz.", "Ok");
			}
		}
	}
}
