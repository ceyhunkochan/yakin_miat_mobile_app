﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Xamarin.Forms;
using Yakinmiyat.SupplyList;
using Yakinmiyat.Users;

namespace Yakinmiat
{
	public class MyProductViewModel : BaseViewModel
	{
		string searchVal;
		int? _id;
		int? _piece;
		decimal? _price;
		string _term;
		int? _drugid;
		List<SupplyListModel> supplyList;
		public const string SupplyListPropertyName = "SupplyList";
		public List<SupplyListModel> SupplyList
		{
			get { return supplyList; }
			set { SetProperty(ref supplyList, value, SupplyListPropertyName); }
		}


		int? piece = null;
		public const string PiecePropertyName = "Piece";
		public int? Piece
		{
			get { return piece; }
			set { SetProperty(ref piece, value, PiecePropertyName); }
		}

		string price = null;
		public const string PricePropertyName = "Price";
		public string Price
		{
			get { return price; }
			set { SetProperty(ref price, value, PricePropertyName); }
		}



		DateTime term = DateTime.Today.AddMonths(1);
		public const string TermPropertyName = "Term";
		public DateTime Term
		{
			get { return term; }
			set { SetProperty(ref term, value, TermPropertyName); }
		}


		public DataBaseModel d;
		public UserInfoModel userIModel;

		public MyProductViewModel(Page page, string SearchVal, int? drugid, int? id, int? piece, decimal? price, string term) : base(page)
		{
			this.searchVal = SearchVal;
			this._id = id;
			this._drugid = drugid;
			this._piece = piece;
			this._price = price;
			this._term = term;
			this.SupplyList = new List<SupplyListModel>();

			using (var sqlLiteDB = new DBService())
			{
				d = sqlLiteDB.Data();
				if (d != null)
				{
					userIModel = Newtonsoft.Json.JsonConvert.DeserializeObject<UserInfoModel>(d.UserInfo);
				}
			}

		}

		public const string SupplyListCommandPropertyName = "SupplyListCommand";
		Command supplyListCommand;
		public Command SupplyListCommand
		{
			get
			{
				return supplyListCommand ??
					(supplyListCommand = new Command(async () => await ExecuteSupplyListCommand()));
			}

		}

		public const string SupplyItemCommandPropertyName = "SupplyItemCommand";
		Command supplyItemCommand;
		public Command SupplyItemCommand
		{
			get
			{
				return supplyItemCommand ??
					(supplyItemCommand = new Command(async () => await ExecuteSupplyItemCommand()));
			}
		}

		public const string EditSupplyCommandPropertyName = "EditSupplyCommand";
		Command editSupplyCommand;
		public Command EditSupplyCommand
		{
			get
			{
				return editSupplyCommand ??
					(editSupplyCommand = new Command(async () => await ExecuteEditSupplyCommandCommand()));
			}
		}

		private async Task ExecuteEditSupplyCommandCommand()
		{

			IsBusy = true;
			editSupplyCommand.ChangeCanExecute();
			try
			{
				if (_id.HasValue)
				{
					var returnData = await SupplyController.UpdateSupplyList(_id, _piece, _term, _price, UserBaseId);
					if (returnData == "OK")
					{
						await page.DisplayAlert("Uyarı", "Ürün güncelleme işlemi başarılı.", "Tamam");
						await page.Navigation.PopAsync();
					}
					else 
					{

						JObject json_object = JObject.Parse(returnData);
						string message = json_object.Value<string>("Message");
						await page.DisplayAlert("Uyarı",message, "Tamam");
					}
						
				}
				else
				{
					var returnData = await SupplyController.SetSupplyList("", "", _drugid, _piece, UserBaseId, _term, _price);
					if (returnData.isErrorHappend == false)
					{
						await page.DisplayAlert("Uyarı", "Ürün ekleme işlemi başarılı.", "Tamam");
						await page.Navigation.PopAsync();
					} else 
						await page.DisplayAlert("Uyarı", "Ürün ekleme işlemi sırasında sorun oluştu. " + returnData.Error[0].Error + "", "Tamam");
				}

			}
			catch (Exception ex)
			{
				BugService.SendBugMessage(userIModel.UserId, d.DeviceUniqueId, ex.Message, ex.StackTrace);
			}
			finally
			{
				IsBusy = false;
			}


		}

		private async Task ExecuteSupplyItemCommand()
		{

			IsBusy = true;
			supplyItemCommand.ChangeCanExecute();
			try
			{

				this.SupplyList = await SupplyController.GetSupplyList("", "", UserBaseId);

				if (this.SupplyList.Count > 0)
				{
					var item = this.SupplyList.Where(x => x.Id == this._id).FirstOrDefault();
					if (item != null)
					{
						this.Piece = item.Piece;
						this.Price = item.Price;

						int year;
						int month;
						int day;
						if (!string.IsNullOrEmpty(item.Date))
						{
							var parsedDate = item.Date.Split('.');
							year = Convert.ToInt32(parsedDate[2]);
							month = Convert.ToInt32(parsedDate[1]);
							day = Convert.ToInt32(parsedDate[0]);
							this.Term = new DateTime(year, month, day);
						}
					}
				}
			}
			catch (Exception ex)
			{
				BugService.SendBugMessage(userIModel.UserId, d.DeviceUniqueId, ex.Message, ex.StackTrace);
			}
			finally
			{
				IsBusy = false;
			}
		}

		private async Task ExecuteSupplyListCommand()
		{

			IsBusy = true;
			supplyListCommand.ChangeCanExecute();
			try
			{
				this.SupplyList = await SupplyController.GetSupplyList("", "", UserBaseId);
			}
			catch (Exception ex)
			{
				BugService.SendBugMessage(userIModel.UserId, d.DeviceUniqueId, ex.Message, ex.StackTrace);
			}
			finally
			{
				IsBusy = false;
			}
		}
	}
}
