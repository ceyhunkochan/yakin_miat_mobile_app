﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xamarin.Forms;
using Yakinmiyat.SearchDrug;
using Yakinmiyat.Users;

namespace Yakinmiat
{
	public class FindProductViewModel : BaseViewModel
	{
		private Command getSearchDrugListCommand;
		public int? _DrugId;
		public int? _followId;
		public string _followDate;
		protected bool addDrug = true;


		public DataBaseModel d;
		public UserInfoModel userIModel;


		public FindProductViewModel(Page page, int? DrugId, int? followId, string followDate) : base(page)
		{
			this._DrugId = DrugId;
			this._followId = followId;
			this._followDate = followDate;

			using (var sqlLiteDB = new DBService())
			{
				d = sqlLiteDB.Data();
				if (d != null)
				{
					userIModel = Newtonsoft.Json.JsonConvert.DeserializeObject<UserInfoModel>(d.UserInfo);
				}
			}

		}

		List<SearchDrugModel> searchDrugList;
		public const string SearchDrugListPropertyName = "SearchDrugList";
		public List<SearchDrugModel> SearchDrugList
		{
			get { return searchDrugList; }
			set { SetProperty(ref searchDrugList, value, SearchDrugListPropertyName); }
		}



		string productNameLabel = string.Empty;
		public const string ProductNameLabelPropertyName = "DrugName";
		public string ProductNameLabel
		{
			get { return productNameLabel; }
			set { SetProperty(ref productNameLabel, value, ProductNameLabelPropertyName); }
		}
	 
 

		public Command GetSearchDrugListCommand
		{
			get
			{
				return getSearchDrugListCommand ??
					(getSearchDrugListCommand = new Command(async () => await ExecuteGetSearchDrugListCommand(), () => { return !IsBusy; }));
			}
		}

		public const string AddFollowCommandPropertyName = "AddFollowCommand";
		Command addFollowCommand;
		public Command AddFollowCommand
		{
			get
			{
				return addFollowCommand ??
					(addFollowCommand = new Command(async () => await ExecuteAddFollowCommandCommand()));
			}
		}

		private async Task ExecuteAddFollowCommandCommand()
		{

			IsBusy = true;
			addFollowCommand.ChangeCanExecute();

			try
			{

				var wish_list = await WishController.GetFollow(UserBaseId);

				foreach (var item in wish_list)
				{
					if (item.DrugId == this._DrugId)
					{
						DateTime miat1 = DateTime.ParseExact(item.Term, "dd.mm.yyyy", null);
						DateTime miat2 = DateTime.ParseExact(this._followDate, "dd.mm.yyyy", null);

						if (DateTime.Compare(miat1, miat2) == 0)
						{
							addDrug = false;
						}
					}
				}


				//Drug does not exit in User's follow list
				if (addDrug)
				{

					SearchDrugModel postModel = new SearchDrugModel();
					postModel.SearchDrug = _DrugId.ToString();
					postModel.UserId = UserBaseId;

					List<SearchDrugModel> returnModel = await SearchDrugController.SearchDrug(postModel);



					if (returnModel == null)
					{
						IsBusy = false;
						await page.DisplayAlert("Uyarı", "Ürün hiçbir eczane de bulunamadı.", "Tamam");
						await page.Navigation.PopAsync();
					}
					else
					{
						

						if (returnModel.Count == 0)
						{

							await page.DisplayAlert("Uyarı", "Ürün hiçbir eczane de bulunamadı.", "Tamam");
							await page.Navigation.PopAsync();
						}
						else
						{

							bool isTrue = await page.DisplayAlert("Uyarı", "Ürün takip listenize eklenecek onaylıyor musunuz ?", "Evet", "Hayır");
							if (isTrue)
							{
								
								var resultDatum = await WishController.AddFollow(this._followId, UserBaseId);

								IsBusy = false;

								if (resultDatum == "OK")
								{

									await page.DisplayAlert("Uyarı", "Takip listenize ürün eklendi.", "Tamam");//
									this.SearchDrugList = new List<SearchDrugModel>();
									this.ProductNameLabel = returnModel[0].DrugName;
									this.SearchDrugList.AddRange(returnModel);

								}
								else
								{
									await page.DisplayAlert("Uyarı", "Ürün ekleme başarısız !\nDaha sonra tekrar deneyin.", "Tamam");
									this.SearchDrugList = new List<SearchDrugModel>();
									this.ProductNameLabel = returnModel[0].DrugName;
									this.SearchDrugList.AddRange(returnModel);
								}
							}
							else
							{
								this.SearchDrugList = new List<SearchDrugModel>();
								this.ProductNameLabel = returnModel[0].DrugName;
								this.SearchDrugList.AddRange(returnModel);
							}

						}
					
					}

				}
				else
				{
					IsBusy = false;
					await page.DisplayAlert("Uyarı", "Bu ürün takip listenizde mevcut.", "Tamam");
					await page.Navigation.PopAsync();
				}


			}
			catch (Exception ex)
			{
				BugService.SendBugMessage(userIModel.UserId, d.DeviceUniqueId, ex.Message, ex.StackTrace);
			}
			finally
			{
				IsBusy = false;
			}


		}

		private async Task ExecuteGetSearchDrugListCommand()
		{
			if (IsBusy)
				return;

			IsBusy = true;

			getSearchDrugListCommand.ChangeCanExecute();

			try
			{

				SearchDrugModel postModel = new SearchDrugModel();
				postModel.SearchDrug = _DrugId.ToString();
				postModel.UserId = UserBaseId;

				var returnModel = await SearchDrugController.SearchDrug(postModel);
				this.SearchDrugList = new List<SearchDrugModel>();

				if (returnModel.Count <= 0)
				{
					IsBusy = false;
					await page.DisplayAlert("Uyarı", "Ürün hiçbir eczane de bulunamadı.", "Tamam");
					await page.Navigation.PopAsync();
				}
				else
				{
					this.ProductNameLabel = returnModel[0].DrugName;
					this.SearchDrugList.AddRange(returnModel);	
				}


			}
			catch (Exception ex)
			{
				IsBusy = false;
				BugService.SendBugMessage(userIModel.UserId, d.DeviceUniqueId, ex.Message, ex.StackTrace);
				await page.DisplayAlert("Uyarı", "Ürün arama sırasında bir sorun oluştu.", "Tamam");
				await page.Navigation.PopAsync();

			}
			finally
			{
				IsBusy = false;
			}

			 

		}
	}
}
