﻿using System; 
using Plugin.Connectivity;
using Version.Plugin;
using Xamarin.Forms;
using Yakinmiyat.Users;

namespace Yakinmiat
{
	public partial class App : Application
	{
		public static string uniqueDeviceID;
		public static string regToken;
		public static DBService database;
		public static DataBaseModel data;
		public static LoginModel _LoginModel;
		public static string userID;
		public static string ProductSearchPage = null;
		public static ConstantModel _ConstantModel;
		public static int rootPageId = 0;

		 

		public App()
		{
			InitializeComponent();
			MainPage = new SplashPage();
		}

		protected async override void OnStart()
		{



			if (CrossConnectivity.Current.IsConnected)
			{
				using (database = new DBService())
				{
					data = database.Data();
					if (data != null)
					{
						_LoginModel = new LoginModel();

						try
						{
							if (data.AppliVersion == CrossVersion.Current.Version)
							{

								_LoginModel.UserName = data.UserName;
								_LoginModel.Password = data.Password;
								_LoginModel.Device = Device.OS.ToString();
								_LoginModel.IP = await DependencyService.Get<IIPAddressTask>().GetIPAddress();
								var userInfo = await UserController.Login(_LoginModel);


								if (!userInfo.isErrorHappend)
								{
									DeviceRegisterModel regModel = new DeviceRegisterModel();
									string checkRegToken = regToken;

									//regmodel variables
									regModel.apiPass = Constants.apiPass;
									regModel.apiUser = Constants.apiUser;
									regModel.Token = checkRegToken;
									regModel.Device = Device.OS.ToString();
									regModel.UserId = userInfo.UserId;
									regModel.DeviceUniqueId = uniqueDeviceID;


									if (data.RegistrationId == null)
									{
										var registerResult = await UserController.RegisterUserDevice(userInfo.UserId, 
										                                                             Device.OS.ToString(),
										                                                             checkRegToken,
										                                                             uniqueDeviceID,
										                                                             null); //hubId
									}

									_ConstantModel = new ConstantModel();
									_ConstantModel = await ConstantController.GetConstant();

									MainPage = new RootPage();

								}
								else
								{
									database.Delete();
									MainPage = new LoginPage();
								}



							}//if version is different
							else
							{
								database.Delete();

								_ConstantModel = new ConstantModel();
								_ConstantModel = await ConstantController.GetConstant();

								MainPage = new LoginPage();
							}

						}
						catch (Exception ex)
						{
							database.Delete();
							await Application.Current.MainPage.DisplayAlert("Uyarı","Exception:  "+ex.Message,"Tamam");
							//UserInfoModel userIModel = Newtonsoft.Json.JsonConvert.DeserializeObject<UserInfoModel>(data.UserInfo);
							//BugService.SendBugMessage(userIModel.Id, data.DeviceUniqueId, ex.Message, ex.StackTrace);
						}


					}
					else
					{
						//database.Delete();
						MainPage = new LoginPage();
					}

				}

			}
			else
			{
				await MainPage.DisplayAlert("Uyarı", "Lütfen internet bağlantınızı kontrol ediniz.", "Tamam");
				DependencyService.Get<IClose>().Close();
			}



		}

		protected override void OnSleep()
		{
			// Handle when your app sleeps
		}

		protected override void OnResume()
		{
			// Handle when your app resumes
		}



	}
}
