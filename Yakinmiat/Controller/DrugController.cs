﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Yakinmiat.Drugs;

namespace Yakinmiat
{
	public static class DrugController
	{

		//query = drug_name
		public static async Task<List<DrugModel>> GetDrugs(string query)
		{
			List<DrugModel> returnModel = new List<DrugModel>();
			string url = Services.BaseUrl + "/getdrugs?query=" + query + "";
			var result = await Services.Request(url, null);
			returnModel = await Newtonsoft.Json.JsonConvert.DeserializeObjectAsync<List<DrugModel>>(result);
			return returnModel;
		}

		public static async Task<QRCodeModel> GetBarcodeDrugs(string query)
		{
			QRCodeModel returnModel = new QRCodeModel();
			string url = Services.BaseUrl + "/parseqrcode?code=" + query + "";
			var result = await Services.Request(url, null);
			returnModel = await Newtonsoft.Json.JsonConvert.DeserializeObjectAsync<QRCodeModel>(result);
			return returnModel;
		}
	}
}
