﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Yakinmiyat.Models;
using Yakinmiyat.Profile;

namespace Yakinmiat
{
	public static class ProfileController
	{


		public static async Task<ProfileModel> GetProfile(string user, string pass, string userid, string profileId)
		{
			ProfileModel returnModel = new ProfileModel();
			user = "epsApiUser!";
			pass = "epsApiPass*!";
			try
			{
				string url = Services.BaseUrl + "/getprofile?user=" + user + "&pass=" + pass + "&userid=" + userid + "&profileId="+profileId+"";

				var result = await Services.Request(url, null);
				returnModel = await Newtonsoft.Json.JsonConvert.DeserializeObjectAsync<ProfileModel>(result);
				return returnModel;
			}
			catch (Exception ex)
			{
				returnModel.isErrorHappend = true;
				returnModel.Error.Add(new ErrorViewModel
				{
					ErrorName = "Sunucu tarafında hata oluştu api kontrolü yapın",
					Error = ex.InnerException.ToString()
				});
				return returnModel;
			}
		}

		public static async Task<ResultViewModel> SetFavorite(string user, string pass, string userid, string favoriteUserId, bool? state)
		{
			ResultViewModel returnModel = new ResultViewModel();
			user = "epsApiUser!";
			pass = "epsApiPass*!";
			try
			{
				string url = Services.BaseUrl + "/setfavorite?user=" + user + "&pass=" + pass + "&userid=" + userid + "&favoritUserId=" + favoriteUserId + "&state=" + state+"";

				var result = await Services.Request(url, null);
				returnModel = await Newtonsoft.Json.JsonConvert.DeserializeObjectAsync<ResultViewModel>(result);
				return returnModel;
			}
			catch (Exception ex)
			{
				returnModel.isErrorHappend = true;
				returnModel.Error.Add(new ErrorViewModel
				{
					ErrorName = "Sunucu tarafında hata oluştu api kontrolü yapın",
					Error = ex.InnerException.ToString()
				});
				return returnModel;
			}
		}
	}
}
