﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Yakinmiyat.Favorite;
using Yakinmiyat.Models;

namespace Yakinmiat
{
	public static class FavoriteController
	{
		//user = api_user
		public static async Task<List<FavoriteModel>> GetMyFavoriteList(string user, string pass, string userid)
		{ 
			List<FavoriteModel> returnModel = new List<FavoriteModel>();
			try
			{
				string url = Services.BaseUrl + "/getmyfavoritelist?user=" + user + "&pass=" + pass + "&userid=" + userid + "";
				var result = await Services.Request(url, null);
				returnModel = await Newtonsoft.Json.JsonConvert.DeserializeObjectAsync<List<FavoriteModel>>(result);
				return returnModel;
			}
			catch (Exception ex)
			{
				returnModel[0].isErrorHappend = true;
				returnModel[0].Error.Add(new ErrorViewModel
				{
					ErrorName = "Sunucu tarafında hata oluştu api kontrolü yapın",
					Error = ex.InnerException.ToString()
				});
				return returnModel;
			}
		}
	}
}
