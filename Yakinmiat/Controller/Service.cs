﻿using System;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Yakinmiat
{
	public static class Services
	{
		public const string BaseUrl = "http://yakinmiat.com/api";
		public static async Task<string> Request(string Url, string Data)
		{
			string returnVal = "";
			Data = "";
			using (var client = new HttpClient())
			{
				var response = await client.PostAsync(Url, new StringContent(Data, Encoding.UTF8, "application/json"));
				if (response.IsSuccessStatusCode)
				{
					var content = JsonConvert.DeserializeObject(response.Content.ReadAsStringAsync().Result);
					returnVal = content.ToString();
				}
			}
			return returnVal;
		}
	}
}
