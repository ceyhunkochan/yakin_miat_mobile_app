﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;
using Yakinmiyat.Models;

namespace Yakinmiat
{
	public class BugService
	{
		 
		public async static void SendBugMessage(string UserID, string DeviceID, string message, string log)
		{
			//ResultViewModel _ResultViewModel = new ResultViewModel();
			//BugReportModel bugReportModel = new BugReportModel();

			//bugReportModel.UserID = UserID;
			//bugReportModel.DeviceID = DeviceID;
			//bugReportModel.ErrorMessage = message;
			//bugReportModel.Log = log;

			//_ResultViewModel = await SendBugReport(bugReportModel);

			try
			{
				ResultViewModel result = await SendBugReportGET(UserID, DeviceID, message, log);

				var y = result;
			}
			catch (Exception ex)
			{
				var x = ex;
			}
		}

		public static async Task<ResultViewModel> SendBugReportGET(string UserID, string DeviceID, string message, string log)//  int? brand
		{

			//http://yakinmiat.com/api/SendBugReport?DeviceID=&ErrorMessage=&Log=&UserID=
			ResultViewModel returnModel = new ResultViewModel();
			try
			{
				string url = Services.BaseUrl + "/SendBugReport?DeviceID=" + DeviceID + "&ErrorMessage=" + message + "&Log=" + log + "&UserID=" + UserID + ""; 
				var result = await Services.Request(url, null);
				returnModel = await Newtonsoft.Json.JsonConvert.DeserializeObjectAsync<ResultViewModel>(result);
				return returnModel;
			}
			catch (Exception ex)
			{
				returnModel.isErrorHappend = true;
				returnModel.Error.Add(new ErrorViewModel
				{
					ErrorName = "Sunucu tarafında hata oluştu api kontrolü yapın",
					Error = ex.InnerException.ToString()
				});
				return returnModel;
			}
		}

		public async static Task<ResultViewModel> SendBugReport(BugReportModel reportModel)
		{
			ResultViewModel returnModel = new ResultViewModel();
			try
			{
				var loginModelJson = Newtonsoft.Json.JsonConvert.SerializeObject(reportModel);
				var result = await Services.Request("http://yakinmiat.com/api/SendBugReport", loginModelJson);
				returnModel = await Newtonsoft.Json.JsonConvert.DeserializeObjectAsync<ResultViewModel>(result);
				return returnModel;

			}
			catch (Exception ex)
			{
				returnModel.isErrorHappend = true;
				returnModel.Error.Add(new ErrorViewModel
				{
					ErrorName = "Sunucu tarafında hata oluştu api kontrolü yapın",
					Error = ex.InnerException.ToString()
				});

				Debug.WriteLine(returnModel.Error[0].Error);
				return returnModel;
			}
		}
 
	}
}
