﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Yakinmiyat.Models;
using Yakinmiyat.SupplyList;

namespace Yakinmiat
{
	public static class SupplyController
	{
		/// <summary>
		/// Gets the supply list.
		/// </summary>
		/// <returns>The supply list.</returns>
		/// <param name="user">User.</param>
		/// <param name="pass">Pass.</param>
		/// <param name="userid">Userid.</param>
		public static async Task<List<SupplyListModel>> GetSupplyList(string user, string pass, string userid)
		{
			user = "epsApiUser!";
			pass = "epsApiPass*!";
			List<SupplyListModel> returnModel = new List<SupplyListModel>();
			try
			{
				string url = Services.BaseUrl + "/getsupplylist?user=" + user + "&pass=" + pass + "&userid=" + userid + "";
				var result = await Services.Request(url, null);
				returnModel = await Newtonsoft.Json.JsonConvert.DeserializeObjectAsync<List<SupplyListModel>>(result);
				return returnModel;
			}
			catch (Exception ex)
			{
				returnModel[0].isErrorHappend = true;
				returnModel[0].Error.Add(new ErrorViewModel
				{
					ErrorName = "Sunucu tarafında hata oluştu api kontrolü yapın",
					Error = ex.InnerException.ToString()
				});
				return returnModel;
			}
		}

		/// <summary>
		/// Sets the supply list.
		/// </summary>
		/// <returns>The supply list.</returns>
		/// <param name="user">User.</param>
		/// <param name="pass">Pass.</param>
		/// <param name="drugid">Drugid.</param>
		/// <param name="piece">Piece.</param>
		/// <param name="userid">Userid.</param>
		/// <param name="term">Term.</param>
		public static async Task<ResultViewModel> SetSupplyList(string user, string pass, int? drugid, int? piece, string userid, string term, decimal? price)
		{
			ResultViewModel returnModel = new ResultViewModel();
			try
			{
				string url = Services.BaseUrl + "/setsupplylist?user=" + user + "&pass=" + pass + "&userid=" + userid + "&drugid=" + drugid + "&piece=" + piece + "&term=" + term + "&price=" + price + "";
				var result = await Services.Request(url, null);
				returnModel = await Newtonsoft.Json.JsonConvert.DeserializeObjectAsync<ResultViewModel>(result);
				return returnModel;
			}
			catch (Exception ex)
			{
				returnModel.isErrorHappend = true;
				returnModel.Error.Add(new ErrorViewModel
				{
					ErrorName = "Sunucu tarafında hata oluştu api kontrolü yapın",
					Error = ex.InnerException.ToString()
				});
				return returnModel;
			}
		}

		public static async Task<string> UpdateSupplyList(int? id, int? piece, string term, decimal? price, string userid)
		{
			string returnModel;
			try
			{
				
				string url = Services.BaseUrl + "/updatesupplylist?id=" + id + "&piece=" + piece + "&term=" + term + "&price=" + price + "&userid=" + userid + "";
				var result = await Services.Request(url, null);
			
				return result;
			}
			catch (Exception ex)
			{
				
				return returnModel="Hata";
			}
		}

		/// <summary>
		/// Deletes the supply.
		/// </summary>
		/// <returns>The supply.</returns>
		/// <param name="user">User.</param>
		/// <param name="pass">Pass.</param>
		/// <param name="userid">Userid.</param>
		/// <param name="id">Identifier.</param>
		/// <param name="deleteoption">Deleteoption.</param>
		/// <param name="content">Content.</param>
		/// <param name="piece">Piece.</param>
		public static async Task<ResultViewModel> DeleteSupply(string user, string pass, string userid, int? id, string deleteoption, string content, int? piece)
		{
			ResultViewModel returnModel = new ResultViewModel();
			try
			{
				string url = Services.BaseUrl + "/deletesupply?user=" + user + "&pass=" + pass + "&userid=" + userid + "&id=" + id + "&deleteoption=" + deleteoption + "&content=" + content + "&piece=" + piece + "";
				var result = await Services.Request(url, null);
				returnModel = await Newtonsoft.Json.JsonConvert.DeserializeObjectAsync<ResultViewModel>(result);
				return returnModel;
			}
			catch (Exception ex)
			{
				returnModel.isErrorHappend = true;
				returnModel.Error.Add(new ErrorViewModel
				{
					ErrorName = "Sunucu tarafında hata oluştu api kontrolü yapın",
					Error = ex.InnerException.ToString()
				});
				return returnModel;
			}
		}
	}
}
