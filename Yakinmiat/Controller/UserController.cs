﻿using System;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Yakinmiyat.Models;
using Yakinmiyat.Users;

namespace Yakinmiat
{
	public static class UserController
	{
		
		/// <summary>
		/// Kullanıcının login bilgileri post edilip server tarafında kontrolu sağlanır.
		/// </summary>
		/// <param name="model">Model.</param>
		public static async Task<UserInfoModel> Login(LoginModel model)
		{
			UserInfoModel returnModel = new UserInfoModel();
			try
			{
				var loginModelJson = Newtonsoft.Json.JsonConvert.SerializeObject(model);
				var result = await Services.Request(Services.BaseUrl+"/login?UserName="+model.UserName+"&Password="+model.Password+"", null);
				returnModel = await Newtonsoft.Json.JsonConvert.DeserializeObjectAsync<UserInfoModel>(result);

				return returnModel;
			}
			catch (Exception ex)
			{
				returnModel.isErrorHappend = true;
				returnModel.Error.Add(new ErrorViewModel
				{
					ErrorName = "Sunucu tarafında hata oluştu api kontrolü yapın",
					Error = ex.InnerException.ToString()
				});
				return returnModel;
			}
		}

		/// <summary>
		/// Registers the user device.
		/// </summary>
		/// <returns>The user device.</returns>
		/// <param name="model">Model.</param>
		public static async Task<ResultViewModel> RegisterUserDevice(string userid, string device, string token, string uniqueid, string hubid)
		{
			ResultViewModel returnModel = new ResultViewModel();
			try
			{
				string url = Services.BaseUrl + "/registeruserdevice?userid=" + userid + "&device=" + device + "&token=" + token + "&uniqueid=" + uniqueid + "&hubid=" + hubid + "";
				var result = await Services.Request(url, null);
				returnModel = await Newtonsoft.Json.JsonConvert.DeserializeObjectAsync<ResultViewModel>(result);
				return returnModel;
			}
			catch (Exception ex)
			{
				returnModel.isErrorHappend = true;
				returnModel.Error.Add(new ErrorViewModel
				{
					ErrorName = "Sunucu tarafında hata oluştu api kontrolü yapın",
					Error = ex.InnerException.ToString()
				});
				return returnModel;
			}
		}
		/// <summary>
		/// Un the register user device.
		/// </summary>
		/// <returns>The register user device.</returns>
		/// <param name="model">Model.</param>
		public static async Task<ResultViewModel> UnRegisterUserDevice(DeviceRegisterModel model)
		{
			ResultViewModel returnModel = new ResultViewModel();
			try
			{
				var Json = Newtonsoft.Json.JsonConvert.SerializeObject(model);
				var result = await Services.Request(Services.BaseUrl + "/unregisteruserdevice", Json);
				returnModel = await Newtonsoft.Json.JsonConvert.DeserializeObjectAsync<ResultViewModel>(result);
				return returnModel;
			}
			catch (Exception ex)
			{
				returnModel.isErrorHappend = true;
				returnModel.Error.Add(new ErrorViewModel
				{
					ErrorName = "Sunucu tarafında hata oluştu api kontrolü yapın",
					Error = ex.InnerException.ToString()
				});
				return returnModel;
			}
		}

		/// <summary>
		/// Resets the password.
		/// </summary>
		/// <returns>The password.</returns>
		/// <param name="model">Model.</param>
		public static async Task<ResultViewModel> ResetPassword(ResetPasswordModel model)
		{ 
			ResultViewModel returnModel = new ResultViewModel();
			try
			{
				var Json = Newtonsoft.Json.JsonConvert.SerializeObject(model);
				var result = await Services.Request(Services.BaseUrl + "/resetpassword", Json);
				returnModel = await Newtonsoft.Json.JsonConvert.DeserializeObjectAsync<ResultViewModel>(result);
				return returnModel;
			}
			catch (Exception ex)
			{
				returnModel.isErrorHappend = true;
				returnModel.Error.Add(new ErrorViewModel
				{
					ErrorName = "Sunucu tarafında hata oluştu api kontrolü yapın",
					Error = ex.InnerException.ToString()
				});
				return returnModel;
			}
		}
	}
}
