﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Yakinmiyat.Models;
using Yakinmiyat.SearchDrug;

namespace Yakinmiat
{
	public static class SearchDrugController
	{
		public static async Task<List<SearchDrugModel>> SearchDrug(SearchDrugModel model)
		{
			List<SearchDrugModel> returnModel = new List<SearchDrugModel>();
			try
			{
				var Json = Newtonsoft.Json.JsonConvert.SerializeObject(model);
				var result = await Services.Request(Services.BaseUrl + "/searchdrug?SearchDrug="+model.SearchDrug+"&UserId="+model.UserId+"", null);
				returnModel = await Newtonsoft.Json.JsonConvert.DeserializeObjectAsync<List<SearchDrugModel>>(result);
				return returnModel;
			}
			catch (Exception ex)
			{
				returnModel[0].isErrorHappend = true;
				returnModel[0].Error.Add(new ErrorViewModel
				{
					ErrorName = "Sunucu tarafında hata oluştu api kontrolü yapın",
					Error = ex.InnerException.ToString()
				});
				return returnModel;
			}
		}
	}
}
