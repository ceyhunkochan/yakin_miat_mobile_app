﻿using System;
using System.Threading.Tasks;

namespace Yakinmiat
{
	public static class ConstantController
	{
		public static async Task<ConstantModel> GetConstant()
		{
			ConstantModel returnModel = new ConstantModel();
			string url = Services.BaseUrl + "/getconstant";
			var result = await Services.Request(url, null);
			returnModel = await Newtonsoft.Json.JsonConvert.DeserializeObjectAsync<ConstantModel>(result);
			return returnModel;
		}
	}
}
