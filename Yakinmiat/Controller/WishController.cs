﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Yakinmiyat.Models;
using Yakinmiyat.SearchDrug;
using Yakinmiyat.WishList;

namespace Yakinmiat
{
	public static class WishController
	{
		/// <summary>
		/// Gets the wish list.
		/// </summary>
		/// <returns>The wish list.</returns>
		/// <param name="user">User.</param>
		/// <param name="pass">Pass.</param>
		/// <param name="userid">Userid.</param>
		public static async Task<List<WishListModel>> GetWishList(string user, string pass, string userid)
		{ 
			List<WishListModel> returnModel = new List<WishListModel>();
			try
			{
				string url = Services.BaseUrl + "/getwishlist?user=" + user + "&pass="+pass+"&userid="+userid+"";
				var result = await Services.Request(url, null);
				returnModel = await Newtonsoft.Json.JsonConvert.DeserializeObjectAsync<List<WishListModel>>(result);
				return returnModel;
			}
			catch (Exception ex)
			{
				returnModel[0].isErrorHappend = true;
				returnModel[0].Error.Add(new ErrorViewModel
				{
					ErrorName = "Sunucu tarafında hata oluştu api kontrolü yapın",
					Error = ex.InnerException.ToString()
				});
				return returnModel;
			}
		}

		/// <summary>
		/// Others the suppliers.
		/// </summary>
		/// <returns>The suppliers.</returns>
		/// <param name="model">Model.</param>
		public static async Task<List<SearchDrugModel>> OtherSuppliers(OtherSuppliersModel model)
		{
			List<SearchDrugModel> returnModel = new List<SearchDrugModel>();
			try
			{
				var Json = Newtonsoft.Json.JsonConvert.SerializeObject(model);
				var result = await Services.Request(Services.BaseUrl + "/othersuppliers", Json);
				returnModel = await Newtonsoft.Json.JsonConvert.DeserializeObjectAsync<List<SearchDrugModel>>(result);
				return returnModel;
			}
			catch (Exception ex)
			{
				returnModel[0].isErrorHappend = true;
				returnModel[0].Error.Add(new ErrorViewModel
				{
					ErrorName = "Sunucu tarafında hata oluştu api kontrolü yapın",
					Error = ex.InnerException.ToString()
				});
				return returnModel;
			}
		}

		/// <summary>
		/// Sets the wish list.
		/// </summary>
		/// <returns>The wish list.</returns>
		/// <param name="user">User.</param>
		/// <param name="pass">Pass.</param>
		/// <param name="drugid">Drugid.</param>
		/// <param name="piece">Piece.</param>
		/// <param name="userid">Userid.</param>
		public static async Task<ResultViewModel> SetWishList(string user, string pass, int? drugid, int? piece, string userid)
		{ 
			ResultViewModel returnModel = new ResultViewModel();
			try
			{
				string url = Services.BaseUrl + "/setwishlist?user=" + user + "&pass=" + pass + "&drugid=" + drugid + "&piece="+piece+"&userid="+userid+"";
				var result = await Services.Request(url, null);
				returnModel = await Newtonsoft.Json.JsonConvert.DeserializeObjectAsync<ResultViewModel>(result);
				return returnModel;
			}
			catch (Exception ex)
			{
				returnModel.isErrorHappend = true;
				returnModel.Error.Add(new ErrorViewModel
				{
					ErrorName = "Sunucu tarafında hata oluştu api kontrolü yapın",
					Error = ex.InnerException.ToString()
				});
				return returnModel;
			}
		}


		public static async Task<string> UpdateWishList(int? id, int? piece, string userid)
		{
			string returnModel;
			try
			{

				string url = Services.BaseUrl + "/updatewishlist?id=" + id + "&piece=" + piece + "&userid=" + userid + "";
				var result = await Services.Request(url, null);

				return result;
			}
			catch (Exception ex)
			{

				return returnModel = "Hata";
			}
		}

		/// <summary>
		/// Deletes the wish.
		/// </summary>
		/// <returns>The wish.</returns>
		/// <param name="user">User.</param>
		/// <param name="pass">Pass.</param>
		/// <param name="userid">Userid.</param>
		/// <param name="id">Identifier.</param>
		/// <param name="deleteoption">Deleteoption.</param>
		/// <param name="content">Content.</param>
		/// <param name="piece">Piece.</param>
		public static async Task<ResultViewModel> DeleteWish(string user, string pass, string userid, int? id, string deleteoption, string content, int? piece)
		{ 
			ResultViewModel returnModel = new ResultViewModel();
			try
			{
				string url = Services.BaseUrl + "/deletewish?user=" + user + "&pass=" + pass + "&userid=" + userid + "&id=" + id + "&deleteoption=" + deleteoption + "&content="+content+"&piece="+piece+"";
				var result = await Services.Request(url, null);
				returnModel = await Newtonsoft.Json.JsonConvert.DeserializeObjectAsync<ResultViewModel>(result);
				return returnModel;
			}
			catch (Exception ex)
			{
				returnModel.isErrorHappend = true;
				returnModel.Error.Add(new ErrorViewModel
				{
					ErrorName = "Sunucu tarafında hata oluştu api kontrolü yapın",
					Error = ex.InnerException.ToString()
				});
				return returnModel;
			}
		}


		public static async Task<List<FollowCartModel>> GetFollow(string userid)
		{
			List<FollowCartModel> returnModel = new List<FollowCartModel>();
			try
			{

				string url = Services.BaseUrl + "/getfollow?userid=" + userid + "";
				var result = await Services.Request(url, null);
				returnModel = await Newtonsoft.Json.JsonConvert.DeserializeObjectAsync<List<FollowCartModel>>(result);
				return returnModel;
			}
			catch (Exception ex)
			{

				return returnModel;
			}
		}

		public static async Task<string> AddFollow(int? id, string userid)
		{
			string returnModel;
			try
			{

				string url = Services.BaseUrl + "/addfollow?id=" + id + "&userid=" + userid + "&type=urunbul";
				var result = await Services.Request(url, null);

				return result;
			}
			catch (Exception ex)
			{

				return returnModel = "Hata";
			}
		}

		public static async Task<string> DeleteFollow(int? id)
		{
			string returnModel;
			try
			{

				string url = Services.BaseUrl + "/deletefollow?id=" + id + "";
				var result = await Services.Request(url, null);

				return result;
			}
			catch (Exception ex)
			{

				return returnModel = "Hata";
			}
		}
	}
}
