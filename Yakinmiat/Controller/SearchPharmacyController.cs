﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Yakinmiyat.Models;
using Yakinmiyat.SearchPharmacy;

namespace Yakinmiat
{
	public static class SearchPharmacyController
	{
		//apiPass = "epsApiPass*!";
		//apiUser = "epsApiUser!";

		//code = GLN number 
		public static async Task<List<SearchPharmacyResultModel>> SearchPharmacy(string user, string pass, string userid, string name, string code, int? city)//  int? brand
		{ 
			List<SearchPharmacyResultModel> returnModel = new List<SearchPharmacyResultModel>();
			try
			{
				string url = Services.BaseUrl + "/searchpharmacy?user=" + user + "&pass=" + pass + "&userid=" + userid + "&name="+name+"&code="+code+"&city="+city+"";//+"&brand="+brand+""
				var result = await Services.Request(url, null);
				returnModel = await Newtonsoft.Json.JsonConvert.DeserializeObjectAsync<List<SearchPharmacyResultModel>>(result);
				return returnModel;
			}
			catch (Exception ex)
			{
				returnModel[0].isErrorHappend = true;
				returnModel[0].Error.Add(new ErrorViewModel
				{
					ErrorName = "Sunucu tarafında hata oluştu api kontrolü yapın",
					Error = ex.InnerException.ToString()
				});
				return returnModel;
			}
		}
	}
}
