﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Yakinmiat
{
	public static class ChatController
	{
		public static async Task<List<ServerChatModel>> GetUserConversation(string thisUser, string conversationUser)
		{
			List<ServerChatModel> returnModel = new List<ServerChatModel>();
			try
			{
				string url = Services.BaseUrl + "/getuserconversation?mobileUser=" + thisUser + "&userid=" + conversationUser + "&page=0";
				var result = await Services.Request(url, null);
				returnModel = await Newtonsoft.Json.JsonConvert.DeserializeObjectAsync<List<ServerChatModel>>(result);
				return returnModel;
			}
			catch (Exception ex)
			{

				return returnModel;
			}
		}

		public static async void SendMessage(string thisUser, string message, string userid)
		{
			List<ServerChatModel> returnModel = new List<ServerChatModel>();
			try
			{


				string url = Services.BaseUrl + "/sendmessage?mobileUser=" + thisUser + "&message=" + message + "&userid=" + userid + "";
				var result = await Services.Request(url, null);
				returnModel = await Newtonsoft.Json.JsonConvert.DeserializeObjectAsync<List<ServerChatModel>>(result);

				if (returnModel.Count>0)
				{
					ChatMessage chatItemForWeb = new ChatMessage();
					chatItemForWeb._avatar = returnModel[0].Image;
					chatItemForWeb._message = returnModel[0].Message;
					chatItemForWeb._dir = "in";
					chatItemForWeb._name = returnModel[0].PharmacyName;
					chatItemForWeb._time = returnModel[0].SendDate;

					await App._chatServices.Send(chatItemForWeb, returnModel[0].ReceiverHubId);
					for (int i = 0; i < returnModel.Count; i++)
					{
						if (i>0)
						{
							ChatMessage chatItemForMobile = new ChatMessage();
							chatItemForMobile._avatar = returnModel[i].Image;
							chatItemForMobile._message = returnModel[i].Message;
							chatItemForMobile._dir = "in";
							chatItemForMobile._name = returnModel[i].PharmacyName;
							chatItemForMobile._time = returnModel[i].SendDate;
							await App._chatServices.SendMobile(chatItemForWeb, returnModel[i].ReceiverHubId);
						}
					}
				}

			}
			catch (Exception ex)
			{
				
			}
		}

		public static async Task<List<ActiveChatUserModel>> GetConversation(string thisUser)
		{
			List<ActiveChatUserModel> returnModel = new List<ActiveChatUserModel>();
			try
			{
				string url = Services.BaseUrl + "/getconversation?mobileUser=" + thisUser + "";
				var result = await Services.Request(url, null);
				returnModel = await Newtonsoft.Json.JsonConvert.DeserializeObjectAsync<List<ActiveChatUserModel>>(result);
				return returnModel;
			}
			catch (Exception ex)
			{

				return returnModel;
			}
		}

	}
}
