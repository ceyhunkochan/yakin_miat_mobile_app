﻿using System;
using SQLite;
using SQLite.Extensions;
using SQLite.Net.Interop;
using SQLitePCL;



namespace Yakinmiat
{
	public interface ISQLite
	{
		string Directory { get; }
		ISQLitePlatform Platform { get; }
	}
}
