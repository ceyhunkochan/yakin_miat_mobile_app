﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNet.SignalR.Client;
using Xamarin.Forms;
using Yakinmiat;

[assembly: Dependency(typeof(ChatServices))]
namespace Yakinmiat
{
	public class ChatServices : IChatServices
	{
		private readonly HubConnection _connection;
		private readonly IHubProxy _proxy;
		public event EventHandler<ChatMessage> OnMessageReceived;

		public ChatServices()
		{
			_connection = new HubConnection("http://yakinmiat.com/");
			_proxy = _connection.CreateHubProxy("notificationHub");
		}

		public async Task Connect()
		{
			await _connection.Start();
			App.HubId = _connection.ConnectionId;



			_proxy.On("appendMobileNewMessage", (string name, string avatar, string message) => OnMessageReceived(this, new ChatMessage
			{
				_name = name,
				_avatar = avatar,
				_message = message,
				_time = DateTime.Now.ToString(),
				_dir = "out"
			}));
		}

		public Task JoinRoom(string roomName)
		{
			throw new NotImplementedException();
		}

		public async Task Send(ChatMessage message, string receiverId)
		{
			await _proxy.Invoke("Send", "in", message._time, message._name, message._avatar, message._message, receiverId);
		}

		public async Task SendMobile(ChatMessage message, string receiverId)
		{
			await _proxy.Invoke("SendMobile", message._name, message._avatar, message._message, receiverId);
		}
	}
}
