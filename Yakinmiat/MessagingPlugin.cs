﻿using System;
using System.Threading.Tasks;

namespace Yakinmiat
{

	public interface IEmailTask
	{
		bool CanSendEmail { get; }
		bool CanSendEmailAttachments { get; }
		bool CanSendEmailBodyAsHtml { get; }
		//void SendEmail(IEmailMessage email);
		void SendEmail(string to, string subject = null, string message = null);
	}

	public interface IWebTask
	{
		void OpenBrowser(string url);
	}

	public interface IPhoneCallTask
	{
		bool CanMakePhoneCall { get; }
		void MakePhoneCall(string number, string name = null);
	}

	public interface IDownloadTask
	{

		void OpenFile(string filePath);
		Task<bool> DownloadFromString(string uri);
	}


	public interface IDeleteDirectoryTask
	{
		Task<bool> DeleteDirectory();
	}

	public interface IIPAddressTask
	{
		Task<string> GetIPAddress();
	}

	public interface IClose
	{
		void Close();
	}

	public interface IAndroidNotify
	{
		void AndroidNotify();
	}
}
