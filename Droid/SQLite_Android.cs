﻿using System;
using Xamarin.Forms;
using Yakinmiat.Droid;
using System.IO;
using SQLite;
using SQLite.Net.Interop;
using SQLite.Net.Attributes;

[assembly: Dependency(typeof(SQLite_Android))]
namespace Yakinmiat.Droid
{
	public class SQLite_Android: ISQLite
	{
		private string _Directory;
		private ISQLitePlatform _Platform;

		public SQLite_Android()
		{
		}

		public string Directory
		{
			get
			{
				if (string.IsNullOrEmpty(_Directory))
				{
					_Directory = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
				}
				return _Directory;
			}
		}

		 

		public ISQLitePlatform Platform
		{
			get
			{
				if (_Platform == null)
				{
					_Platform = new SQLite.Net.Platform.XamarinAndroid.SQLitePlatformAndroid();
				}
				return _Platform;
			}
		}
	}
}

