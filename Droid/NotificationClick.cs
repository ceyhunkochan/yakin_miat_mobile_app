using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace Yakinmiat.Droid
{
    [Activity(Label = "NotificationClick")]
    public class NotificationClick : Activity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.NotificationClick);
            var intent = new Intent(this, typeof(MainActivity));
            StartActivity(intent);
            // Create your application here
        }
    }
}