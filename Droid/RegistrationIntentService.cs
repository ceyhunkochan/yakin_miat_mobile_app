﻿using System;
using Android.App;
using Android.Content;
using Android.Util;
using Android.Gms.Gcm;
using Android.Gms.Gcm.Iid;

namespace Yakinmiat.Droid
{
    [Service(Exported = false)]
    public class RegistrationIntentService : IntentService
    {
        static object locker = new object();

        public RegistrationIntentService() : base("RegistrationIntentService") { }
        
		public static string reg_token="";


		protected override void OnHandleIntent(Intent intent)
		{
			try
			{
				Log.Info("RegistrationIntentService", "Calling InstanceID.GetToken");
				lock (locker)
				{
					var instanceID = InstanceID.GetInstance(this); 
					var token = instanceID.GetToken(
						"750765844863", GoogleCloudMessaging.InstanceIdScope, null);

					Log.Info("RegistrationIntentService", "GCM Registration Token: " + token);
					Log.Debug("GCM TOKEN: ",token);
					SendRegistrationToAppServer(token);
					Subscribe(token);
				}
			}
			catch (Exception e)
			{
				Log.Debug("RegistrationIntentService", "Failed to get a registration token");
				return;
			}
		}

		 

        void SendRegistrationToAppServer(string token)
        {
            // Add custom implementation here as needed.
        }


       /*
        void SendRegistrationToAppServer(string token)
        {
            // Add custom implementation here as needed.
            URI url = null;
            try
            {
                url = new URI("http://mywebsite.com/demo/push/savetoken/?device_token=" + token + "&device_type=android&channels_id=1,2");
            }
            catch (URISyntaxException e)
            {
                // TODO Auto-generated catch block
                e.PrintStackTrace();
            }
            HttpClient httpclient = new DefaultHttpClient();
            HttpGet request = new HttpGet();
            request.setURI(url);
            try
            {
                httpclient.execute(request);
            }
            catch (ClientProtocolException e)
            {
                // TODO Auto-generated catch block
                e.PrintStackTrace();
            }
            catch (IOException e)
            {
                // TODO Auto-generated catch block
                e.PrintStackTrace();
            }
        }
        */


        void Subscribe(string token)
        {
            var pubSub = GcmPubSub.GetInstance(this);
            pubSub.Subscribe(token, "/topics/global", null);
        }
    }
}