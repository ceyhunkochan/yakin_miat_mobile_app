using System;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Gms.Gcm;
using Android.Util;

namespace Yakinmiat.Droid
{



    [Service(Exported = false), IntentFilter(new[] { "com.google.android.c2dm.intent.RECEIVE" })]
    public class MyGcmListenerService : GcmListenerService
    {

        public override void OnMessageReceived(string from, Bundle data)
        {

            var message = data.GetString("message");
		    var title = data.GetString("title");

			var pageId_string = data.GetString("pageId");
			int pageId = int.Parse(pageId_string);
			App.rootPageId = pageId;

		    //EkonomiKulubu.App.notice_type = notification_type;
		    //var notification_type = data.GetString("notification_type");
           

            Log.Debug("MyGcmListenerService", "From:    " + from);
            Log.Debug("MyGcmListenerService", "Message: " + message);
           
			SendNotification(message, title);

        }

        public void SendNotification(string _message, string _title)
        {
            var intent = new Intent(this, typeof(MainActivity));
            intent.AddFlags(ActivityFlags.ClearTop);
            var pendingIntent = PendingIntent.GetActivity(this, 0, intent, PendingIntentFlags.OneShot);

            string pathToPushSound = "android.resource://" + this.ApplicationContext.PackageName + "/raw/notice_sound";
            global::Android.Net.Uri soundUri = global::Android.Net.Uri.Parse(pathToPushSound);


            var notificationBuilder = new Notification.Builder(this)
                .SetSmallIcon(Resource.Drawable.ic_launcher)
                .SetContentTitle(_title)
                .SetContentText(_message)
                .SetAutoCancel(true)
                .SetVibrate(new long[] { 1000, 1000 })
                .SetSound(soundUri)
                .SetContentIntent(pendingIntent);

            var notificationManager = (NotificationManager)GetSystemService(Context.NotificationService);
            notificationManager.Notify(0, notificationBuilder.Build());

        }


    }


}