﻿using System;

using Xamarin.Forms;
using Android.App;
using Android.Content;
using System.Net;
using System.IO;
using System.Diagnostics;
using System.Threading.Tasks;
using Yakinmiat.Droid;

[assembly: Xamarin.Forms.Dependency(typeof(MessagingPlugingImplementation))]
namespace Yakinmiat.Droid
{
	public class MessagingPlugingImplementation : IEmailTask, IWebTask, IPhoneCallTask, IDownloadTask, IDeleteDirectoryTask, IIPAddressTask, IClose, IAndroidNotify
	{


		public MessagingPlugingImplementation()
		{
		}

		public bool CanMakePhoneCall
		{
			get
			{
				throw new NotImplementedException();
			}
		}

		public bool CanSendEmail
		{
			get
			{
				var mgr = Android.App.Application.Context.PackageManager;
				var emailIntent = new Intent(Intent.ActionSend);
				emailIntent.SetType("message/rfc822");

				return emailIntent.ResolveActivity(mgr) != null;
			}
		}

		public bool CanSendEmailAttachments
		{
			get
			{
				throw new NotImplementedException();
			}
		}

		public bool CanSendEmailBodyAsHtml
		{
			get
			{
				throw new NotImplementedException();
			}
		}



		public void MakePhoneCall(string number, string name = null)
		{

			/*
			AlertDialog.Builder builder = new AlertDialog.Builder(Android.App.Application.Context);
			AlertDialog dialog = builder.Create();
			dialog.SetTitle("ARAMA");
			dialog.SetMessage("Bu numarayı aramak istiyor musunuz? ");
			dialog.SetButton("EVET", (sender, e) =>
			{
				if (CanSendEmail)
					Device.OpenUri(new Uri("tel:" + number));
				else
				{
					AlertDialog dialog2 = builder.Create();
					dialog2.SetTitle("ARAMA");
					dialog2.SetTitle("Bu cihaz arama yapmayı desteklemiyor!");
					dialog2.SetButton("TAMAM", (s, en) =>
					{

					});
					dialog2.Show();

				}

			});

			dialog.SetButton("HAYIR", (sender, e) =>
			{


			});

			dialog.Show();

			*/

			Device.OpenUri(new Uri("tel:" + number));
		}


		public void OpenBrowser(string url)
		{

			/*
			AlertDialog.Builder builder = new AlertDialog.Builder(Android.App.Application.Context);
			AlertDialog dialog = builder.Create();
			dialog.SetTitle("WEB SAYFASI");
			dialog.SetMessage("Bu websitesini varsayılan tarayıca açmak istiyor musunuz? ");
			dialog.SetButton("EVET", (sender, e) => {

				if (url.StartsWith("http"))
					Device.OpenUri(new Uri(url));
				else
					Device.OpenUri(new Uri("http://" + url));

			});

			dialog.SetButton("HAYIR", (sender, e) =>
			{

 
			});

			dialog.Show();
			*/
			if (url.Contains("http"))
				Device.OpenUri(new Uri(url));
			else
				Device.OpenUri(new Uri("http://" + url));


		}

		public void SendEmail(string to, string subject = null, string message = null)
		{

			if (CanSendEmail)
				Device.OpenUri(new Uri("mailto:" + to));

		}




		public async Task<bool> DownloadFromString(string uri)
		{
			bool isDownloadCompleted = true;
			var webClient = new WebClient();
			string[] arr = uri.Split('/');
			string fileName = arr[arr.Length - 1];
			Uri var_uri = new Uri(uri);

			try
			{
				string RootPath = Android.OS.Environment.ExternalStorageDirectory.AbsolutePath;
				DirectoryInfo dirInfo = System.IO.Directory.CreateDirectory(RootPath + "/Download/EkonomiKulubuPDF");
				if (!File.Exists(RootPath + "/Download/EkonomiKulubuPDF/" + fileName))
				{
					await webClient.DownloadFileTaskAsync(var_uri, RootPath + "/Download/EkonomiKulubuPDF/" + fileName);
					webClient.DownloadFileCompleted += (sender, e) =>
					{
						if (e.Error != null || e.Cancelled)
						{
							isDownloadCompleted = false;
						}
					};
				}
			}
			catch (Exception ex)
			{
				if (ex != null)
				{
					Debug.WriteLine(ex.ToString());
					isDownloadCompleted = false;
					return false;
				}

			}

			return isDownloadCompleted;
		}



		//Open Medhod with problems
		public void OpenFile(string filePath)
		{

			var bytes = File.ReadAllBytes(filePath);

			//Copy the private file's data to the EXTERNAL PUBLIC location
			string externalStorageState = global::Android.OS.Environment.ExternalStorageState;
			string application = "";

			string extension = System.IO.Path.GetExtension(filePath);

			switch (extension.ToLower())
			{
				case ".doc":
				case ".docx":
					application = "application/msword";
					break;
				case ".pdf":
					application = "application/pdf";
					break;
				case ".xls":
				case ".xlsx":
					application = "application/vnd.ms-excel";
					break;
				case ".jpg":
				case ".jpeg":
				case ".png":
					application = "image/jpeg";
					break;
				default:
					application = "*/*";
					break;
			}
			var externalPath = global::Android.OS.Environment.ExternalStorageDirectory.Path + "/report" + extension;
			File.WriteAllBytes(externalPath, bytes);

			Java.IO.File file = new Java.IO.File(externalPath);
			file.SetReadable(true);
			//Android.Net.Uri uri = Android.Net.Uri.Parse("file://" + filePath);
			Android.Net.Uri uri = Android.Net.Uri.FromFile(file);
			Intent intent = new Intent(Intent.ActionView);
			intent.SetDataAndType(uri, application);
			intent.SetFlags(ActivityFlags.ClearWhenTaskReset | ActivityFlags.NewTask);

			try
			{
				Xamarin.Forms.Forms.Context.StartActivity(intent);
			}
			catch (Exception e)
			{
				Debug.WriteLine(e.ToString());
			}
		}

		public void Close()
		{
			Android.OS.Process.KillProcess(Android.OS.Process.MyPid());
		}

		public async Task<bool> DeleteDirectory()
		{
			string directory = "/storage/emulated/0/Download/EkonomiKulubuPDF";
			if (System.IO.Directory.Exists(directory))
			{
				try
				{
					Directory.Delete(directory, true);
					return true;
				}
				catch (Exception ex)
				{
					Debug.Write("Delete Directory Error: " + ex.ToString());
					return false;
				}

			}
			else
				return false;

		}

		public async Task<string> GetIPAddress()
		{
			string URLGetIP = "http://api.ipify.org";
			try
			{
				WebClient wc = new WebClient();
				string ip = await wc.DownloadStringTaskAsync(new Uri(URLGetIP));
				return ip;
			}
			catch
			{
				return string.Empty;
			}

		}


		public void AndroidNotify() 
		{
			MainActivity.MainActivityNotify("FORMS PUSH","successful");
		}

	}

}
