﻿using System;
using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using Android.Gms.Common;
using Plugin.Media;
using Xamarin.Forms.Platform.Android;

namespace Yakinmiat.Droid
{
	[Activity(Label = "Yakinmiat", Icon = "@drawable/ic_launcher", Theme = "@style/MyTheme", MainLauncher = true, ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation, ScreenOrientation = ScreenOrientation.Portrait)]
	public class MainActivity : global::Xamarin.Forms.Platform.Android.FormsAppCompatActivity
	{
		protected override async void OnCreate(Bundle bundle)
		{

			ReturnMainActivity = this;

			TabLayoutResource = Resource.Layout.Tabbar;
			ToolbarResource = Resource.Layout.Toolbar;

			base.OnCreate(bundle);

			global::Xamarin.Forms.Forms.Init(this, bundle);
			ImageCircleRenderer.Init();
			await CrossMedia.Current.Initialize();

			//RequestWindowFeature(WindowFeatures.NoTitle);
			string unique_DeviceID = Android.Provider.Settings.System.GetString(this.ContentResolver, Android.Provider.Settings.Secure.AndroidId);
			Yakinmiat.App.uniqueDeviceID = unique_DeviceID;

			if (IsPlayServicesAvailable())
			{
				var intent = new Intent(this, typeof(RegistrationIntentService));
				StartService(intent);
			}

			LoadApplication(new App());


		}

		public override void OnBackPressed()
		{
			
		}

		//protected override void OnDestroy()
		//{
		//	base.OnDestroy();
		//}

		public bool IsPlayServicesAvailable()
		{
			string str = "";
			int resultCode = GoogleApiAvailability.Instance.IsGooglePlayServicesAvailable(this);
			if (resultCode != ConnectionResult.Success)
			{
				if (GoogleApiAvailability.Instance.IsUserResolvableError(resultCode))
				{
					str = GoogleApiAvailability.Instance.GetErrorString(resultCode);
					//Toast.MakeText(this, str, ToastLength.Long).Show();
				}
				else
				{
					str = "Sorry, Google Play Services is not supported ";
					//Toast.MakeText(this, str, ToastLength.Long).Show();
					Finish();
				}
				return false;
			}
			else
			{
				str = "Google Play Services is available.";
				//Toast.MakeText(this, str, ToastLength.Long).Show();
				return true;
			}

		}



		public static Context ReturnMainActivity { get; set; }

		public static void MainActivityNotify(string _message, string _title) 
		{
			App.rootPageId = 5;

			var intent = new Intent(ReturnMainActivity, typeof(MainActivity));
			intent.AddFlags(ActivityFlags.ClearTop);
			var pendingIntent = PendingIntent.GetActivity(ReturnMainActivity, 0, intent, PendingIntentFlags.OneShot);

			string pathToPushSound = "android.resource://" + ReturnMainActivity.ApplicationContext.PackageName + "/raw/notice_sound";
			global::Android.Net.Uri soundUri = global::Android.Net.Uri.Parse(pathToPushSound);


			var notificationBuilder = new Notification.Builder(ReturnMainActivity)
				.SetSmallIcon(Resource.Drawable.ic_launcher)
				.SetContentTitle(_title)
				.SetContentText(_message)
				.SetAutoCancel(true)
				.SetVibrate(new long[] { 1000, 1000 })
				.SetSound(soundUri)
				.SetContentIntent(pendingIntent);

			var notificationManager = (NotificationManager) ReturnMainActivity.GetSystemService(Context.NotificationService);
			notificationManager.Notify(0, notificationBuilder.Build());
		}
	
	}




}
